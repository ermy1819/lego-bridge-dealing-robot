﻿// <copyright file="DealSummary.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

namespace LegoBridgeDealer.Controls
{
  using System;
  using System.IO;
  using Android.Bluetooth;
  using Android.Content;
  using Android.Widget;
  using LegoBridgeDealer.Activities;
  using LegoBridgeDealer.Communication;

  public class DealSummary : LinearLayout
  {
    private readonly DealInfo deal_;

    public DealSummary(Context context, DealInfo deal)
      : base(context)
    {
      deal_ = deal;
      Initialize();
    }

    private void Initialize()
    {
      Inflate(Context, Resource.Layout.DealSummary, this);
      FindViewById<TextView>(Resource.Id.dealNumber).Text = deal_.DealNumber.ToString();
      FindViewById<TextView>(Resource.Id.dealer).Text = deal_.Dealer;
      FindViewById<TextView>(Resource.Id.vulnerability).Text = deal_.Vulnerability;
      FindViewById<Button>(Resource.Id.dealButton).Click += DealClicked_;
      FindViewById<Button>(Resource.Id.resultButton).Click += ResultsClicked_;
      FindViewById<Button>(Resource.Id.cardsButton).Click += ShowCardsClicked_;
    }

    private void ShowCardsClicked_(object sender, EventArgs e)
    {
      var intent = new Intent(Context, typeof(ShowCards));
      intent.PutExtras(deal_.CardsBundle);
      Context.StartActivity(intent);
    }

    private void ResultsClicked_(object sender, EventArgs e)
    {
      var intent = new Intent(Context, typeof(ShowResults));
      intent.PutStringArrayListExtra(ShowResults.DealResults, deal_.Results);
      Context.StartActivity(intent);
    }

    private async void DealClicked_(object sender, EventArgs e)
    {
      Toast.MakeText(Context, Resources.GetText(Resource.String.SendDealToRobot), ToastLength.Short).Show();
      try
      {
        var bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
        if (!bluetoothAdapter.IsEnabled)
        {
          Intent enableBtIntent = new Intent(BluetoothAdapter.ActionRequestEnable);
          Context.StartActivity(enableBtIntent);
          Toast.MakeText(Context, Resources.GetText(Resource.String.BluetoothDisabled), ToastLength.Short).Show();
          return;
        }

        await new DealSender().Send(deal_);
        Toast.MakeText(Context, Resources.GetText(Resource.String.SendingSuccessful), ToastLength.Short).Show();
      }
      catch (IOException ex)
      {
        Toast.MakeText(Context, Resources.GetText(Resource.String.SendingFailed) + Environment.NewLine + ex.Message,
          ToastLength.Long).Show();
      }
      catch (Java.IO.IOException ex)
      {
        Toast.MakeText(Context, Resources.GetText(Resource.String.SendingFailed) + Environment.NewLine + ex.Message,
          ToastLength.Long).Show();
      }
      catch (Exception ex)
      {
        Toast.MakeText(Context, "Unexpected exception of type " + ex.GetType().Name + Environment.NewLine + ex.Message,
          ToastLength.Long).Show();
      }
    }
  }
}