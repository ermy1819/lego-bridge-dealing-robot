﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LegoBridgeDealer.PbnModel;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnParser
{
  /// <summary>
  /// Parses .pbn files (Portable Bridge Notation) into Model objects. Target PBN version is 2.1.
  /// Note however that this parser is not complete, all kinds of PBN input can not be parsed yet.
  /// </summary>
#pragma warning disable CA1724
  public class PbnParser
#pragma warning restore CA1724
  {
    internal static event EventHandler<ReadLineEventArgs> LineRead;

    public Root Parse(TextReader reader)
    {
      var result = new Root();
      string line;
      Stack<IParserState> state = new Stack<IParserState>();
      Queue<(int GameId, string Line)> parseLater = new Queue<(int GameId, string Line)>();
      state.Push(new LookingForDeal(result, parseLater));

      while ((line = reader.ReadLine()) != null)
      {
        LineRead?.Invoke(null, new ReadLineEventArgs(line));
        while ((line = state.Peek().ParseLine(line, state)) != null)
        {
          // Nothing else to do than the call to ParseLine
        }
      }

      while (state.Count > 0)
      {
        if (state.Peek() is ParsingDeal)
          state.Peek().ParseLine(null, state);
        else
          state.Pop();
      }

      //Assert.That(LineRead == null);
      ParseSecondPass_(result, parseLater);
      MergeEvents_(result);
      RemoveInvalidGames_(result.Games);
      RemoveUnreferencedPlayers_(result);

      return result;
    }

    internal static void Clean()
    {
      LineRead = null;
    }

    private void RemoveInvalidGames_(Repository<Game> games)
    {
      foreach (var game in games.Values.ToList())
      {
        if (game.Contract.Tricks < 0) games.Remove(game.Id);
      }
    }

    private void RemoveUnreferencedPlayers_(Root result)
    {
      foreach (var player in result.Players.Keys.ToList())
      {
        if (!result.Games.Values.Any(
              g => g.NorthId == player || g.SouthId == player || g.EastId == player || g.WestId == player))
          result.Players.Remove(player);
      }
    }

    private void ParseSecondPass_(Root result, Queue<(int GameId, string Line)> parseLater)
    {
      while (parseLater.Count > 0)
      {
        (var gameId, var line) = parseLater.Dequeue();
        var segments = line.Split(';');
        switch (segments[0])
        {
          case "ScoreTable":
            new ScoreTableSecondPass(result).Parse(gameId, segments);
            break;
          default: throw new InvalidOperationException("Unknown command to parse for second pass");
        }
      }
    }

    private void MergeEvents_(Root results)
    {
      foreach (var keyValuePair in results.Events.OrderBy(e => e.Key).Where(kv => kv.Value.Name == "#"))
      {
        keyValuePair.Value.Name = results.Events[keyValuePair.Key - 1].Name;
      }

      foreach (var keyValuePair in results.Events.OrderBy(e => e.Key).Where(kv => kv.Value.Site == "#"))
      {
        keyValuePair.Value.Site = results.Events[keyValuePair.Key - 1].Site;
      }

      foreach (var keyValuePair in results.Events.OrderBy(e => e.Key).Where(kv => kv.Value.StartDate == new DateTime(1, 1, 1)))
      {
        if (keyValuePair.Key > results.Events.OrderBy(e => e.Key).First().Key)
        {
          keyValuePair.Value.StartDate = results.Events[keyValuePair.Key - 1].StartDate;
          keyValuePair.Value.EndDate = results.Events[keyValuePair.Key - 1].EndDate;
        }
      }

      // Does not yet handle events with same name, different sites
      foreach (var eventGroup in results.Events.GroupBy(i => i.Value.Name).ToList())
      {
        var events = eventGroup.ToList();
        var toKeep = events.First();
        toKeep.Value.StartDate = events.Min(e => e.Value.StartDate);
        toKeep.Value.EndDate = events.Max(e => e.Value.EndDate);

        foreach (var @event in events.Skip(1))
        {
          foreach (var game in results.Games.Values.Where(g => g.EventId == @event.Key))
          {
            game.EventId = events[0].Key;
          }

          results.Events.Remove(@event.Key);
        }
      }
    }
  }
}
