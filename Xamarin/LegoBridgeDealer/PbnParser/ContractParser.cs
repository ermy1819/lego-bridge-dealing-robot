using System;
using System.Globalization;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnParser
{
  internal class ContractParser
  {
    public Contract Parse(string value)
    {
      if (string.IsNullOrWhiteSpace(value))
        return new Contract(-1, default(Suit), default(Risk));
      return value.Equals("Pass", StringComparison.InvariantCultureIgnoreCase)
             || value.Equals("PAS", StringComparison.InvariantCultureIgnoreCase)
               ? new Contract()
               : new Contract(int.Parse(value.Substring(0, 1), CultureInfo.InvariantCulture), CharToSuit_(value[1]), RiskFromTrailingXes_(value));
    }

    private Risk RiskFromTrailingXes_(string value)
    {
      if (value.EndsWith("XX", StringComparison.InvariantCulture)) return Risk.Redoubled;
      if (value.EndsWith("X", StringComparison.InvariantCulture)) return Risk.Doubled;
      return Risk.Undoubled;
    }

    private Suit CharToSuit_(char value)
    {
      switch (value)
      {
        case 'C':
          return Suit.Clubs;
        case 'D': return Suit.Diamonds;
        case 'H': return Suit.Hearts;
        case 'S': return Suit.Spades;
        case 'N': return Suit.NoTrump;
      }

      throw new FormatException("Invalid value for suit");
    }
  }
}