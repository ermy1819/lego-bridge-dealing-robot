﻿namespace LegoBridgeDealer.PbnParser
{
  internal class BlockComment : ReadToEnd
  {
    protected override string EndToken_ => "}";
  }
}