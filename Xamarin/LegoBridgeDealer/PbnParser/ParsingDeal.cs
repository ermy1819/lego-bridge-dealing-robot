﻿// <copyright file="ParsingDeal.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

namespace LegoBridgeDealer.PbnParser
{
  using System;
  using System.Collections.Generic;
  using System.Text;
  using LegoBridgeDealer.PbnModel;
  using LegoBridgeDealer.PbnModel.WriteModel;

  internal class ParsingDeal : IParserState
  {
    private readonly Game currentGame_;
    private readonly Event currentEvent_;
    private readonly List<string> completedTags_ = new List<string>();
    private readonly Repository<Player> players_;
    private readonly Queue<(int GameId, string Line)> parseLater_;
    private readonly StringBuilder rawPbn_ = new StringBuilder();

    public ParsingDeal(Game currentGame, Event currentEvent, Repository<Player> players, Queue<(int GameId, string Line)> parseLater)
    {
      currentGame_ = currentGame;
      currentEvent_ = currentEvent;
      players_ = players;
      parseLater_ = parseLater;
      PbnParser.LineRead += PbnParser_LineRead_;
    }

    public string ParseLine(string line, Stack<IParserState> state)
    {
      //Assert.That(state.Peek(), Is.SameAs(this));

      if (rawPbn_.Length == 0) rawPbn_.AppendLine(line);

      if (string.IsNullOrWhiteSpace(line))
      {
        state.Pop();
        currentGame_.RawPbn = rawPbn_.ToString();
        PbnParser.LineRead -= PbnParser_LineRead_;
        return null;
      }

      line = line.TrimStart();
      if (line.StartsWith("[", StringComparison.InvariantCulture))
      {
        ////state.Push(new ReadToEndOfTag());
        state.Push(new ReadTag(completedTags_, currentGame_, currentEvent_, players_, parseLater_));
        return line.Substring(1).TrimOrNull();
      }

      if (line.StartsWith("{", StringComparison.InvariantCulture))
      {
        state.Push(new BlockComment());
        return line.Substring(1).TrimOrNull();
      }

      return null;
    }

    private void PbnParser_LineRead_(object sender, ReadLineEventArgs e)
    {
      rawPbn_.AppendLine(e.Line);
    }
  }
}