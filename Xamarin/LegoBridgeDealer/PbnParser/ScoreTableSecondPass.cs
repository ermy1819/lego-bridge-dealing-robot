using System.Globalization;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnParser
{
  internal class ScoreTableSecondPass
  {
    private readonly Root root_;

    public ScoreTableSecondPass(Root root)
    {
      root_ = root;
    }

    public void Parse(int templateGameId, string[] segments)
    {
      //Assert.That(segments[0], Is.EqualTo("ScoreTable"));
      //Assert.That(segments.Length, Is.EqualTo(8));

      if (segments[1] == "-" || segments[2] == "-" || segments[3] == "-" || segments[6].Contains(">")) return;

      var template = root_.Games[templateGameId];
      var game = new Game
                   {
                     Id = root_.Games.Count,
                     NorthId = ParseAsInt_(segments[1]),
                     SouthId = ParseAsInt_(segments[1]),
                     EastId = ParseAsInt_(segments[2]),
                     WestId = ParseAsInt_(segments[2]),
                     Contract = new ContractParser().Parse(segments[3]),
                     Declarer = new DirectionParser().Parse(segments[4]),
                     Result = segments[5] == "-" ? 0 : int.Parse(segments[5], CultureInfo.InvariantCulture),
                     Score = new Score(int.Parse(segments[6], CultureInfo.InvariantCulture)),
                     ScorePercentage = float.Parse(segments[7], CultureInfo.InvariantCulture),
                     Board = template.Board,
                     Date = template.Date,
                     Deal = template.Deal,
                     Dealer = template.Dealer,
                     EventId = template.EventId,
                     RawPbn = template.RawPbn,
                     ScoreIMP = template.ScoreIMP,
                     ScoreMP = template.ScoreMP,
                     Scoring = template.Scoring,
                     Vulnerable = template.Vulnerable,
                   };

      root_.Games.Add(game.Id, game);
    }

    private int ParseAsInt_(string value)
    {
      if (int.TryParse(value, out var parsed))
        return parsed;
      return value.GetHashCode();
    }
  }
}