﻿namespace LegoBridgeDealer.PbnParser
{
  internal static class StringExtensions
  {
    public static string TrimOrNull(this string input)
    {
      if (string.IsNullOrWhiteSpace(input)) return null;
      return input.TrimStart();
    }
  }
}