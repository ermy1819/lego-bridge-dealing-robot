using System;
using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnParser
{
  internal class ParseScoreTable : IParserState
  {
    private readonly string tableHeader_;
    private readonly Game currentGame_;
    private readonly Queue<(int GameId, string Line)> parseLater_;

    private List<(string Header, int Index)> headers_;
    private List<string> columns_;

    public ParseScoreTable(string tableHeader, Game currentGame, Queue<(int GameId, string Line)> parseLater)
    {
      tableHeader_ = tableHeader;
      currentGame_ = currentGame;
      parseLater_ = parseLater;
    }

    public string ParseLine(string line, Stack<IParserState> state)
    {
      //Assert.That(state.Peek(), Is.SameAs(this));

      if (string.IsNullOrWhiteSpace(line) || line.TrimStart().StartsWith("[", StringComparison.InvariantCulture))
      {
        state.Pop();
        return line;
      }

      headers_ = headers_ ?? tableHeader_.Split(';').Select<string, (string Header, int Index)>((header, index) => (header.Substring(0, IndexOfHeaderEnd_(header)), index)).ToList();
      //Assert.That(headers_.Count == headers_.Distinct().Count());
      columns_ = line.Split('"').SelectMany((l, i) => i % 2 == 0 ? l.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) : new[] { l }).ToList();

      var declarer = GetValue_("Declarer");
      var nsDeclarer = !(declarer == "E" || declarer == "W");

      parseLater_.Enqueue(
        (currentGame_.Id,
        $"ScoreTable;{GetValue_("PairId_NS")};{GetValue_("PairId_EW")};{GetValue_("Contract")};{declarer};{GetValue_("Result")};{GetSelectedValue_("Score_NS", "Score_EW", nsDeclarer)};{GetSelectedValue_("Percentage_NS", "Percentage_EW", nsDeclarer)}"
        ));
      return null;
    }

    private static int IndexOfHeaderEnd_(string h)
    {
      var indexOf = h.IndexOf("\\", StringComparison.InvariantCulture);
      return indexOf == -1 ? h.Length : indexOf;
    }

    private string GetSelectedValue_(string key1, string key2, bool select1)
    {
      string value = GetValue_(select1 ? key1 : key2);
      if (value == "-")
      {
        value = "-" + GetValue_(select1 ? key2 : key1);
      }

      return value;
    }

    private string GetValue_(string key)
    {
      var index = headers_.First(h => h.Header == key).Index;
      return columns_[index];
    }
  }
}