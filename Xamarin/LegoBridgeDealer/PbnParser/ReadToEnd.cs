﻿using System;
using System.Collections.Generic;

namespace LegoBridgeDealer.PbnParser
{
  internal abstract class ReadToEnd : IParserState
  {
    protected abstract string EndToken_ { get; }

    public string ParseLine(string line, Stack<IParserState> state)
    {
      //Assert.That(state.Peek(), Is.SameAs(this));
      var index = line.IndexOf(EndToken_, StringComparison.Ordinal);
      if (index == -1) return null;
      state.Pop();
      return line.Substring(index + 1).TrimOrNull();
    }
  }
}