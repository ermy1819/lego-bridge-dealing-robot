using System;
using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnParser
{
  internal class ParseTotalScoreTable : IParserState
  {
    private readonly string tableHeader_;

    private readonly Repository<Player> players_;

    public ParseTotalScoreTable(string tableHeader, Repository<Player> players)
    {
      tableHeader_ = tableHeader;
      players_ = players;
    }

    public string ParseLine(string line, Stack<IParserState> state)
    {
      //Assert.That(state.Peek(), Is.SameAs(this));

      if (line.TrimStart().StartsWith("[", StringComparison.InvariantCulture))
      {
        state.Pop();
        return line;
      }

      if (string.IsNullOrWhiteSpace(line))
        return null;

      var headers = tableHeader_.Split(';').Select((header, index) => new { header, index }).ToList();
      var nameIndex = headers.Single(h => h.header.Substring(0, IndexOfHeaderEnd_(h.header)) == "Names").index;
      var idIndex = headers.Single(h => h.header.Substring(0, IndexOfHeaderEnd_(h.header)) == "PairId").index;
      var columns = line.Split('"').SelectMany((l, i) => i % 2 == 0 ? l.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries) : new[] { l }).ToList();
      if (!int.TryParse(columns[idIndex], out var id))
        id = columns[idIndex].GetHashCode();

      if (!players_.ContainsKey(id))
        players_.Add(id, new Player { Id = id, Name = columns[nameIndex] });
      return null;
    }

    private static int IndexOfHeaderEnd_(string h)
    {
      var indexOf = h.IndexOf("\\", StringComparison.InvariantCulture);
      return indexOf == -1 ? h.Length : indexOf;
    }
  }
}