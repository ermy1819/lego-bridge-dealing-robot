using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnParser
{
  internal class LookingForDeal : IParserState
  {
    private readonly Root result_;
    private readonly Queue<(int GameId, string Line)> parseLater_;

    private Game currentGame_;
    private Event currentEvent_;

    public LookingForDeal(Root result, Queue<(int GameId, string Line)> parseLater)
    {
      result_ = result;
      parseLater_ = parseLater;
    }

    public string ParseLine(string line, Stack<IParserState> state)
    {
      //Assert.That(state.Peek(), Is.SameAs(this));
      if (string.IsNullOrWhiteSpace(line)) return null;
      if (line.All(char.IsControl)) return null;
      currentGame_ = new Game { Id = result_.Games.Count, EventId = result_.Events.Count };
      currentEvent_ = new Event { Id = result_.Events.Count };
      result_.Games.Add(result_.Games.Count, currentGame_);
      result_.Events.Add(result_.Events.Count, currentEvent_);
      state.Push(new ParsingDeal(currentGame_, currentEvent_, result_.Players, parseLater_));
      return line;
    }
  }
}