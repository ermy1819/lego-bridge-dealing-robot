﻿namespace LegoBridgeDealer.PbnParser
{
  internal class ReadToEndOfTag : ReadToEnd
  {
    protected override string EndToken_ => "]";
  }
}