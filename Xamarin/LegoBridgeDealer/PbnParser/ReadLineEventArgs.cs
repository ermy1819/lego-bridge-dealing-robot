﻿using System;

namespace LegoBridgeDealer.PbnParser
{
  internal class ReadLineEventArgs : EventArgs
  {
    public ReadLineEventArgs(string line)
    {
      Line = line;
    }

    public string Line { get; }
  }
}