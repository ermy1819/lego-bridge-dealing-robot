﻿using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnParser
{
  internal class ReadTag : IParserState
  {
    private readonly List<string> completedTags_;
    private readonly Game currentGame_;
    private readonly Event currentEvent_;

    private readonly Repository<Player> players_;

    private readonly Queue<(int GameId, string Line)> parseLater_;

    public ReadTag(List<string> completedTags, Game currentGame, Event currentEvent, Repository<Player> players, Queue<(int GameId, string Line)> parseLater)
    {
      completedTags_ = completedTags;
      currentGame_ = currentGame;
      currentEvent_ = currentEvent;
      players_ = players;
      parseLater_ = parseLater;
    }

    public string ParseLine(string line, Stack<IParserState> state)
    {
      //Assert.That(state.Peek(), Is.SameAs(this));
      if (string.IsNullOrWhiteSpace(line)) return null;
      var tagName = line.Split(' ').First();
      state.Pop();
      if (!completedTags_.Contains(tagName))
      {
        completedTags_.Add(tagName);
        state.Push(new ReadTagValue(tagName, currentGame_, currentEvent_, players_, parseLater_));
      }
      else
      {
        state.Push(new ReadToEndOfTag());
      }

      return line.Substring(tagName.Length).TrimOrNull();
    }
  }
}