using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using LegoBridgeDealer.PbnModel;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnParser
{
  internal class ReadTagValue : IParserState
  {
    private readonly string tagName_;
    private readonly Game currentGame_;
    private readonly Event currentEvent_;
    private readonly Repository<Player> players_;
    private readonly Queue<(int GameId, string Line)> parseLater_;

    private string value_ = string.Empty;

    public ReadTagValue(string tagName, Game currentGame, Event currentEvent, Repository<Player> players, Queue<(int GameId, string Line)> parseLater)
    {
      //Assert.IsNotNull(tagName);
      tagName_ = tagName;
      currentGame_ = currentGame;
      currentEvent_ = currentEvent;
      players_ = players;
      parseLater_ = parseLater;
    }

    public string ParseLine(string line, Stack<IParserState> state)
    {
      //Assert.That(state.Peek(), Is.SameAs(this));
      var endIndex = line.IndexOf("]", StringComparison.Ordinal);
      if (endIndex == -1)
      {
        value_ += line + Environment.NewLine;
        return null;
      }

      value_ += line.Substring(0, endIndex).TrimEnd();
      value_ = value_.Trim('"');
      state.Pop();

      switch (tagName_)
      {
        case "Event":
          currentEvent_.Name = value_;
          break;
        case "Site":
          currentEvent_.Site = value_;
          break;
        case "Date":
          if (value_ != "?" && value_ != "#" && value_ != string.Empty) currentEvent_.StartDate = currentEvent_.EndDate = currentGame_.Date = DateTime.Parse(value_.Replace("??", "01"), CultureInfo.InvariantCulture);
          break;
        case "Board":
          if (value_ != "?" && value_ != string.Empty) currentGame_.Board = int.Parse(value_, CultureInfo.InvariantCulture);
          break;
        case "West":
          currentGame_.WestId = GetPlayerId_(value_);
          break;
        case "North":
          currentGame_.NorthId = GetPlayerId_(value_);
          break;
        case "East":
          currentGame_.EastId = GetPlayerId_(value_);
          break;
        case "South":
          currentGame_.SouthId = GetPlayerId_(value_);
          break;
        case "Vulnerable":
          switch (value_)
          {
            case "None":
            case "Love":
            case "-":
              currentGame_.Vulnerable = Vulnerability.None;
              break;
            case "NS":
              currentGame_.Vulnerable = Vulnerability.NorthSouth;
              break;
            case "EW":
              currentGame_.Vulnerable = Vulnerability.EastWest;
              break;
            case "All":
            case "Both":
              currentGame_.Vulnerable = Vulnerability.All;
              break;
          }

          break;
        case "Deal":
          currentGame_.Deal = value_;
          break;
        case "Scoring":
          currentGame_.Scoring = value_;
          break;
        case "Contract":
          currentGame_.Contract = new ContractParser().Parse(value_);
          break;
        case "Declarer":
          currentGame_.Declarer = new DirectionParser().Parse(value_);
          break;
        case "Dealer":
          currentGame_.Dealer = new DirectionParser().Parse(value_);
          break;
        case "Result":
          if (!value_.EndsWith("*", StringComparison.InvariantCulture) && value_ != string.Empty && value_ != "?")
          {
            currentGame_.Result = int.Parse(value_.TrimStart('^'), CultureInfo.InvariantCulture);
          }

          break;
        case "Score":
          if (value_.StartsWith("NS", StringComparison.InvariantCulture))
            currentGame_.Score = new Score(int.Parse(new string(value_.Skip(2).TakeWhile(c => char.IsDigit(c) || c == '-' || char.IsWhiteSpace(c)).ToArray()), CultureInfo.InvariantCulture.NumberFormat), PlayerDirection.North);
          else if (value_.StartsWith("EW", StringComparison.InvariantCulture))
            currentGame_.Score = new Score(int.Parse(new string(value_.Skip(2).TakeWhile(c => char.IsDigit(c) || c == '-').ToArray()), CultureInfo.InvariantCulture.NumberFormat), PlayerDirection.East);
          else
            currentGame_.Score = new Score(int.Parse(value_, CultureInfo.InvariantCulture.NumberFormat));
          break;
        case "ScoreMP":
          currentGame_.ScoreMP = float.Parse(value_, CultureInfo.InvariantCulture.NumberFormat);
          break;
        case "ScoreIMP":
          currentGame_.ScoreIMP = float.Parse(value_, CultureInfo.InvariantCulture.NumberFormat);
          break;
        case "ScorePercentage":
          currentGame_.ScorePercentage = float.Parse(value_, CultureInfo.InvariantCulture.NumberFormat);
          break;
        case "TotalScoreTable":
          state.Push(new ParseTotalScoreTable(value_, players_));
          break;
        case "ScoreTable":
          state.Push(new ParseScoreTable(value_, currentGame_, parseLater_));
          break;
      }

      return line.Substring(endIndex + 1).TrimOrNull();
    }

    private int GetPlayerId_(string name)
    {
      var existing = players_.Values.SingleOrDefault(p => p.Name == name);
      if (existing != null) return existing.Id;

      int id = players_.Count;
      players_.Add(id, new Player { Id = id, Name = name });
      return id;
    }
  }
}