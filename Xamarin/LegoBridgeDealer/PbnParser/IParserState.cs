﻿using System.Collections.Generic;

namespace LegoBridgeDealer.PbnParser
{
  /// <summary>
  /// The common interface for all parser states.
  /// </summary>
  internal interface IParserState
  {
    /// <summary>
    /// Parse the input line.
    /// </summary>
    /// <param name="line">The input line to parse.</param>
    /// <param name="state">The parser objects stack</param>
    /// <returns>The remaining parts of the line not parsed; or null if the line is completely parsed.</returns>
    string ParseLine(string line, Stack<IParserState> state);
  }
}