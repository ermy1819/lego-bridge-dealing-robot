﻿// <copyright file="PairsDealList.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

namespace LegoBridgeDealer.Activities
{
  using Android.App;

  /// <inheritdoc />
  [Activity(Label = "@string/DealList", ParentActivity = typeof(MatchPointTournamentSelection))]
  internal class PairsDealList : DealList { }
}