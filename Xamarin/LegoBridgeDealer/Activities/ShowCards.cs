﻿// <copyright file="ShowCards.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

namespace LegoBridgeDealer.Activities
{
  using Android.App;
  using Android.OS;
  using Android.Support.V7.App;
  using Android.Widget;

  [Activity(Label = "@string/ShowCards")]
  public class ShowCards : AppCompatActivity
  {
    protected override void OnCreate(Bundle savedInstanceState)
    {
      base.OnCreate(savedInstanceState);

      SetContentView(Resource.Layout.CardDisplay);

      FindViewById<TextView>(Resource.Id.northSpades).Text = Intent.GetStringExtra(DealInfo.NorthSpadesKey);
      FindViewById<TextView>(Resource.Id.northHearts).Text = Intent.GetStringExtra(DealInfo.NorthHeartsKey);
      FindViewById<TextView>(Resource.Id.northDiamonds).Text = Intent.GetStringExtra(DealInfo.NorthDiamondsKey);
      FindViewById<TextView>(Resource.Id.northClubs).Text = Intent.GetStringExtra(DealInfo.NorthClubsKey);
      FindViewById<TextView>(Resource.Id.southSpades).Text = Intent.GetStringExtra(DealInfo.SouthSpadesKey);
      FindViewById<TextView>(Resource.Id.southHearts).Text = Intent.GetStringExtra(DealInfo.SouthHeartsKey);
      FindViewById<TextView>(Resource.Id.southDiamonds).Text = Intent.GetStringExtra(DealInfo.SouthDiamondsKey);
      FindViewById<TextView>(Resource.Id.southClubs).Text = Intent.GetStringExtra(DealInfo.SouthClubsKey);
      FindViewById<TextView>(Resource.Id.eastSpades).Text = Intent.GetStringExtra(DealInfo.EastSpadesKey);
      FindViewById<TextView>(Resource.Id.eastHearts).Text = Intent.GetStringExtra(DealInfo.EastHeartsKey);
      FindViewById<TextView>(Resource.Id.eastDiamonds).Text = Intent.GetStringExtra(DealInfo.EastDiamondsKey);
      FindViewById<TextView>(Resource.Id.eastClubs).Text = Intent.GetStringExtra(DealInfo.EastClubsKey);
      FindViewById<TextView>(Resource.Id.westSpades).Text = Intent.GetStringExtra(DealInfo.WestSpadesKey);
      FindViewById<TextView>(Resource.Id.westHearts).Text = Intent.GetStringExtra(DealInfo.WestHeartsKey);
      FindViewById<TextView>(Resource.Id.westDiamonds).Text = Intent.GetStringExtra(DealInfo.WestDiamondsKey);
      FindViewById<TextView>(Resource.Id.westClubs).Text = Intent.GetStringExtra(DealInfo.WestClubsKey);
    }
  }
}