﻿// <copyright file="DealList.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

namespace LegoBridgeDealer.Activities
{
  using System.Collections.Generic;
  using System.IO;
  using System.Linq;
  using Android.OS;
  using Android.Support.V7.App;
  using Android.Widget;
  using LegoBridgeDealer.Controls;
  using LegoBridgeDealer.PbnModel.ReadModel;

  /// <summary>
  /// An activity that shows the list of all deals for a single tournament
  /// </summary>
  internal abstract class DealList : AppCompatActivity
  {
    /// <summary>
    /// Key for the tournament pbn path for a selected tournament.
    /// </summary>
    public const string TournamentPbnPath = "TOURNAMENT_PBN_PATH_FOR_SELECTED_TOURNAMENT";

    /// <inheritdoc />
    protected override void OnCreate(Bundle savedInstanceState)
    {
      base.OnCreate(savedInstanceState);
      var pbnPath = Intent.GetStringExtra(TournamentPbnPath);
      var scrollView = new ScrollView(this);
      var layout = new LinearLayout(this) { Orientation = Orientation.Vertical };
      scrollView.AddView(layout);
      Title = Path.GetFileNameWithoutExtension(pbnPath);
      SetContentView(scrollView);

      foreach (var deal in GetDeals_(pbnPath))
      {
        var summary = new DealSummary(this, deal);
        layout.AddView(summary);
      }
    }

    private IEnumerable<DealInfo> GetDeals_(string pbnPath)
    {
      var parser = new PbnParser.PbnParser();
      var pbnContent = parser.Parse(new StreamReader(Assets.Open(pbnPath)));
      var allDeals = new SelectionFactory().CreateSelection(pbnContent, new PbnModel.ReadModel.Filter());
      allDeals.Repopulate();
      int index = 0;
      foreach (var deal in allDeals.Deals)
      {
        var playerDeals = deal.PlayerDeals.Where(p => p.Position.IsNorthOrSouth).ToList();
        var dealCount = playerDeals.Count;
        yield return new DealInfo { DealNumber = deal.Board, Dealer = deal.Dealer.ToString(), Vulnerability = deal.Vulnerability.ToString(), Results = playerDeals.GroupBy(p => p.Score).OrderByDescending(g => g.Key).Select(g => $"{g.Count()} {g.Key.ToString()} {(g.Select(d => d.ScoreMP).Distinct().Single() * (dealCount - 1) + 50) / dealCount:F2}%").ToList(), CardsBundle = CreateCardsBundle_(deal.Cards), Cards = string.Join(" - ", deal.Cards.North, deal.Cards.East, deal.Cards.South, deal.Cards.West) };
      }
    }

    private Bundle CreateCardsBundle_(ICards cards)
    {
      var bundle = new Bundle();
      var north = cards.North.ToString().Split(' ');
      bundle.PutString(DealInfo.NorthSpadesKey, north[1]);
      bundle.PutString(DealInfo.NorthHeartsKey, north[2]);
      bundle.PutString(DealInfo.NorthDiamondsKey, north[3]);
      bundle.PutString(DealInfo.NorthClubsKey, north[4]);
      var south = cards.South.ToString().Split(' ');
      bundle.PutString(DealInfo.SouthSpadesKey, south[1]);
      bundle.PutString(DealInfo.SouthHeartsKey, south[2]);
      bundle.PutString(DealInfo.SouthDiamondsKey, south[3]);
      bundle.PutString(DealInfo.SouthClubsKey, south[4]);
      var east = cards.East.ToString().Split(' ');
      bundle.PutString(DealInfo.EastSpadesKey, east[1]);
      bundle.PutString(DealInfo.EastHeartsKey, east[2]);
      bundle.PutString(DealInfo.EastDiamondsKey, east[3]);
      bundle.PutString(DealInfo.EastClubsKey, east[4]);
      var west = cards.West.ToString().Split(' ');
      bundle.PutString(DealInfo.WestSpadesKey, west[1]);
      bundle.PutString(DealInfo.WestHeartsKey, west[2]);
      bundle.PutString(DealInfo.WestDiamondsKey, west[3]);
      bundle.PutString(DealInfo.WestClubsKey, west[4]);

      return bundle;
    }
  }

  /// <summary>
  /// Info to display for each deal
  /// </summary>
  public class DealInfo
  {
    public const string NorthSpadesKey = "NorthSpadesKey";
    public const string NorthHeartsKey = "NorthHeartsKey";
    public const string NorthDiamondsKey = "NorthDiamondsKey";
    public const string NorthClubsKey = "NorthClubsKey";

    public const string SouthSpadesKey = "SouthSpadesKey";
    public const string SouthHeartsKey = "SouthHeartsKey";
    public const string SouthDiamondsKey = "SouthDiamondsKey";
    public const string SouthClubsKey = "SouthClubsKey";

    public const string EastSpadesKey = "EastSpadesKey";
    public const string EastHeartsKey = "EastHeartsKey";
    public const string EastDiamondsKey = "EastDiamondsKey";
    public const string EastClubsKey = "EastClubsKey";

    public const string WestSpadesKey = "WestSpadesKey";
    public const string WestHeartsKey = "WestHeartsKey";
    public const string WestDiamondsKey = "WestDiamondsKey";
    public const string WestClubsKey = "WestClubsKey";

    public int DealNumber { get; set; }
    public string Dealer { get; set; }
    public string Vulnerability { get; set; }
    public IList<string> Results { get; set; }
    public string Cards { get; set; }
    public Bundle CardsBundle { get; set; }
  }
}