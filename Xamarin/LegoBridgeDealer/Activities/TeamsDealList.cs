﻿// <copyright file="TeamsDealList.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

namespace LegoBridgeDealer.Activities
{
  using Android.App;

  /// <inheritdoc />
  [Activity(Label = "@string/DealList", ParentActivity = typeof(TeamsTournamentSelection))]
  internal class TeamsDealList : DealList { }
}