﻿// <copyright file="TournamentSelection.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

using System.IO;

namespace LegoBridgeDealer.Activities
{
  using System;
  using Android.Content;
  using Android.OS;
  using Android.Support.V7.App;
  using Android.Widget;

  /// <summary>
  /// Activity for selecting which tournament to play.
  /// </summary>
  internal abstract class TournamentSelection : AppCompatActivity
  {
    /// <inheritdoc />
    protected override void OnCreate(Bundle savedInstanceState)
    {
      base.OnCreate(savedInstanceState);

      var scrollView = new ScrollView(this);
      var layout = new LinearLayout(this) { Orientation = Orientation.Vertical };
      scrollView.AddView(layout);
      SetContentView(scrollView);

      foreach (var name in Assets.List(GetTournamentFolder_()))
      {
        var button = new Button(this)
        {
          Text = Path.GetFileNameWithoutExtension(name),
        };

        button.Click += (s, e) => ShowTournament_(name);
        layout.AddView(button);
      }
    }

    /// <summary>
    /// Gets the names of the available bridge tournaments the user can chose from.
    /// </summary>
    /// <returns>List of tournament names</returns>
    protected abstract string GetTournamentFolder_();

    /// <summary>
    /// Get the type of the deal list to create
    /// </summary>
    /// <returns>The appropriate type of deal list to use.</returns>
    protected abstract Type DealListType_();

    private void ShowTournament_(string name)
    {
      var intent = new Intent(this, DealListType_());
      intent.PutExtra(DealList.TournamentPbnPath, Path.Combine(GetTournamentFolder_(), name));
      StartActivity(intent);
    }
  }
}