﻿// <copyright file="TeamsTournamentSelection.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using Android.App;

namespace LegoBridgeDealer.Activities
{
  /// <summary>
  /// Tournaments for teams (IMP)
  /// </summary>
  [Activity(Label = "@string/TeamsTournamentSelection", ParentActivity = typeof(TournamentTypeSelection))]
  internal class TeamsTournamentSelection : TournamentSelection
  {
    /// <inheritdoc />
    protected override string GetTournamentFolder_()
    {
      return "TeamCompetitions";
    }

    /// <inheritdoc />
    protected override Type DealListType_()
    {
      return typeof(TeamsDealList);
    }
  }
}