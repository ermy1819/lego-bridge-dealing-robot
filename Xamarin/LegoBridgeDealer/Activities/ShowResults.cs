﻿// <copyright file="ShowResults.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

namespace LegoBridgeDealer.Activities
{
  using Android.App;
  using Android.OS;
  using Android.Support.V7.App;
  using Android.Widget;

  [Activity(Label = "@string/ShowResults")]
  internal class ShowResults : AppCompatActivity
  {
    public const string DealResults = "ShowResultsDealResult";

    protected override void OnCreate(Bundle savedInstanceState)
    {
      base.OnCreate(savedInstanceState);

      var scrollView = new ScrollView(this);
      var layout = new LinearLayout(this) { Orientation = Orientation.Vertical };
      scrollView.AddView(layout);
      SetContentView(scrollView);

      foreach (var result in Intent.GetStringArrayListExtra(DealResults))
      {
        var text = new TextView(this)
        {
          Text = result,
        };

        layout.AddView(text);
      }
    }
  }
}