﻿// <copyright file="TournamentTypeSelection.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;

namespace LegoBridgeDealer.Activities
{
  /// <summary>
  /// Main activity. Users selects tournament type (teams or matchpoints).
  /// </summary>
  [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
  internal class TournamentTypeSelection : AppCompatActivity
  {
    /// <inheritdoc />
    protected override void OnCreate(Bundle savedInstanceState)
    {
      base.OnCreate(savedInstanceState);

      SetContentView(Resource.Layout.TournamentTypeSelection);

      var pair = FindViewById<Button>(Resource.Id.pairButton);
      var team = FindViewById<Button>(Resource.Id.teamButton);

      pair.Click += (s, e) => OpenTournamentSelection_(typeof(MatchPointTournamentSelection));
      team.Click += (s, e) => OpenTournamentSelection_(typeof(TeamsTournamentSelection));
    }

    private void OpenTournamentSelection_(Type tournamentSelectionType)
    {
      var intent = new Intent(this, tournamentSelectionType);
      StartActivity(intent);
    }
  }
}