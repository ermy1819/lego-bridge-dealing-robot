﻿// <copyright file="MatchPointTournamentSelection.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using Android.App;

namespace LegoBridgeDealer.Activities
{
  /// <summary>
  /// Tournaments for pairs (Matchpoints)
  /// </summary>
  [Activity(Label = "@string/MatchPointTournamentSelection", ParentActivity = typeof(TournamentTypeSelection))]
  internal class MatchPointTournamentSelection : TournamentSelection
  {
    /// <inheritdoc />
    protected override string GetTournamentFolder_()
    {
      return "PairCompetitions";
    }

    /// <inheritdoc />
    protected override Type DealListType_()
    {
      return typeof(PairsDealList);
    }
  }
}