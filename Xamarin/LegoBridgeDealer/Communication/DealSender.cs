﻿// <copyright file="DealSender.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

namespace LegoBridgeDealer.Communication
{
  using System.Collections.Generic;
  using System.Linq;
  using System.Threading.Tasks;
  using Android.Bluetooth;
  using Android.OS;
  using LegoBridgeDealer.Activities;

  internal class DealSender
  {
    public async Task Send(DealInfo deal)
    {
      var bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
      var device = bluetoothAdapter.BondedDevices.Single(d => d.Name == "NXT");

      var uuids = device.GetUuids();
      var socket = device.CreateRfcommSocketToServiceRecord(uuids[0].Uuid);
      await socket.ConnectAsync();
      var bytesToSend = ConvertToByteArray_(deal.CardsBundle);
      await socket.OutputStream.WriteAsync(bytesToSend, 0, 52);
      var buffer = new byte[1];
      var response = await socket.InputStream.ReadAsync(buffer, 0, 1);
      socket.Close();

      if (response != 1 || buffer[0] != 1)
      {
        throw new Java.IO.IOException("Deal not accepted by NXT");
      }
    }

    private byte[] ConvertToByteArray_(Bundle dealCards)
    {
      var converted = new byte[52];
      ConvertToByteArray_(dealCards.GetString(DealInfo.NorthClubsKey), 0, 1, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.NorthDiamondsKey), 13, 1, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.NorthHeartsKey), 26, 1, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.NorthSpadesKey), 39, 1, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.EastClubsKey), 0, 2, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.EastDiamondsKey), 13, 2, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.EastHeartsKey), 26, 2, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.EastSpadesKey), 39, 2, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.SouthClubsKey), 0, 3, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.SouthDiamondsKey), 13, 3, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.SouthHeartsKey), 26, 3, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.SouthSpadesKey), 39, 3, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.WestClubsKey), 0, 4, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.WestDiamondsKey), 13, 4, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.WestHeartsKey), 26, 4, converted);
      ConvertToByteArray_(dealCards.GetString(DealInfo.WestSpadesKey), 39, 4, converted);

      return converted;
    }

    private static Dictionary<char, int> cardIndex_ = new Dictionary<char, int>
    {
      {'2', 0},
      {'3', 1},
      {'4', 2},
      {'5', 3},
      {'6', 4},
      {'7', 5},
      {'8', 6},
      {'9', 7},
      {'T', 8},
      {'J', 9},
      {'Q', 10},
      {'K', 11},
      {'A', 12},
    };

    private void ConvertToByteArray_(string suit, int offset, byte playerIndex, byte[] buffer)
    {
      foreach (char card in suit.Skip(1))
      {
        buffer[offset + cardIndex_[card]] = playerIndex;
      }
    }
  }
}
