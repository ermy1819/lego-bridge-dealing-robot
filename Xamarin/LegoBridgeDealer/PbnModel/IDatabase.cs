﻿using System.Collections.Generic;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel
{
  public interface IDatabase
  {
    void Persist(Root data);

    IEnumerable<Player> GetAllPlayers();

    IEnumerable<Event> GetAllEvents();

    IEnumerable<Game> GetGames(Event @event);

    IEnumerable<int> GetEventIdsForPlayers(IEnumerable<int> playerIds);
  }
}