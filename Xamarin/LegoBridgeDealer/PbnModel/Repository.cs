﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace LegoBridgeDealer.PbnModel
{
  [Serializable]
#pragma warning disable CA1710 // Identifiers should have correct suffix
  public class Repository<T> : Dictionary<int, T>
#pragma warning restore CA1710 // Identifiers should have correct suffix
    where T : Aggregate
  {
    private readonly Func<IEnumerable<T>> queryMethod_;

    public Repository()
    {      
    }

    public Repository(Func<IEnumerable<T>> queryMethod)
    {
      queryMethod_ = queryMethod;
    }

    protected Repository(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {      
    }

    public List<T> Fetch(bool addFetchedValuesToRepository)
    {
      if (queryMethod_ == null)
        throw new InvalidOperationException("Cannot fetch when no query method was passed to constructor");

      var items = queryMethod_().ToList();

      if (addFetchedValuesToRepository)
      {
        foreach (var item in items)
        {
          Add(item.Id, item);
        }        
      }

      return items;
    }
  }
}