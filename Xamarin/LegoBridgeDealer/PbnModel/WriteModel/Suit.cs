﻿namespace LegoBridgeDealer.PbnModel.WriteModel
{
  public enum Suit
  {
    Clubs = 0,
    Diamonds = 1,
    Hearts = 2,
    Spades = 3,
    NoTrump = 4,
  }
}