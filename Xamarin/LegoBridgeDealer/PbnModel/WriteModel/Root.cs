﻿using System.Collections.Generic;

namespace LegoBridgeDealer.PbnModel.WriteModel
{
  public class Root
  {
    private readonly IDatabase database_;

    public Root()
    {
      Games = new Repository<Game>();
      Events = new Repository<Event>();
      Players = new Repository<Player>();
    }

    public Root(IDatabase database)
    {
      database_ = database;
      Games = new Repository<Game>(FetchGames_);
      Events = new Repository<Event>(() => database_.GetAllEvents());
      Players = new Repository<Player>(() => database_.GetAllPlayers());
    }

    public Repository<Game> Games { get; }

    public Repository<Event> Events { get; }

    public Repository<Player> Players { get; }

    public void Persist(Root data)
    {
      database_.Persist(data);
    }

    public IEnumerable<int> FetchEventIdsForCurrentPlayers()
    {
      return database_.GetEventIdsForPlayers(Players.Keys);
    }

    private IEnumerable<Game> FetchGames_()
    {
      foreach (var eventsValue in Events.Values)
      {
        foreach (var game in database_.GetGames(eventsValue))
        {
          yield return game;
        }
      }
    }
  }
}
