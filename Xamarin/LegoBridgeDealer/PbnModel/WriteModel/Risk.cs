﻿namespace LegoBridgeDealer.PbnModel.WriteModel
{
  public enum Risk
  {
    Undoubled,

    Doubled,

    Redoubled,
  }
}