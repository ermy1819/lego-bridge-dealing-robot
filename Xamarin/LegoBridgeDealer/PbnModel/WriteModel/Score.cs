﻿using System.Linq;

namespace LegoBridgeDealer.PbnModel.WriteModel
{
  public struct Score : IValueObject
  {
    private static readonly PlayerDirection[] allowedForPlayer_ = new[] { PlayerDirection.Declarer, PlayerDirection.North, PlayerDirection.East, };
    private static readonly PlayerDirection[] allowedDeclarer_ = new[] { PlayerDirection.South, PlayerDirection.North, PlayerDirection.East, PlayerDirection.West, };

    public Score(int score, PlayerDirection forPlayer = PlayerDirection.Declarer)
    {
      //Assert.That(allowedForPlayer_.Contains(forPlayer));
      Value = score;
      ForPlayer = forPlayer;
    }

    public int Value { get; }
    public PlayerDirection ForPlayer { get; }

    public int GetScore(PlayerDirection forDeclarer)
    {
      //Assert.That(allowedDeclarer_.Contains(forDeclarer));
      if (ForPlayer == PlayerDirection.North && (forDeclarer == PlayerDirection.East || forDeclarer == PlayerDirection.West))
        return -Value;
      if (ForPlayer == PlayerDirection.East && (forDeclarer == PlayerDirection.North || forDeclarer == PlayerDirection.South))
        return -Value;
      return Value;
    }

    public override string ToString()
    {
      return $"<{Value}, {ForPlayer}";
    }
  }
}