﻿using System;

namespace LegoBridgeDealer.PbnModel.WriteModel
{
  public class Game : Aggregate
  {
    public DateTime Date { get; set; }

    public int Board { get; set; }

    public int WestId { get; set; }
    public int NorthId { get; set; }
    public int EastId { get; set; }
    public int SouthId { get; set; }

    public Vulnerability Vulnerable { get; set; }

    public string Deal { get; set; }

    public string Scoring { get; set; }

    public PlayerDirection Declarer { get; set; }
    public PlayerDirection Dealer { get; set; }

    public Contract Contract { get; set; }

    /// <summary>
    /// Gets or sets the number of tricks won by declarer (0-13).
    /// </summary>
    public int Result { get; set; }

    public Score Score { get; set; }
    public float? ScoreIMP { get; set; }
    public float? ScoreMP { get; set; }
    public float? ScorePercentage { get; set; }

    public string RawPbn { get; set; }

    public int EventId { get; set; }
  }
}