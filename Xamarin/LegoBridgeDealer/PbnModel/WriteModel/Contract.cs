﻿// <copyright file="Contract.cs" company="Erik Myklebust">
// Copyright (c) Erik Myklebust. All rights reserved.
// </copyright>

namespace LegoBridgeDealer.PbnModel.WriteModel
{
  public struct Contract : IValueObject
  {
    public Contract(int tricks, Suit suit, Risk risk)
    {
      Tricks = tricks;
      Suit = suit;
      Risk = risk;
    }

    public int Tricks { get; }
    public Suit Suit { get; }
    public Risk Risk { get; }

    public bool Pass => Tricks == 0;
  }
}