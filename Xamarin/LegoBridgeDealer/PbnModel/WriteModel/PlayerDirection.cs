﻿namespace LegoBridgeDealer.PbnModel.WriteModel
{
  public enum PlayerDirection
  {
    // Absolute positions
    North = 0,
    South = 2,
    East = 1,
    West = 3,

    // Positions by function
    Declarer = 10,
    Dummy = 11,
    LeftHandDefender = 12,
    RightHandDefender = 13,

    // Relative positions
    Me,
    Partner,
    LeftHandOpponent,
    RightHandOpponent,
  }
}