﻿using System;
using System.Linq;
using System.Text;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IHand
  {
    int? HighCardPoints { get; }

    int SuitLength(Suit suit);
    (int Spades, int Hearts, int Diamonds, int Clubs)? ExtraPoints(Suit trump, int partnerTrumpLength);
  }

  internal interface IHandFactory
  {
    IHand CreateHand(string hand);
  }

  internal class HandFactory : IHandFactory
  {
    internal ISuitFactory SuitFactory { get; set; } = new SuitFactory();

    public IHand CreateHand(string hand)
    {
      return new Hand_(hand, SuitFactory);
    }

    private class Hand_ : IHand
    {
      private readonly ISuitFactory suitFactory_;

      private readonly bool isWellFormed_;
      private readonly string spades_;
      private readonly string hearts_;
      private readonly string diamonds_;
      private readonly string clubs_;

      public Hand_(string hand, ISuitFactory suitFactory)
      {
        suitFactory_ = suitFactory;
        var suits = hand.Split('.');
        isWellFormed_ = suits.Length == 4;

        if (isWellFormed_)
        {
          spades_ = Sort_(suits[0]);
          hearts_ = Sort_(suits[1]);
          diamonds_ = Sort_(suits[2]);
          clubs_ = Sort_(suits[3]);
          HighCardPoints = hand.Count(c => c == 'A') * 4 + hand.Count(c => c == 'K') * 3 + hand.Count(c => c == 'Q') * 2
                           + hand.Count(c => c == 'J');
        }
      }

      public int? HighCardPoints { get; }

      public int SuitLength(Suit suit) => GetSuit_(suit)?.Length ?? 0;

      public (int Spades, int Hearts, int Diamonds, int Clubs)? ExtraPoints(Suit trump, int partnerTrumpLength)
      {
        if (!isWellFormed_)
          return null;

        var trumpSuit = GetSuit_(trump);
        if (trump == Suit.NoTrump || trumpSuit.Length + partnerTrumpLength < 8)
          return (0, 0, 0, 0);

        var trumpLengthPoints = Math.Max(0, partnerTrumpLength + trumpSuit.Length - 8);

        return (GetSuitExtraPoints_(trump, Suit.Spades) ?? trumpLengthPoints,
          GetSuitExtraPoints_(trump, Suit.Hearts) ?? trumpLengthPoints,
          GetSuitExtraPoints_(trump, Suit.Diamonds) ?? trumpLengthPoints,
          GetSuitExtraPoints_(trump, Suit.Clubs) ?? trumpLengthPoints);
      }

      public override string ToString()
      {
        return isWellFormed_ ? $"{HighCardPoints}HP: {suitFactory_.CreateSuit(Suit.Spades)}{spades_} {suitFactory_.CreateSuit(Suit.Hearts)}{hearts_} {suitFactory_.CreateSuit(Suit.Diamonds)}{diamonds_} {suitFactory_.CreateSuit(Suit.Clubs)}{clubs_}" : "-";
      }

      private static void AppendCard_(string suit, StringBuilder builder, string card)
      {
        if (suit.Contains(card)) builder.Append(card);
      }

      private int? GetSuitExtraPoints_(Suit trump, Suit suit)
      {
        if (trump == suit) return null;

        var suitCards = GetSuit_(suit);

        switch (suitCards.Length)
        {
          case 0: return 3;
          case 1:
            if (suitCards == "J") return 1;
            if (suitCards == "Q" || suitCards == "K") return 0;
            return 2;
          case 2: return suitCards == "AK" || !suitCards.Contains("Q") && !suitCards.Contains("J") ? 1 : 0;
          default: return 0;
        }
      }

      private string GetSuit_(Suit suit)
      {
        switch (suit)
        {
          case Suit.Clubs:
            return clubs_;
          case Suit.Diamonds:
            return diamonds_;
          case Suit.Hearts:
            return hearts_;
          case Suit.Spades:
            return spades_;
          case Suit.NoTrump:
            return null;
          default:
            throw new ArgumentOutOfRangeException(nameof(suit), suit, null);
        }
      }

      private string Sort_(string suit)
      {
        var builder = new StringBuilder();
        AppendCard_(suit, builder, "A");
        AppendCard_(suit, builder, "K");
        AppendCard_(suit, builder, "Q");
        AppendCard_(suit, builder, "J");
        AppendCard_(suit, builder, "T");
        AppendCard_(suit, builder, "9");
        AppendCard_(suit, builder, "8");
        AppendCard_(suit, builder, "7");
        AppendCard_(suit, builder, "6");
        AppendCard_(suit, builder, "5");
        AppendCard_(suit, builder, "4");
        AppendCard_(suit, builder, "3");
        AppendCard_(suit, builder, "2");

        return builder.ToString();
      }
    }
  }
}