﻿using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IEvent
  {
    string Name { get; }
  }

  internal interface IEventFactory
  {
    IEvent CreateEvent(Event @event);
  }

  internal class EventFactory : IEventFactory
  {
    public IEvent CreateEvent(WriteModel.Event @event)
    {
      return new Event(@event);
    }

    private class Event : IEvent
      {
      private readonly WriteModel.Event event_;

      public Event(WriteModel.Event @event)
      {
        event_ = @event;
      }

      public string Name => event_.Name;

      public override string ToString()
      {
        return Name;
      }
    }
  }
}