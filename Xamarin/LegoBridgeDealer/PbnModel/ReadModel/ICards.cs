﻿using System;
using System.Collections.Generic;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface ICards
  {
    IHand South { get; }
    IHand West { get; }
    IHand North { get; }
    IHand East { get; }

    int? NorthSouthTotalPoints(Suit trump);
    int? EastWestTotalPoints(Suit trump);
  }

  internal interface ICardsFactory
  {
    ICards CreateCards(string deal);
  }

  internal class CardsFactory : ICardsFactory
  {
    public IHandFactory HandFactory { get; set; } = new HandFactory();

    public ICards CreateCards(string deal)
    {
      return new Cards_(deal, HandFactory);
    }

    private class Cards_ : ICards
    {
      public Cards_(string deal, IHandFactory handFactory)
      {
        var hands = deal.Split(' ');
        var first = hands[0].Split(':');
        if (hands.Length != 4 || first.Length != 2)
        {
          South = handFactory.CreateHand("-");
          West = handFactory.CreateHand("-");
          North = handFactory.CreateHand("-");
          East = handFactory.CreateHand("-");
        }
        else
        {
          var values = new List<IHand>
                         {
                           handFactory.CreateHand(first[1]),
                           handFactory.CreateHand(hands[1]),
                           handFactory.CreateHand(hands[2]),
                           handFactory.CreateHand(hands[3]),
                         };
          int northIndex = first[0] == "E" ? 3 : first[0] == "S" ? 2 : first[0] == "W" ? 1 : 0;
          North = values[northIndex % 4];
          East = values[(northIndex + 1) % 4];
          South = values[(northIndex + 2) % 4];
          West = values[(northIndex + 3) % 4];
        }
      }

      public IHand South { get; }

      public IHand West { get; }

      public IHand North { get; }

      public IHand East { get; }

      public int? NorthSouthTotalPoints(Suit trump)
      {
        return SumPoints_(North, South, trump);
      }

      public int? EastWestTotalPoints(Suit trump)
      {
        return SumPoints_(East, West, trump);
      }

      private int? SumPoints_(IHand hand1, IHand hand2, Suit trump)
      {
        var extra1 = hand1.ExtraPoints(trump, hand2.SuitLength(trump));
        var extra2 = hand2.ExtraPoints(trump, hand1.SuitLength(trump));
        if (extra2 == null || extra1 == null) return null;

        return hand1.HighCardPoints + hand2.HighCardPoints + Math.Max(extra1.Value.Spades, extra2.Value.Spades)
               + Math.Max(extra1.Value.Hearts, extra2.Value.Hearts) + Math.Max(extra1.Value.Diamonds, extra2.Value.Diamonds)
               + Math.Max(extra1.Value.Clubs, extra2.Value.Clubs);
      }
    }
  }
}