﻿using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IContractFilter
  {
    void SetData(IContractFilterData dataContract);

    bool Excluded(IPlayerDeal deal);
  }

  internal class ContractFilter : IContractFilter
  {
    private IContractFilterData data_;

    public void SetData(IContractFilterData dataContract)
    {
      data_ = dataContract;
    }

    public bool Excluded(IPlayerDeal deal)
    {
      if (deal.Contract.IsPass)
      {
        if (!data_.Pass && (data_.Diamonds || data_.Hearts || data_.NoTrump
                                    || data_.Spades || data_.Clubs)) return true;

        if (!data_.Tricks0 && (data_.Tricks0 || data_.Tricks1 || data_.Tricks2 || data_.Tricks3
                                 || data_.Tricks4 || data_.Tricks5 || data_.Tricks6
                                 || data_.Tricks7 || data_.Game)) return true;
      }
      else
      {
        if (data_.Diamonds || data_.Hearts || data_.NoTrump || data_.Spades
            || data_.Clubs || data_.Pass)
        {
          if (deal.Contract.Suit.Suit == Suit.Clubs && !data_.Clubs) return true;
          if (deal.Contract.Suit.Suit == Suit.Diamonds && !data_.Diamonds) return true;
          if (deal.Contract.Suit.Suit == Suit.Hearts && !data_.Hearts) return true;
          if (deal.Contract.Suit.Suit == Suit.Spades && !data_.Spades) return true;
          if (deal.Contract.Suit.Suit == Suit.NoTrump && !data_.NoTrump) return true;
        }

        if (data_.Tricks0 || data_.Tricks1 || data_.Tricks2 || data_.Tricks3
            || data_.Tricks4 || data_.Tricks5 || data_.Tricks6 || data_.Tricks7 || data_.Game)
        {
          if (deal.Contract.Tricks == 1 && !data_.Tricks1) return true;
          if (deal.Contract.Tricks == 2 && !data_.Tricks2) return true;
          if (deal.Contract.Tricks == 6 && !data_.Tricks6) return true;
          if (deal.Contract.Tricks == 7 && !data_.Tricks7) return true;

          if (deal.Contract.IsGame && data_.Game)
          {
            // Include item
          }
          else
          {
            if (deal.Contract.Tricks == 3 && !data_.Tricks3) return true;
            if (deal.Contract.Tricks == 4 && !data_.Tricks4) return true;
            if (deal.Contract.Tricks == 5 && !data_.Tricks5) return true;
          }
        }
      }

      if (data_.Undoubled || data_.Doubled || data_.Redoubled)
      {
        if (deal.Contract.Risk.IsRedoubled && !data_.Redoubled) return true;
        if (deal.Contract.Risk.IsDoubled && !data_.Doubled) return true;
        if (!deal.Contract.Risk.IsDoubled && !deal.Contract.Risk.IsRedoubled && !data_.Undoubled) return true;
      }

      return false;
    }
  }
}