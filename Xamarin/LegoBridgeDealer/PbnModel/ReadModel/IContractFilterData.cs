﻿namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IContractFilterData
  {
    bool NoTrump { get; }
    bool Spades { get; }
    bool Hearts { get; }
    bool Diamonds { get; }
    bool Clubs { get; }
    bool Pass { get; }
    bool Tricks0 { get; }
    bool Tricks1 { get; }
    bool Tricks2 { get; }
    bool Tricks3 { get; }
    bool Tricks4 { get; }
    bool Tricks5 { get; }
    bool Tricks6 { get; }
    bool Tricks7 { get; }
    bool Game { get; }
    bool Undoubled { get; }
    bool Doubled { get; }
    bool Redoubled { get; }
  }
}