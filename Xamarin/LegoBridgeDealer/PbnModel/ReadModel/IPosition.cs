﻿using System;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  /// <summary>
  /// North, West, South, East
  /// </summary>
  public interface IPosition
  {
    bool IsDefending { get; }
    bool IsNorthOrSouth { get; }
    int SortIndex { get; }
  }

  internal interface IPositionFactory
  {
    IPosition CreatePosition(PlayerDirection position);
  }

  internal class PositionFactory : IPositionFactory
  {
    public IPosition CreatePosition(PlayerDirection position)
    {
      return new Position(position);
    }

    private class Position : IPosition
    {
      private readonly PlayerDirection position_;

      public Position(PlayerDirection position)
      {
        position_ = position;
      }

      public bool IsDefending
      {
        get
        {
          switch (position_)
          {
            case PlayerDirection.Declarer:
            case PlayerDirection.Dummy: return false;
            case PlayerDirection.LeftHandDefender:
            case PlayerDirection.RightHandDefender: return true;
            default: throw new InvalidOperationException();
          }
        }
      }

      public bool IsNorthOrSouth
      {
        get
        {
          switch (position_)
          {
            case PlayerDirection.North:
            case PlayerDirection.South: return true;
            case PlayerDirection.East:
            case PlayerDirection.West: return false;
            default: throw new InvalidOperationException();
          }
        }
      }

      public int SortIndex => (int)position_;

      public override string ToString()
      {
        switch (position_)
        {
          case PlayerDirection.LeftHandDefender: return "LHD";
          case PlayerDirection.RightHandDefender: return "RHD";
          case PlayerDirection.Me: return "You";
          case PlayerDirection.LeftHandOpponent: return "LHO";
          case PlayerDirection.RightHandOpponent: return "RHO";
          default: return position_.ToString();
        }
      }
    }
  }
}