﻿namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IResultFilter
  {
    void SetData(IResultFilterData dataResult);

    bool Excluded(IPlayerDeal deal);
  }

  internal class ResultFilter : IResultFilter
  {
    private IResultFilterData data_;

    public void SetData(IResultFilterData dataResult)
    {
      data_ = dataResult;
    }

    public bool Excluded(IPlayerDeal deal)
    {
      if (data_.PlayerAbsoluteMin.HasValue && data_.PlayerAbsoluteMin.Value > deal.Result.Tricks) return true;
      if (data_.PlayerAbsoluteMax.HasValue && data_.PlayerAbsoluteMax.Value < deal.Result.Tricks) return true;

      return false;
    }
  }
}