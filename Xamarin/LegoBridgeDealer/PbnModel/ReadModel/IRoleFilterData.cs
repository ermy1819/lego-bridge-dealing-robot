﻿namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IRoleFilterData
  {
    bool Declarer { get; set; }
    bool Lhd { get; set; }
    bool Dummy { get; set; }
    bool Rhd { get; set; }
    bool Dealer { get; set; }
    bool Second { get; set; }
    bool Third { get; set; }
    bool Fourth { get; set; }
  }
}