﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel.Services
{
  internal interface IFairBridgeScoreService
  {
    int ParScore(int totalPoints, Suit suit, bool vulnerable);
    int GetDealScore(int contractScore, int parScore);
    int GetFairBridgePar(IPosition player, IContract contract);
    int GetWallinPar(IPosition player);
  }

  internal interface IFairBridgeScoreServiceFactory
  {
    IFairBridgeScoreService CreateService(IDeal deal);
  }

  internal class FairBridgeScoreServiceFactory : IFairBridgeScoreServiceFactory
  {
    public IFairBridgeScoreService CreateService(IDeal deal)
    {
      return new FairBridgeScoreService_(deal);
    }

    private class FairBridgeScoreService_ : IFairBridgeScoreService
    {
      private static readonly
       ReadOnlyDictionary<int, (float Minus2, float Minus1, float Trick1, float Trick2, float Trick3, float Trick4,
         float Trick5, float Trick6, float Trick7)> tricksMade_
         = new ReadOnlyDictionary<int, (float Minus2, float Minus1, float Trick1, float Trick2, float Trick3, float
           Trick4, float Trick5, float Trick6, float Trick7)>(
           new Dictionary<int, (float Minus2, float Minus1, float Trick1, float Trick2, float Trick3, float Trick4,
             float Trick5, float Trick6, float Trick7)>
           {
              { 21, (0.10f, 0.30f, 0.30f, 0.20f, 0.10f, 0.00f, 0.00f, 0.00f, 0.00f) },
              { 22, (0.10f, 0.20f, 0.35f, 0.25f, 0.10f, 0.00f, 0.00f, 0.00f, 0.00f) },
              { 23, (0.00f, 0.20f, 0.20f, 0.20f, 0.35f, 0.05f, 0.00f, 0.00f, 0.00f) },
              { 24, (0.00f, 0.20f, 0.00f, 0.00f, 0.60f, 0.10f, 0.10f, 0.00f, 0.00f) },
              { 25, (0.00f, 0.20f, 0.00f, 0.00f, 0.20f, 0.60f, 0.00f, 0.00f, 0.00f) },
              { 26, (0.00f, 0.10f, 0.00f, 0.00f, 0.10f, 0.80f, 0.00f, 0.00f, 0.00f) },
              { 27, (0.00f, 0.10f, 0.00f, 0.00f, 0.00f, 0.60f, 0.30f, 0.00f, 0.00f) },
              { 28, (0.00f, 0.10f, 0.00f, 0.00f, 0.00f, 0.30f, 0.60f, 0.00f, 0.00f) },
              { 29, (0.00f, 0.10f, 0.00f, 0.00f, 0.00f, 0.10f, 0.70f, 0.10f, 0.00f) },
              { 30, (0.00f, 0.10f, 0.00f, 0.00f, 0.00f, 0.00f, 0.60f, 0.30f, 0.00f) },
              { 31, (0.00f, 0.20f, 0.00f, 0.00f, 0.00f, 0.00f, 0.50f, 0.30f, 0.00f) },
              { 32, (0.00f, 0.20f, 0.00f, 0.00f, 0.00f, 0.00f, 0.50f, 0.30f, 0.00f) },
              { 33, (0.00f, 0.20f, 0.00f, 0.00f, 0.00f, 0.00f, 0.30f, 0.50f, 0.00f) },
              { 34, (0.00f, 0.10f, 0.00f, 0.00f, 0.00f, 0.00f, 0.20f, 0.70f, 0.00f) },
              { 35, (0.00f, 0.10f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.90f, 0.00f) },
              { 36, (0.00f, 0.10f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.80f, 0.10f) },
              { 37, (0.00f, 0.10f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.50f, 0.40f) },
              { 38, (0.00f, 0.10f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.20f, 0.70f) },
              { 39, (0.00f, 0.10f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.90f) },
              { 40, (0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 1.00f) },
           });

      private static readonly
        ReadOnlyDictionary<(int Score, SuitType_ Suit), (float PartGame, float Game, float Slam, float GrandSlam)>
        bonusScore_
          = new ReadOnlyDictionary<(int Score, SuitType_ Suit), (float PartGame, float Game, float Slam, float GrandSlam)
          >(
            new Dictionary<(int Score, SuitType_ Suit), (float PartGame, float Game, float Slam, float GrandSlam)>
            {
              { (21, SuitType_.Minor), (0.60f, 0.00f, 0.00f, 0.00f) },
              { (22, SuitType_.Minor), (0.70f, 0.00f, 0.00f, 0.00f) },
              { (23, SuitType_.Minor), (0.80f, 0.00f, 0.00f, 0.00f) },
              { (24, SuitType_.Minor), (0.80f, 0.00f, 0.00f, 0.00f) },
              { (25, SuitType_.Minor), (0.80f, 0.00f, 0.00f, 0.00f) },
              { (26, SuitType_.Minor), (0.90f, 0.00f, 0.00f, 0.00f) },
              { (27, SuitType_.Minor), (0.70f, 0.20f, 0.00f, 0.00f) },
              { (28, SuitType_.Minor), (0.30f, 0.60f, 0.00f, 0.00f) },
              { (29, SuitType_.Minor), (0.10f, 0.75f, 0.05f, 0.00f) },
              { (30, SuitType_.Minor), (0.00f, 0.85f, 0.05f, 0.00f) },
              { (31, SuitType_.Minor), (0.00f, 0.60f, 0.20f, 0.00f) },
              { (32, SuitType_.Minor), (0.00f, 0.50f, 0.30f, 0.00f) },
              { (33, SuitType_.Minor), (0.00f, 0.30f, 0.50f, 0.00f) },
              { (34, SuitType_.Minor), (0.00f, 0.20f, 0.70f, 0.00f) },
              { (35, SuitType_.Minor), (0.00f, 0.00f, 0.90f, 0.00f) },
              { (36, SuitType_.Minor), (0.00f, 0.00f, 0.80f, 0.10f) },
              { (37, SuitType_.Minor), (0.00f, 0.00f, 0.50f, 0.40f) },
              { (38, SuitType_.Minor), (0.00f, 0.00f, 0.20f, 0.70f) },
              { (39, SuitType_.Minor), (0.00f, 0.00f, 0.00f, 0.90f) },
              { (40, SuitType_.Minor), (0.00f, 0.00f, 0.00f, 1.00f) },
              { (21, SuitType_.Major), (0.60f, 0.00f, 0.00f, 0.00f) },
              { (22, SuitType_.Major), (0.70f, 0.00f, 0.00f, 0.00f) },
              { (23, SuitType_.Major), (0.80f, 0.00f, 0.00f, 0.00f) },
              { (24, SuitType_.Major), (0.80f, 0.00f, 0.00f, 0.00f) },
              { (25, SuitType_.Major), (0.60f, 0.20f, 0.00f, 0.00f) },
              { (26, SuitType_.Major), (0.10f, 0.80f, 0.00f, 0.00f) },
              { (27, SuitType_.Major), (0.00f, 0.90f, 0.00f, 0.00f) },
              { (28, SuitType_.Major), (0.00f, 0.90f, 0.00f, 0.00f) },
              { (29, SuitType_.Major), (0.00f, 0.85f, 0.05f, 0.00f) },
              { (30, SuitType_.Major), (0.00f, 0.85f, 0.05f, 0.00f) },
              { (31, SuitType_.Major), (0.00f, 0.60f, 0.20f, 0.00f) },
              { (32, SuitType_.Major), (0.00f, 0.50f, 0.30f, 0.00f) },
              { (33, SuitType_.Major), (0.00f, 0.30f, 0.50f, 0.00f) },
              { (34, SuitType_.Major), (0.00f, 0.20f, 0.70f, 0.00f) },
              { (35, SuitType_.Major), (0.00f, 0.00f, 0.90f, 0.00f) },
              { (36, SuitType_.Major), (0.00f, 0.00f, 0.80f, 0.10f) },
              { (37, SuitType_.Major), (0.00f, 0.00f, 0.50f, 0.40f) },
              { (38, SuitType_.Major), (0.00f, 0.00f, 0.20f, 0.70f) },
              { (39, SuitType_.Major), (0.00f, 0.00f, 0.00f, 0.90f) },
              { (40, SuitType_.Major), (0.00f, 0.00f, 0.00f, 1.00f) },
              { (21, SuitType_.NoTrump), (0.60f, 0.00f, 0.00f, 0.00f) },
              { (22, SuitType_.NoTrump), (0.70f, 0.00f, 0.00f, 0.00f) },
              { (23, SuitType_.NoTrump), (0.80f, 0.00f, 0.00f, 0.00f) },
              { (24, SuitType_.NoTrump), (0.70f, 0.10f, 0.00f, 0.00f) },
              { (25, SuitType_.NoTrump), (0.20f, 0.60f, 0.00f, 0.00f) },
              { (26, SuitType_.NoTrump), (0.10f, 0.80f, 0.00f, 0.00f) },
              { (27, SuitType_.NoTrump), (0.00f, 0.90f, 0.00f, 0.00f) },
              { (28, SuitType_.NoTrump), (0.00f, 0.90f, 0.00f, 0.00f) },
              { (29, SuitType_.NoTrump), (0.00f, 0.85f, 0.05f, 0.00f) },
              { (30, SuitType_.NoTrump), (0.00f, 0.80f, 0.10f, 0.00f) },
              { (31, SuitType_.NoTrump), (0.00f, 0.60f, 0.20f, 0.00f) },
              { (32, SuitType_.NoTrump), (0.00f, 0.50f, 0.30f, 0.00f) },
              { (33, SuitType_.NoTrump), (0.00f, 0.30f, 0.50f, 0.00f) },
              { (34, SuitType_.NoTrump), (0.00f, 0.20f, 0.70f, 0.00f) },
              { (35, SuitType_.NoTrump), (0.00f, 0.00f, 0.90f, 0.00f) },
              { (36, SuitType_.NoTrump), (0.00f, 0.00f, 0.80f, 0.10f) },
              { (37, SuitType_.NoTrump), (0.00f, 0.00f, 0.50f, 0.40f) },
              { (38, SuitType_.NoTrump), (0.00f, 0.00f, 0.20f, 0.70f) },
              { (39, SuitType_.NoTrump), (0.00f, 0.00f, 0.00f, 0.90f) },
              { (40, SuitType_.NoTrump), (0.00f, 0.00f, 0.00f, 1.00f) },
            });

      private readonly IDeal deal_;
      private int? nsWallinPar_;
      private int? ewWallinPar_;

      public FairBridgeScoreService_(IDeal deal)
      {
        deal_ = deal;
      }

      private enum SuitType_
      {
        Minor,
        Major,
        NoTrump
      }

      public int GetDealScore(int contractScore, int parScore)
      {
        var sign = contractScore > parScore ? 1 : -1;
        var difference = Math.Abs(contractScore - parScore);
        int result;
        if (difference < 100)
          result = difference;
        else if (difference < 200)
          result = 100 + (9 * (difference - 100)) / 10;
        else if (difference < 300)
          result = 190 + (6 * (difference - 200)) / 10;
        else if (difference < 400)
          result = 250 + (4 * (difference - 300)) / 10;
        else if (difference < 500)
          result = 290 + (25 * (difference - 400)) / 100;
        else if (difference < 700)
          result = 315 + (2 * (difference - 500)) / 10;
        else if (difference < 1000)
          result = 355 + (15 * (difference - 700)) / 100;
        else
          result = 400 + (1 * (difference - 1000)) / 10;

        return result * sign;
      }

      public int GetFairBridgePar(IPosition player, IContract contract)
      {
        var northSouth = contract.Declarer.IsNorthOrSouth;
        var suit = contract.Suit.Suit;
        var strength = northSouth ? deal_.Cards.NorthSouthTotalPoints(suit) : deal_.Cards.EastWestTotalPoints(suit);
        if (strength == null)
          return 0;
        var noTrumpStrength = northSouth
          ? deal_.Cards.NorthSouthTotalPoints(Suit.NoTrump)
          : deal_.Cards.EastWestTotalPoints(Suit.NoTrump);
        //Assert.IsNotNull(noTrumpStrength);
        int par;

        if (noTrumpStrength < 18 || strength < 20)
        {
          var isVulnerable = deal_.Vulnerability.IsVulnerable(contract.Opponent);
          par = -ParScore(40 - noTrumpStrength.Value, Suit.NoTrump, isVulnerable);
        }
        else
        {
          var isVulnerable = deal_.Vulnerability.IsVulnerable(contract.Declarer);
          par = ParScore(strength.Value, suit, isVulnerable);
          if (contract.Suit.IsMinor)
          {
            par = Math.Max(par, ParScore(noTrumpStrength.Value, Suit.NoTrump, isVulnerable));
          }
        }

        if (player.IsNorthOrSouth ^ northSouth)
          return -par;
        return par;
      }

      public int GetWallinPar(IPosition player)
      {
        var isNorthSouth = player.IsNorthOrSouth;
        if (isNorthSouth)
        {
          return nsWallinPar_ ?? (nsWallinPar_ = ComputeWallinPar_(true)).Value;
        }

        return ewWallinPar_ ?? (ewWallinPar_ = ComputeWallinPar_(false)).Value;
      }

      public int ParScore(int totalPoints, Suit suit, bool vulnerable)
      {
        if (totalPoints <= 20)
          return 0;
        if (totalPoints > 40)
          totalPoints = 40;

        var suitType = ToSuitType_(suit);
        var tricks = tricksMade_[totalPoints];
        var bonus = bonusScore_[(totalPoints, suitType)];
        //Assert.That(
        //  tricks.Minus2 + tricks.Minus1 + tricks.Trick1 + tricks.Trick2 + tricks.Trick3 + tricks.Trick4 +
        //  tricks.Trick5 +
        //  tricks.Trick6 + tricks.Trick7, Is.EqualTo(1.0f));
        //Assert.That(tricks.Minus2 + tricks.Minus1 + bonus.PartGame + bonus.Game + bonus.Slam + bonus.GrandSlam, Is.EqualTo(1.0f));
        //Assert.That(bonus.GrandSlam, Is.LessThanOrEqualTo(tricks.Trick7));
        //Assert.That(bonus.Slam + bonus.GrandSlam, Is.LessThanOrEqualTo(tricks.Trick6 + tricks.Trick7));
        //switch (suitType)
        //{
        //  case SuitType_.Minor:
        //    Assert.That(bonus.Game + bonus.Slam + bonus.GrandSlam - 0.00001f, Is.LessThanOrEqualTo(tricks.Trick5 + tricks.Trick6 + tricks.Trick7));
        //    break;
        //  case SuitType_.Major:
        //    Assert.That(bonus.Game + bonus.Slam + bonus.GrandSlam - 0.00001f, Is.LessThanOrEqualTo(tricks.Trick4 + tricks.Trick5 + tricks.Trick6 + tricks.Trick7));
        //    break;
        //  case SuitType_.NoTrump:
        //    Assert.That(bonus.Game + bonus.Slam + bonus.GrandSlam - 0.00001f, Is.LessThanOrEqualTo(tricks.Trick4 + tricks.Trick5 + tricks.Trick6 + tricks.Trick7));
        //    break;
        //}

        var bonusScore = 50 * bonus.PartGame + bonus.GrandSlam * (vulnerable ? 2000 : 1300) +
                         bonus.Slam * (vulnerable ? 1250 : 800) + bonus.Game * (vulnerable ? 500 : 300);
        var scorePerTrick = suitType == SuitType_.Minor ? 20 : 30;
        var penaltyScore = vulnerable ? -100 : -50;
        var trickCorrection = suitType == SuitType_.NoTrump ? -1 : 0;
        var trickScore = tricks.Minus2 * 2 * penaltyScore +
                         tricks.Minus1 * penaltyScore +
                         (suitType == SuitType_.NoTrump ? 10 : 0) * (1 - tricks.Minus2 - tricks.Minus1) +
                         tricks.Trick1 * scorePerTrick +
                         tricks.Trick2 * (2 + trickCorrection) * scorePerTrick +
                         tricks.Trick3 * (3 + trickCorrection) * scorePerTrick +
                         tricks.Trick4 * (4 + trickCorrection) * scorePerTrick +
                         tricks.Trick5 * (5 + trickCorrection) * scorePerTrick +
                         tricks.Trick6 * 6 * scorePerTrick +
                         tricks.Trick7 * 7 * scorePerTrick;

        return Math.Max((int)(bonusScore + trickScore), 0);
      }

      private int ComputeWallinPar_(bool isNorthSouth)
      { 
        var nnt = deal_.Cards.NorthSouthTotalPoints(Suit.NoTrump);
        var nsp = deal_.Cards.NorthSouthTotalPoints(Suit.Spades);
        var nhe = deal_.Cards.NorthSouthTotalPoints(Suit.Hearts);
        var ndi = deal_.Cards.NorthSouthTotalPoints(Suit.Diamonds);
        var ncl = deal_.Cards.NorthSouthTotalPoints(Suit.Clubs);
        var ent = deal_.Cards.EastWestTotalPoints(Suit.NoTrump);
        var esp = deal_.Cards.EastWestTotalPoints(Suit.Spades);
        var ehe = deal_.Cards.EastWestTotalPoints(Suit.Hearts);
        var edi = deal_.Cards.EastWestTotalPoints(Suit.Diamonds);
        var ecl = deal_.Cards.EastWestTotalPoints(Suit.Clubs);
        if (nnt == null || nsp == null || nhe == null || ndi == null || ncl == null ||
            ent == null || esp == null || ehe == null || edi == null || ecl == null)
          return 0;

        var northVulnerable = deal_.Vulnerability.IsVulnerable(PlayerDirection.North);
        var eastVulnerable = deal_.Vulnerability.IsVulnerable(PlayerDirection.East);
        var northStrength = Math.Max(Math.Max(nsp.Value, nhe.Value), Math.Max(ndi.Value, ncl.Value));
        var eastStrength = Math.Max(Math.Max(esp.Value, ehe.Value), Math.Max(edi.Value, ecl.Value));
        var maxNorth = ParScore(nnt.Value, Suit.NoTrump, northVulnerable);
        if (nsp > nnt)
          maxNorth = Math.Max(maxNorth, ParScore(nsp.Value, Suit.Spades, northVulnerable));
        if (nhe > nnt)
          maxNorth = Math.Max(maxNorth, ParScore(nhe.Value, Suit.Hearts, northVulnerable));
        if (ndi > nnt)
          maxNorth = Math.Max(maxNorth, ParScore(ndi.Value, Suit.Diamonds, northVulnerable));
        if (ncl > nnt)
          maxNorth = Math.Max(maxNorth, ParScore(ncl.Value, Suit.Clubs, northVulnerable));
        var maxEast = ParScore(ent.Value, Suit.NoTrump, eastVulnerable);
        if (esp > ent)
          maxEast = Math.Max(maxEast, ParScore(esp.Value, Suit.Spades, eastVulnerable));
        if (ehe > ent)
          maxEast = Math.Max(maxEast, ParScore(ehe.Value, Suit.Hearts, eastVulnerable));
        if (edi > ent)
          maxEast = Math.Max(maxEast, ParScore(edi.Value, Suit.Diamonds, eastVulnerable));
        if (ecl > ent)
          maxEast = Math.Max(maxEast, ParScore(ecl.Value, Suit.Clubs, eastVulnerable));

        var eastPenalty = eastVulnerable ? 200 : 100;
        var northPenalty = northVulnerable ? 200 : 100;

        if (maxNorth > maxEast)
        {
          if (maxNorth > eastPenalty && northStrength - eastStrength < 3)
            maxNorth = eastPenalty;
          return isNorthSouth ? maxNorth : -maxNorth;
        }

        if (maxEast > maxNorth)
        {
          if (maxEast > northPenalty && eastStrength - northStrength < 3)
            maxEast = northPenalty;
          return isNorthSouth ? -maxEast : maxEast;
        }

        // Equal score. Highest suit wins
        if (maxNorth == 0)
          return 0;

        if (northStrength == nnt)
          return (isNorthSouth ? -1 : 1) * Math.Min(maxEast, northPenalty);
        if (eastStrength == ent)
          return (isNorthSouth ? 1 : -1) * Math.Min(maxNorth, eastPenalty);
        if (northStrength == nsp)
          return (isNorthSouth ? 1 : -1) * Math.Min(maxNorth, eastPenalty);
        if (eastStrength == esp)
          return (isNorthSouth ? -1 : 1) * Math.Min(maxEast, northPenalty);
        if (northStrength == nhe)
          return (isNorthSouth ? 1 : -1) * Math.Min(maxNorth, eastPenalty);
        if (eastStrength == ehe)
          return (isNorthSouth ? -1 : 1) * Math.Min(maxEast, northPenalty);
        if (northStrength == ndi)
          return (isNorthSouth ? 1 : -1) * Math.Min(maxNorth, eastPenalty);
        if (eastStrength == edi)
          return (isNorthSouth ? -1 : 1) * Math.Min(maxEast, northPenalty);

        // Only case we can get here is if one in each pair has six clubs and no matching suits
        return 0;
      }

      private SuitType_ ToSuitType_(Suit suit)
      {
        switch (suit)
        {
          case Suit.Clubs:
          case Suit.Diamonds:
            return SuitType_.Minor;
          case Suit.Hearts:
          case Suit.Spades:
            return SuitType_.Major;
          case Suit.NoTrump:
            return SuitType_.NoTrump;
          default:
            throw new ArgumentOutOfRangeException(nameof(suit), suit, null);
        }
      }
    }
  }
}