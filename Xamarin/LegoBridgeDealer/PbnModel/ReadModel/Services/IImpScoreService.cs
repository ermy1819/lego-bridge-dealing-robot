using System.Collections.Generic;
using System.Linq;

namespace LegoBridgeDealer.PbnModel.ReadModel.Services
{
  internal interface IImpScoreService
  {
    void Initialize(IEnumerable<(int Score, IPosition DirectionForScore)> rawScores);
    decimal GetAverageImp(int rawScore, IPosition directionForScore);
  }

  internal interface IImpScoreServiceFactory
  {
    IImpScoreService CreateService();
  }

  internal class ImpScoreServiceFactory : IImpScoreServiceFactory
  {
    public IImpScoreService CreateService()
    {
      return new ImpScoreService_();
    }

    private class ImpScoreService_ : IImpScoreService
    {
      private List<int> rawScores_;
      private Dictionary<int, decimal> scoreMap_;

      public void Initialize(IEnumerable<(int Score, IPosition DirectionForScore)> rawScores)
      {
        rawScores_ = rawScores.Select(r => ReverseScore_(r.DirectionForScore) ? -r.Score : r.Score).ToList();
      }

      public decimal GetAverageImp(int rawScore, IPosition directionForScore)
      {
        //Assert.IsNotNull(rawScores_);
        if (scoreMap_ == null) ComputeScores_();
        rawScore = ReverseScore_(directionForScore) ? -rawScore : rawScore;
        //Assert.That(scoreMap_.ContainsKey(rawScore));

        return ReverseScore_(directionForScore) ? -scoreMap_[rawScore] : scoreMap_[rawScore];
      }

      private void ComputeScores_()
      {
        scoreMap_ = new Dictionary<int, decimal>(rawScores_.Count);
        if (rawScores_.Count == 1)
        {
          scoreMap_.Add(rawScores_.Single(), 0m);
          return;
        }

        double divider = 1.0 / (rawScores_.Count - 1);
        foreach (var score in rawScores_.Distinct())
        {
          var imps = 0;

          foreach (var score2 in rawScores_)
          {
            int differ = score - score2;
            imps += GetImp_(differ);
          }

          scoreMap_.Add(score, (decimal)(imps * divider));
        }
      }

      private int GetImp_(int differ)
      {
        int imp = -24;
        if (differ > -4000) imp++;
        if (differ > -3500) imp++;
        if (differ > -3000) imp++;
        if (differ > -2500) imp++;
        if (differ > -2250) imp++;
        if (differ > -2000) imp++;
        if (differ > -1750) imp++;
        if (differ > -1500) imp++;
        if (differ > -1300) imp++;
        if (differ > -1100) imp++;
        if (differ > -900) imp++;
        if (differ > -750) imp++;
        if (differ > -600) imp++;
        if (differ > -500) imp++;
        if (differ > -430) imp++;
        if (differ > -370) imp++;
        if (differ > -320) imp++;
        if (differ > -270) imp++;
        if (differ > -220) imp++;
        if (differ > -170) imp++;
        if (differ > -130) imp++;
        if (differ > -90) imp++;
        if (differ > -50) imp++;
        if (differ > -20) imp++;
        if (differ >= 4000) imp++;
        if (differ >= 3500) imp++;
        if (differ >= 3000) imp++;
        if (differ >= 2500) imp++;
        if (differ >= 2250) imp++;
        if (differ >= 2000) imp++;
        if (differ >= 1750) imp++;
        if (differ >= 1500) imp++;
        if (differ >= 1300) imp++;
        if (differ >= 1100) imp++;
        if (differ >= 900) imp++;
        if (differ >= 750) imp++;
        if (differ >= 600) imp++;
        if (differ >= 500) imp++;
        if (differ >= 430) imp++;
        if (differ >= 370) imp++;
        if (differ >= 320) imp++;
        if (differ >= 270) imp++;
        if (differ >= 220) imp++;
        if (differ >= 170) imp++;
        if (differ >= 130) imp++;
        if (differ >= 90) imp++;
        if (differ >= 50) imp++;
        if (differ >= 20) imp++;

        return imp;
      }

      private bool ReverseScore_(IPosition directionForScore)
      {
        return !directionForScore.IsNorthOrSouth;
      }
    }
  }
}