﻿namespace LegoBridgeDealer.PbnModel.ReadModel.Services
{
  internal interface IBridgeScoreService
  {
    int Compute(IDealResult result, IContract contract);
  }

  internal class BridgeScoreService : IBridgeScoreService
  {
    public int Compute(IDealResult result, IContract contract)
    {
      if (contract.IsPass) return 0;
      if (result.IsDefeated) return contract.Risk.Cost(result.RelativeTricks, contract.IsVulnerable);
      return contract.ScoreBonus + contract.ScoreForTricks + result.RelativeTricks * (contract.Risk.ScorePerOverTrick(contract.IsVulnerable) ?? contract.Suit.ScorePerTrick);
    }
  }
}
