﻿using System.Collections.Generic;
using System.Linq;

namespace LegoBridgeDealer.PbnModel.ReadModel.Services
{
  internal interface IMatchPointScoreService
  {
    void Initialize(IEnumerable<(int Score, IPosition DirectionForScore)> rawScores);
    decimal GetPercentage(int rawScore, IPosition directionForScore);
  }

  internal interface IMatchPointScoreServiceFactory
  {
    IMatchPointScoreService CreateService();
  }

  internal class MatchPointScoreServiceFactory : IMatchPointScoreServiceFactory
  {
    public IMatchPointScoreService CreateService()
    {
      return new MatchPointScoreService_();
    }

    private class MatchPointScoreService_ : IMatchPointScoreService
    {
      private List<int> rawScores_;
      private Dictionary<int, decimal> scoreMap_;

      public void Initialize(IEnumerable<(int Score, IPosition DirectionForScore)> rawScores)
      {
        rawScores_ = rawScores.Select(r => ReverseScore_(r.DirectionForScore) ? -r.Score : r.Score).ToList();
      }

      public decimal GetPercentage(int rawScore, IPosition directionForScore)
      {
        //Assert.IsNotNull(rawScores_);
        if (scoreMap_ == null) ComputeScores_();
        rawScore = ReverseScore_(directionForScore) ? -rawScore : rawScore;
        //Assert.That(scoreMap_.ContainsKey(rawScore));

        return ReverseScore_(directionForScore) ? 100 - scoreMap_[rawScore] : scoreMap_[rawScore];
      }

      private void ComputeScores_()
      {
        scoreMap_ = new Dictionary<int, decimal>(rawScores_.Count);
        if (rawScores_.Count == 1)
        {
          scoreMap_.Add(rawScores_.Single(), 50m);
          return;
        }

        double divider = 50.0 / (rawScores_.Count - 1);
        int aggregatedCount = 0;
        foreach (var group in rawScores_.GroupBy(r => r).OrderBy(g => g.Key))
        {
          int points = aggregatedCount * 2 + group.Count() - 1;
          scoreMap_.Add(group.Key, (decimal)(points * divider));
          aggregatedCount += group.Count();
        }

        //Assert.That(aggregatedCount, Is.EqualTo(rawScores_.Count));
      }

      private bool ReverseScore_(IPosition directionForScore)
      {
        return !directionForScore.IsNorthOrSouth;
      }
    }
  }
}
