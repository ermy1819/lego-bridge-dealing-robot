﻿namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IResultFilterData
  {
    int? PlayerAbsoluteMin { get; }
    int? PlayerAbsoluteMax { get; }
    int? PlayerRelativeMin { get; }
    int? PlayerRelativeMax { get; }
    int? DeclarerAbsoluteMin { get; }
    int? DeclarerAbsoluteMax { get; }
    int? DeclarerRelativeMin { get; }
    int? DeclarerRelativeMax { get; }
  }
}