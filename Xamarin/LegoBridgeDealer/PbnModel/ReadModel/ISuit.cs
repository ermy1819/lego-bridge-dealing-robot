﻿using System.Collections.Generic;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface ISuit
  {
    int SortIndex { get; }

    int ScorePerTrick { get; }
    int ScoreForFirstTrick { get; }
    int TricksForGame { get; }
    Suit Suit { get; }
    bool IsMinor { get; }
    bool IsMajor { get; }
    bool IsNoTrump { get; }
  }

  internal interface ISuitFactory
  {
    ISuit CreateSuit(Suit suit);
  }

  internal class SuitFactory : ISuitFactory
  {
    public ISuit CreateSuit(Suit suit)
    {
      return new Suit_(suit);
    }

    private class Suit_ : ISuit
    {
      private static readonly Dictionary<Suit, string> symbols_ =
        new Dictionary<Suit, string>
          {
            { Suit.Clubs, "♣" },
            { Suit.Diamonds, "♦" },
            { Suit.Hearts, "♥" },
            { Suit.Spades, "♠" },
            { Suit.NoTrump, "NT" }
          };
      private readonly Suit suit_;

      public Suit_(Suit suit)
      {
        suit_ = suit;
      }

      public int SortIndex => (int)suit_;

      public int ScorePerTrick => IsMinor ? 20 : 30;
      public int ScoreForFirstTrick => IsNoTrump ? 40 : ScorePerTrick;
      public int TricksForGame => IsNoTrump ? 3 : IsMajor ? 4 : 5;
      public Suit Suit => suit_;

      public bool IsMinor => suit_ == Suit.Diamonds || suit_ == Suit.Clubs;
      public bool IsMajor => suit_ == Suit.Hearts || suit_ == Suit.Spades;
      public bool IsNoTrump => suit_ == Suit.NoTrump;

      public override string ToString()
      {
        return symbols_[suit_];
      }
    }
  }
}