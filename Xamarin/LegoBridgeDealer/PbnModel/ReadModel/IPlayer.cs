﻿using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IPlayer
  {
    string Name { get; }
  }

  public interface IPlayerFactory
  {
    IPlayer CreatePlayer(Player player);
  }

  internal class PlayerFactory : IPlayerFactory
  {
    public IPlayer CreatePlayer(WriteModel.Player player)
    {
      return new Player(player);
    }

    private class Player : IPlayer
    {
      private readonly WriteModel.Player player_;

      public Player(WriteModel.Player player)
      {
        player_ = player;
      }

      public string Name => player_.Name;

      public override string ToString()
      {
        return Name;
      }
    }
  }
}