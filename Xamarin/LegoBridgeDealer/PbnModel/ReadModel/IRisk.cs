using System;
using System.Collections.Generic;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IRisk
  {
    int RiskScoreBonus { get; }

    int TrickScoreMultiplier { get; }

    bool IsDoubled { get; }

    bool IsRedoubled { get; }

    int Cost(int relativeTricks, bool isVulnerable);

    int? ScorePerOverTrick(bool isVulnerable);
  }

  internal interface IRiskFactory
  {
    IRisk CreateRisk(Risk risk);
  }

  internal class RiskFactory : IRiskFactory
  {
    public IRisk CreateRisk(Risk risk)
    {
      return new Risk_(risk);
    }

    private class Risk_ : IRisk
    {
      private static readonly Dictionary<Risk, string> names_ =
        new Dictionary<Risk, string>
          {
            { Risk.Undoubled, string.Empty },
            { Risk.Doubled, "X" },
            { Risk.Redoubled, "XX" },
          };

      private readonly Risk risk_;

      public Risk_(Risk risk)
      {
        risk_ = risk;
      }

      public int RiskScoreBonus => SwitchOnRisk_(0, 50, 100);
      public int TrickScoreMultiplier => SwitchOnRisk_(1, 2, 4);

      public bool IsDoubled => risk_ == Risk.Doubled;

      public bool IsRedoubled => risk_ == Risk.Redoubled;

      public int? ScorePerOverTrick(bool isVulnerable) => SwitchOnRisk_<int?>(null, 100, 200) * (isVulnerable ? 2 : 1);

      public override string ToString()
      {
        return names_[risk_];
      }

      public int Cost(int relativeTricks, bool isVulnerable)
      {
        //Assert.That(relativeTricks, Is.LessThan(0));
        return SwitchOnRisk_<Func<int, bool, int>>(
          (r, v) => r * (v ? 100 : 50),
          CostDoubled_,
          (r, v) => CostDoubled_(r, v) * 2).Invoke(relativeTricks, isVulnerable);
      }

      private static int CostDoubled_(int relativeTricks, bool isVulnerable)
      {
        switch (relativeTricks)
        {
          case -1: return isVulnerable ? -200 : -100;
          case -2: return isVulnerable ? -500 : -300;
          case -3: return isVulnerable ? -800 : -500;
          default: return (isVulnerable ? -800 : -500) + 300 * (relativeTricks + 3);
        }
      }

      private T SwitchOnRisk_<T>(T undoubledValue, T doubledValue, T redoubledValue)
      {
        switch (risk_)
        {
          case Risk.Undoubled: return undoubledValue;
          case Risk.Doubled: return doubledValue;
          case Risk.Redoubled: return redoubledValue;
          default:
            //Assert.Fail($"invalid case {risk_}");
            return default(T);
        }
      }
    }
  }
}