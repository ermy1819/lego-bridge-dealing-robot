﻿namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IRoleFilter
  {
    void SetData(IRoleFilterData dataRole);
  }

  internal class RoleFilter : IRoleFilter
  {
    private IRoleFilterData data_;

    public void SetData(IRoleFilterData dataRole)
    {
      data_ = dataRole;
    }
  }
}