﻿using System.Linq;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IFilter
  {
    bool Excluded(IDeal deal);

    bool Excluded(IPlayerDeal deal);
  }

  public class Filter : IFilter
  {
    private readonly IContractFilter contract_;
    private readonly IResultFilter result_;
    private readonly IRoleFilter role_;
    private readonly IVulnerabilityFilter vulnerability_;

    public Filter(IContractFilter contract = null, IResultFilter result = null, IRoleFilter role = null, IVulnerabilityFilter vulnerability = null)
    {
      contract_ = contract ?? new ContractFilter();
      result_ = result ?? new ResultFilter();
      role_ = role ?? new RoleFilter();
      vulnerability_ = vulnerability ?? new VulnerabilityFilter();
    }

    public bool Excluded(IDeal deal)
    {
      return !deal.PlayerDeals.Any();
    }

    public bool Excluded(IPlayerDeal deal)
    {
      return false;
      //return contract_.Excluded(deal) || result_.Excluded(deal);
    }

    public void SetData(IFilterData filterData)
    {
      contract_.SetData(filterData.Contract);
      result_.SetData(filterData.Result);
      role_.SetData(filterData.Role);
      vulnerability_.SetData(filterData.Vulnerability);
    }
  }
}