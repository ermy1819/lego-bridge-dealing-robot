using System;
using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface ISelection
  {
    event EventHandler<EventArgs> Modified;
    IReadOnlyCollection<IDeal> Deals { get; }
    void Repopulate();
  }

  public interface ISelectionFactory
  {
    ISelection CreateSelection(Root root, IFilter filter);
  }

  public class SelectionFactory : ISelectionFactory
  {
    internal IDealFactory DealFactory { get; set; } = new DealFactory();

    public ISelection CreateSelection(Root root, IFilter filter)
    {
      return new Selection_(root, filter, DealFactory);
    }

    private class Selection_ : ISelection
    {
      private readonly Root root_;
      private readonly IFilter filter_;
      private readonly IDealFactory dealFactory_;

      private List<IDeal> deals_;

      public Selection_(Root root, IFilter filter, IDealFactory dealFactory)
      {
        root_ = root;
        filter_ = filter;
        dealFactory_ = dealFactory;
      }

      public event EventHandler<EventArgs> Modified;

      public IReadOnlyCollection<IDeal> Deals => deals_;

      public void Repopulate()
      {
        deals_ = new List<IDeal>();
        foreach (var games in root_.Games.Values.GroupBy(g => (g.Deal, g.EventId)))
        {
          var deal = dealFactory_.CreateDeal(games.ToList(), root_.Players.Values.ToList(), root_.Events[games.Key.Item2], filter_);
          if (!filter_.Excluded(deal))
            deals_.Add(deal);
        }

        Modified?.Invoke(this, new EventArgs());
      }
    }
  }
}