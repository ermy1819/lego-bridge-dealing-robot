﻿using System.Globalization;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IDealResult
  {
    int RelativeTricks { get; }
    int Tricks { get; }
    bool IsDefeated { get; }
  }

  internal interface IDealResultFactory
  {
    IDealResult CreateDealResult(int result, Contract contract);
  }

  internal class DealResultFactory : IDealResultFactory
  {
    public IDealResult CreateDealResult(int result, Contract contract)
    {
      return new DealResult_(result, contract);
    }

    private class DealResult_ : IDealResult
    {
      private readonly int result_;
      private readonly Contract contract_;

      public DealResult_(int result, Contract contract)
      {
        result_ = result;
        contract_ = contract;
      }

      public int RelativeTricks => result_ - 6 - contract_.Tricks;
      public int Tricks => result_;

      public bool IsDefeated => RelativeTricks < 0;

      public override string ToString()
      {
        return contract_.Pass ? "-" : RelativeTricks.ToString("+#;-#;=", CultureInfo.InvariantCulture);
      }
    }
  }
}