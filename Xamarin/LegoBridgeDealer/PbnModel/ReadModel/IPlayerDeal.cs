﻿using System;
using System.Globalization;
using LegoBridgeDealer.PbnModel.ReadModel.Services;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IPlayerDeal
  {
    IPlayer Player { get; }
    IDealResult Result { get; }
    int Score { get; }
    decimal ScoreMP { get; }
    decimal ScoreIMP { get; }
    int ParScoreFairBridge { get; }
    int ScoreFairBridge { get; }
    int ParScoreWallin { get; }
    int ScoreWallin { get; }
    IPosition Position { get; }
    IPosition Role { get; }
    IContract Contract { get; }
  }

  internal interface IPlayerDealFactory
  {
    IPlayerDeal CreatePlayerDeal(Game game, Player player, IMatchPointScoreService matchPointScoreService, IImpScoreService impScoreService, IFairBridgeScoreService fairBridgeScoreService);
  }

  internal class PlayerDealFactory : IPlayerDealFactory
  {
    internal IPlayerFactory PlayerFactory { get; set; } = new PlayerFactory();
    internal IPositionFactory PositionFactory { get; set; } = new PositionFactory();
    internal IDealResultFactory DealResultFactory { get; set; } = new DealResultFactory();
    internal IContractFactory ContractFactory { get; set; } = new ContractFactory();
    internal IBridgeScoreService BridgeScoreService { get; set; } = new BridgeScoreService();

    public IPlayerDeal CreatePlayerDeal(Game game, Player player, IMatchPointScoreService matchPointScoreService, IImpScoreService impScoreService, IFairBridgeScoreService fairBridgeScoreService)
    {
      return new PlayerDeal_(game, player, PlayerFactory, PositionFactory, DealResultFactory, ContractFactory, BridgeScoreService, matchPointScoreService, impScoreService, fairBridgeScoreService);
    }

    private class PlayerDeal_ : IPlayerDeal
    {
      private readonly Game game_;
      private readonly IMatchPointScoreService matchPointScoreService_;
      private readonly IImpScoreService impScoreService_;
      private readonly IFairBridgeScoreService fairBridgeScoreService_;

      public PlayerDeal_(Game game, Player player, IPlayerFactory playerFactory, IPositionFactory positionFactory, IDealResultFactory dealResultFactory, IContractFactory contractFactory, IBridgeScoreService bridgeScoreService, IMatchPointScoreService matchPointScoreService, IImpScoreService impScoreService, IFairBridgeScoreService fairBridgeScoreService)
      {
        game_ = game;
        matchPointScoreService_ = matchPointScoreService;
        impScoreService_ = impScoreService;
        fairBridgeScoreService_ = fairBridgeScoreService;
        Player = playerFactory.CreatePlayer(player ?? new Player { Id = game.NorthId, Name = game.NorthId.ToString(CultureInfo.InvariantCulture) });
        var matchingPosition = GetMatchingPosition_(game, player);
        Position = positionFactory.CreatePosition(matchingPosition);
        Role = positionFactory.CreatePosition(GetRelativePosition_(game.Declarer, matchingPosition));
        Result = dealResultFactory.CreateDealResult(game.Result, game.Contract);
        Contract = contractFactory.CreateContract(game.Contract, game.Declarer, game.Vulnerable);
        Score = (game_.Score.Value != 0 ? game_.Score.Value : bridgeScoreService.Compute(Result, Contract) ) * (Role.IsDefending ? -1 : 1);
      }

      public IPlayer Player { get; }
      public IDealResult Result { get; }
      public int Score { get; }
      public decimal ScoreMP
      {
        get
        {
          if (game_.ScorePercentage == null) return matchPointScoreService_.GetPercentage(Score, Position);
          if (Role.IsDefending) return new decimal(100 - game_.ScorePercentage.Value);
          return new decimal(game_.ScorePercentage.Value);
        }
      }

      public decimal ScoreIMP
      {
        get
        {
          if (game_.ScoreIMP == null) return decimal.Round(impScoreService_.GetAverageImp(Score, Position), 2);
          return new decimal(game_.ScoreIMP ?? 0) * (Role.IsDefending ? -1 : 1);
        }
      }

      public int ParScoreFairBridge => fairBridgeScoreService_.GetFairBridgePar(Position, Contract);
      public int ScoreFairBridge => fairBridgeScoreService_.GetDealScore(Score, ParScoreFairBridge);
      public int ParScoreWallin => fairBridgeScoreService_.GetWallinPar(Position);
      public int ScoreWallin => fairBridgeScoreService_.GetDealScore(Score, ParScoreWallin);

      public IPosition Position { get; }
      public IPosition Role { get; }
      public IContract Contract { get; }

      private PlayerDirection GetRelativePosition_(PlayerDirection declarer, PlayerDirection playerDirection)
      {
        switch (((int)declarer - (int)playerDirection + 4) % 4)
        {
          case 0:
            return PlayerDirection.Declarer;
          case 1:
            return PlayerDirection.LeftHandDefender;
          case 2:
            return PlayerDirection.Dummy;
          case 3:
            return PlayerDirection.RightHandDefender;
          default:
            throw new ArgumentOutOfRangeException(nameof(declarer), "Declarer must be fixed direction");
        }
      }

      private PlayerDirection GetMatchingPosition_(Game game, Player player)
      {
        if (player == null)
          return PlayerDirection.North;

        switch (player.Id)
        {
          case int i when i == game.NorthId:
            return PlayerDirection.North;
          case int i when i == game.WestId:
            return PlayerDirection.West;
          case int i when i == game.SouthId:
            return PlayerDirection.South;
          case int i when i == game.EastId:
            return PlayerDirection.East;
          default:
            throw new NotSupportedException("Incorrect player for game");
        }
      }
    }
  }
}