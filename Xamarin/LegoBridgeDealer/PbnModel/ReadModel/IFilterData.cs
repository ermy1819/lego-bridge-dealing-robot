﻿namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IFilterData
  {
    IContractFilterData Contract { get; }
    IResultFilterData Result { get; }
    IRoleFilterData Role { get; }
    IVulnerabilityFilterData Vulnerability { get; }
  }
}