﻿using System.Linq;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IContract
  {
    IPosition Declarer { get; }
    int SortIndex { get; }

    bool IsPass { get; }
    bool IsGame { get; }

    bool IsVulnerable { get; }

    IRisk Risk { get; }

    ISuit Suit { get; }

    int Tricks { get; }

    int ScoreBonus { get; }

    int ScoreForTricks { get; }
    IPosition Opponent { get; }
  }

  internal interface IContractFactory
  {
    IContract CreateContract(Contract contract, PlayerDirection declarer, Vulnerability vulnerable);
  }

  internal class ContractFactory : IContractFactory
  {
    internal ISuitFactory SuitFactory { get; set; } = new SuitFactory();
    internal IRiskFactory RiskFactory { get; set; } = new RiskFactory();
    internal IPositionFactory PositionFactory { get; set; } = new PositionFactory();

    public IContract CreateContract(Contract contract, PlayerDirection declarer, Vulnerability vulnerable)
    {
      return new Contract_(contract, declarer, vulnerable, SuitFactory, RiskFactory, PositionFactory);
    }

    private class Contract_ : IContract
    {
      private readonly Contract contract_;

      public Contract_(Contract contract, PlayerDirection declarer, Vulnerability vulnerable, ISuitFactory suitFactory, IRiskFactory riskFactory, IPositionFactory positionFactory)
      {
        Declarer = positionFactory.CreatePosition(declarer);
        Opponent = positionFactory.CreatePosition(Declarer.IsNorthOrSouth ? PlayerDirection.East : PlayerDirection.North);
        contract_ = contract;
        Suit = suitFactory.CreateSuit(contract.Suit);
        IsVulnerable = IsVulnerable_(declarer, vulnerable);
        Risk = riskFactory.CreateRisk(contract.Risk);
      }

      public IPosition Declarer { get; }

      public int SortIndex => contract_.Tricks * 100 + Suit.SortIndex * 10 + (int)contract_.Risk;

      public bool IsPass => contract_.Pass;

      public bool IsVulnerable { get; }

      public IRisk Risk { get; }

      public ISuit Suit { get; }

      public int Tricks => contract_.Tricks;

      public int ScoreBonus => Risk.RiskScoreBonus + (Tricks == 7 ? IsVulnerable ? 2000 : 1300 : Tricks == 6 ? IsVulnerable ? 1250 : 800 : IsGame ? IsVulnerable ? 500 : 300 : 50);

      public int ScoreForTricks => (Suit.ScoreForFirstTrick + Suit.ScorePerTrick * (Tricks - 1)) * Risk.TrickScoreMultiplier;
      public IPosition Opponent { get; }

      public bool IsGame => ScoreForTricks >= 100;

      public override string ToString()
      {
        return contract_.Pass ? "Pass" : contract_.Tricks + Suit.ToString() + Risk + " " + Declarer.ToString().First();
      }

      private bool IsVulnerable_(PlayerDirection declarer, Vulnerability vulnerable)
      {
        // TODO: Inject VeulenrabilityFactory and call IVulnaerability.IsVulnerable
        switch (vulnerable)
        {
          case Vulnerability.None: return false;
          case Vulnerability.NorthSouth: return declarer == PlayerDirection.North || declarer == PlayerDirection.South;
          case Vulnerability.EastWest: return declarer == PlayerDirection.East || declarer == PlayerDirection.West;
          case Vulnerability.All: return true;
        }

        return false;
      }
    }
  }
}