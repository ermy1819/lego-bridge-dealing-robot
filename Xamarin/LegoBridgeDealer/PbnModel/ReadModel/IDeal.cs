﻿using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel.ReadModel.Services;
using LegoBridgeDealer.PbnModel.WriteModel;

namespace LegoBridgeDealer.PbnModel.ReadModel
{
  public interface IDeal
  {
    IEvent Event { get; }
    IReadOnlyCollection<IPlayerDeal> PlayerDeals { get; }
    IContract MostFrequentContract { get; }
    ISuit MostFrequentSuit { get; }
    int NumberOfPairs { get; }
    ICards Cards { get; }
    IVulnerability Vulnerability { get; }
    PlayerDirection Dealer { get; }
    int Board { get; }

    /// <summary>
    /// Gets the most frequent score for NS.
    /// </summary>
    int MostFrequentScore { get; }
  }

  internal interface IDealFactory
  {
    IDeal CreateDeal(List<Game> games, List<Player> players, Event @event, IFilter filter);
  }

  internal class DealFactory : IDealFactory
  {
    internal IEventFactory EventFactory { get; set; } = new EventFactory();
    internal IPlayerDealFactory PlayerDealFactory { get; set; } = new PlayerDealFactory();
    internal IContractFactory ContractFactory { get; set; } = new ContractFactory();
    internal ISuitFactory SuitFactory { get; set; } = new SuitFactory();
    internal ICardsFactory CardsFactory { get; set; } = new CardsFactory();
    internal IVulnerabilityFactory VulnerabilityFactory { get; set; } = new VulnerabilityFactory();
    internal IMatchPointScoreServiceFactory MatchPointScoreServiceFactory { get; set; } = new MatchPointScoreServiceFactory();
    internal IImpScoreServiceFactory ImpScoreServiceFactory { get; set; } = new ImpScoreServiceFactory();
    internal IFairBridgeScoreServiceFactory FairBridgeScoreServiceFactory { get; set; } = new FairBridgeScoreServiceFactory();

    public IDeal CreateDeal(List<Game> games, List<Player> players, Event @event, IFilter filter)
    {
      return new Deal_(games, players, @event, filter, EventFactory, PlayerDealFactory, ContractFactory, SuitFactory, CardsFactory, VulnerabilityFactory, MatchPointScoreServiceFactory.CreateService(), ImpScoreServiceFactory.CreateService(), FairBridgeScoreServiceFactory);
    }

    private class Deal_ : IDeal
    {
      public Deal_(List<Game> games, List<Player> players, Event @event, IFilter filter, IEventFactory eventFactory, IPlayerDealFactory playerDealFactory, IContractFactory contractFactory, ISuitFactory suitFactory, ICardsFactory cardsFactory, IVulnerabilityFactory vulnerabilityFactory, IMatchPointScoreService matchPointScoreService, IImpScoreService impScoreService, IFairBridgeScoreServiceFactory fairBridgeScoreServiceFactory)
      {
        Event = eventFactory.CreateEvent(@event);
        var cards = games.Select(g => g.Deal).Distinct().Single();
        Cards = cardsFactory.CreateCards(cards);
        Dealer = games.Select(g => g.Dealer).Distinct().Single();
        Board = games.Select(g => g.Board).Distinct().Single();
        NumberOfPairs = games.Count * 2;
        Vulnerability = vulnerabilityFactory.CreateVulnerability(games.Select(g => g.Vulnerable).Distinct().Single());
        var allDeals = games.Select(g => playerDealFactory.CreatePlayerDeal(g, null, matchPointScoreService, impScoreService, null)).ToList();
        matchPointScoreService.Initialize(allDeals.Select(d => (d.Score, d.Position)));
        impScoreService.Initialize(allDeals.Select(d => (d.Score, d.Position)));
        var fairBridgeScoreService = fairBridgeScoreServiceFactory.CreateService(this);
        var gamePlayerPairs = games.SelectMany(g => players.Where(p => MatchesAnyPosition_(g, p)).Select(p => (g, p)));
        PlayerDeals = gamePlayerPairs.Select(gp => playerDealFactory.CreatePlayerDeal(gp.Item1, gp.Item2, matchPointScoreService, impScoreService, fairBridgeScoreService)).Where(p => !filter.Excluded(p)).ToList();

        var mostFrequentContract = games.GroupBy(g => (g.Contract, IsNorthSouth_(g.Declarer))).OrderByDescending(g => g.Count()).First();
        MostFrequentContract = contractFactory.CreateContract(mostFrequentContract.Key.Item1, mostFrequentContract.GroupBy(g => g.Declarer).OrderByDescending(g => g.Count()).First().Key, mostFrequentContract.First().Vulnerable);

        var mostFrequentSuit = games.GroupBy(g => g.Contract.Suit).OrderByDescending(g => g.Count()).First().Key;
        MostFrequentSuit = suitFactory.CreateSuit(mostFrequentSuit);

        MostFrequentScore = allDeals.GroupBy(d => d.Score * (d.Position.IsNorthOrSouth ? 1 : -1))
          .OrderByDescending(g => g.Count()).First().Key;
      }

      public IEvent Event { get; }
      public IReadOnlyCollection<IPlayerDeal> PlayerDeals { get; }
      public IContract MostFrequentContract { get; }
      public ISuit MostFrequentSuit { get; }
      public int NumberOfPairs { get; }
      public ICards Cards { get; }
      public IVulnerability Vulnerability { get; }
      public int MostFrequentScore { get; }
      public PlayerDirection Dealer { get; }
      public int Board { get; }

      private bool IsNorthSouth_(PlayerDirection playerDirection)
      {
        return playerDirection == PlayerDirection.North || playerDirection == PlayerDirection.South;
      }

      private bool MatchesAnyPosition_(Game game, Player player)
      {
        return game.NorthId == player.Id || game.EastId == player.Id || game.WestId == player.Id || game.SouthId == player.Id;
      }
    }
  }
}
