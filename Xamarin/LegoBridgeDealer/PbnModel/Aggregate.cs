﻿namespace LegoBridgeDealer.PbnModel
{
  /// <summary>
  /// Common base class for all aggregates.
  /// </summary>
  public abstract class Aggregate
  {
    public int Id { get; set; }
  }
}