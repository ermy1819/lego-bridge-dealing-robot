using System;
using LegoBridgeDealer.PbnModel.WriteModel;
using LegoBridgeDealer.PbnParser;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  internal class ScoreTableSecondPassFixture
  {
    [Test]
    public void ShouldVerifyThatFirstSegmentIsTaskName()
    {
      var root = new Root();
      root.Games.Add(0, new Game { Id = 0 });

      var sut = new ScoreTableSecondPass(root);
      Assert.Throws<AssertionException>(
        () => sut.Parse(0, new[] { "ParseScores", "2", "1", "3NT", "W", "5", "-200", "45" }));
    }

    [Test]
    public void ShouldVerifyThatThereAreEightSegments()
    {
      var root = new Root();
      root.Games.Add(0, new Game { Id = 0 });

      var sut = new ScoreTableSecondPass(root);
      Assert.Throws<AssertionException>(
        () => sut.Parse(0, new[] { "ScoreTable", "2", "1", "3NT", "W", "5", "-200", "45", "55" }));
    }

    [Test]
    public void ShouldAddNewGameWithDataFromSegments()
    {
      var root = new Root();
      root.Games.Add(0, new Game { Id = 0 });
      root.Players.Add(1, new Player { Id = 1 });
      root.Players.Add(2, new Player { Id = 2 });

      var sut = new ScoreTableSecondPass(root);
      sut.Parse(0, new[] { "ScoreTable", "2", "1", "3NT", "W", "5", "-200", "45" });

      var result = root.Games[1];
      Assert.That(result.Id, Is.EqualTo(1));
      Assert.That(result.NorthId, Is.EqualTo(2));
      Assert.That(result.SouthId, Is.EqualTo(2));
      Assert.That(result.WestId, Is.EqualTo(1));
      Assert.That(result.EastId, Is.EqualTo(1));
      Assert.That(result.Contract.Tricks, Is.EqualTo(3));
      Assert.That(result.Contract.Suit, Is.EqualTo(Suit.NoTrump));
      Assert.That(result.Contract.Risk, Is.EqualTo(Risk.Undoubled));
      Assert.That(result.Declarer, Is.EqualTo(PlayerDirection.West));
      Assert.That(result.Result, Is.EqualTo(5));
      Assert.That(result.Score.Value, Is.EqualTo(-200));
      Assert.That(result.Score.ForPlayer, Is.EqualTo(PlayerDirection.Declarer));
      Assert.That(result.ScorePercentage, Is.EqualTo(45));
    }

    [Test]
    public void ShouldDoNothingIfOnlyOnePlayerSpecified()
    {
      var root = new Root();
      root.Games.Add(0, new Game { Id = 0 });
      root.Players.Add(1, new Player { Id = 1 });
      root.Players.Add(2, new Player { Id = 2 });

      var sut = new ScoreTableSecondPass(root);
      sut.Parse(0, new[] { "ScoreTable", "2", "-", "3NT", "W", "5", "-200", "45" });
      sut.Parse(0, new[] { "ScoreTable", "-", "1", "Bye", "X", "-", "+.1", "N/A" });

      Assert.That(root.Games.Count, Is.EqualTo(1));
    }

    [Test]
    public void ShouldDoNothingIfNoContractSpecified()
    {
      var root = new Root();
      root.Games.Add(0, new Game { Id = 0 });
      root.Players.Add(1, new Player { Id = 1 });
      root.Players.Add(2, new Player { Id = 2 });

      var sut = new ScoreTableSecondPass(root);
      sut.Parse(0, new[] { "ScoreTable", "2", "1", "-", "-", "-", "A+", "45" });

      Assert.That(root.Games.Count, Is.EqualTo(1));
    }

    [Test]
    public void ShouldDoNothingIfScoreIsWeird()
    {
      var root = new Root();
      root.Games.Add(0, new Game { Id = 0 });
      root.Players.Add(1, new Player { Id = 1 });
      root.Players.Add(2, new Player { Id = 2 });

      var sut = new ScoreTableSecondPass(root);
      sut.Parse(0, new[] { "ScoreTable", "2", "1", "3NT", "E", "9", "400>a", "45" });

      Assert.That(root.Games.Count, Is.EqualTo(1));
    }

    [Test]
    public void ShouldUseReferencedGameAsTemplate()
    {
      var root = new Root();
      root.Games.Add(0, new Game { Id = 0 });
      var template = new Game { Id = 1, Board = 7, Date = DateTime.Today, Deal = "cards", EventId = 14, RawPbn = "Not according to standard", ScoreIMP = 17, ScoreMP = 12.3f, Scoring = "MP1", Vulnerable = Vulnerability.EastWest };
      root.Games.Add(1, template);
      root.Players.Add(1, new Player { Id = 1 });
      root.Players.Add(2, new Player { Id = 2 });

      var sut = new ScoreTableSecondPass(root);
      sut.Parse(1, new[] { "ScoreTable", "2", "1", "3NT", "W", "5", "-200", "45" });

      var result = root.Games[2];
      Assert.That(result.Id,         Is.EqualTo(2));
      Assert.That(result.Board,      Is.EqualTo(template.Board));
      Assert.That(result.Date,       Is.EqualTo(template.Date));
      Assert.That(result.Deal,       Is.EqualTo(template.Deal));
      Assert.That(result.EventId,    Is.EqualTo(template.EventId));
      Assert.That(result.RawPbn,     Is.EqualTo(template.RawPbn));
      Assert.That(result.ScoreIMP,   Is.EqualTo(template.ScoreIMP));
      Assert.That(result.ScoreMP,    Is.EqualTo(template.ScoreMP));
      Assert.That(result.Scoring,    Is.EqualTo(template.Scoring));
      Assert.That(result.Vulnerable, Is.EqualTo(template.Vulnerable));
    }

    [Test]
    public void ShouldBeAbleToHandlePass()
    {
      var root = new Root();
      root.Games.Add(0, new Game { Id = 0 });
      root.Players.Add(1, new Player { Id = 1 });
      root.Players.Add(2, new Player { Id = 2 });

      var sut = new ScoreTableSecondPass(root);
      sut.Parse(0, new[] { "ScoreTable", "2", "1", "Pass", "-", "-", "0", "45" });

      var result = root.Games[1];
      Assert.That(result.Id, Is.EqualTo(1));
      Assert.That(result.Contract.Pass, Is.EqualTo(true));
      Assert.That(result.Result, Is.EqualTo(0));
      Assert.That(result.Score.Value, Is.EqualTo(0));
      Assert.That(result.Score.ForPlayer, Is.EqualTo(PlayerDirection.Declarer));
    }

    [Test]
    public void ShouldBeAbleToHandleNonNumericPlayerIds()
    {
      var root = new Root();
      root.Games.Add(0, new Game { Id = 0 });
      root.Players.Add("A".GetHashCode(), new Player { Id = "A".GetHashCode() });
      root.Players.Add("B".GetHashCode(), new Player { Id = "B".GetHashCode() });

      var sut = new ScoreTableSecondPass(root);
      sut.Parse(0, new[] { "ScoreTable", "B", "A", "3NT", "W", "5", "-200", "45" });

      var result = root.Games[1];
      Assert.That(result.NorthId, Is.EqualTo("B".GetHashCode()));
      Assert.That(result.SouthId, Is.EqualTo("B".GetHashCode()));
      Assert.That(result.WestId, Is.EqualTo("A".GetHashCode()));
      Assert.That(result.EastId, Is.EqualTo("A".GetHashCode()));
    }
  }
}