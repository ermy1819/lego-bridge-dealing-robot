using System.Collections.Generic;
using LegoBridgeDealer.PbnModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using LegoBridgeDealer.PbnParser;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  internal class ReadTagFixture
  {
    [Test]
    public void ShouldFailAssertWhenStateIsWrong()
    {
      var deal = new Game();
      var @event = new Event();
      var state = new Stack<IParserState>();
      var sut = new ReadTag(new List<string>(), deal, @event, new Repository<Player>(), new Queue<(int GameId, string Line)>());
      state.Push(Substitute.For<IParserState>());

      Assert.Throws<AssertionException>(() => sut.ParseLine("text only", state));

      state.Push(sut);
      state.Push(Substitute.For<IParserState>());
      Assert.Throws<AssertionException>(() => sut.ParseLine("text only", state));
    }

    [Test]
    public void ShouldReturnNullWhenInputIsNullOrWhiteSpace()
    {
      var deal = new Game();
      var @event = new Event();
      var state = new Stack<IParserState>();
      var sut = new ReadTag(new List<string>(), deal, @event, new Repository<Player>(), new Queue<(int GameId, string Line)>());
      state.Push(sut);
      Assert.That(sut.ParseLine(null, state), Is.Null);
      Assert.That(sut.ParseLine(string.Empty, state), Is.Null);
      Assert.That(sut.ParseLine("  ", state), Is.Null);
      Assert.That(state.Pop, Is.SameAs(sut));
      Assert.That(state.Count, Is.Zero);
    }

    [TestCase("tag value", "value")]
    [TestCase("tag value1 value2", "value1 value2")]
    [TestCase("tag", null)]
    [TestCase("tag ", null)]
    public void ShouldPushReadTagValueStateWhenTagNameIsRead(string line, string result)
    {
      var deal = new Game();
      var @event = new Event();
      var state = new Stack<IParserState>();
      var completedTags = new List<string> { "irrelevant", "already", "read" };
      var sut = new ReadTag(completedTags, deal, @event, new Repository<Player>(), new Queue<(int GameId, string Line)>());
      state.Push(sut);
      Assert.That(sut.ParseLine(line, state), Is.EqualTo(result));
      Assert.That(completedTags, Has.Member("tag"));
      Assert.That(state.Pop, Is.TypeOf<ReadTagValue>());
      Assert.That(state.Count, Is.Zero);
    }

    [TestCase("tag value", "value")]
    [TestCase("tag value1 value2", "value1 value2")]
    [TestCase("tag", null)]
    [TestCase("tag ", null)]
    public void ShouldPushReadToEndOfTagStateWhenTagNameIsAlreadyCompleted(string line, string result)
    {
      var deal = new Game();
      var @event = new Event();
      var state = new Stack<IParserState>();
      var completedTags = new List<string> { "tag" };
      var sut = new ReadTag(completedTags, deal, @event, new Repository<Player>(), new Queue<(int GameId, string Line)>());
      state.Push(sut);
      Assert.That(sut.ParseLine(line, state), Is.EqualTo(result));
      Assert.That(completedTags, Has.Count.EqualTo(1));
      Assert.That(state.Pop, Is.TypeOf<ReadToEndOfTag>());
      Assert.That(state.Count, Is.Zero);
    }
  }
}