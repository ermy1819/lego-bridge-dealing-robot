﻿namespace LegoBridgeDealerTests.PbnParser
{
  internal partial class PbnParserFixture
  {
    private const string pbn21SpecificationExample_ =
      @"[Event ""International Amsterdam Airport Schiphol Bridgetournament""] 
[Site ""Amsterdam, The Netherlands NLD""]
[Date ""1995.06.10""] 
[Board ""1""] 
[West ""Podgor""] 
[North ""Westra""] 
[East ""Kalish""] 
[South ""Leufkens""] 
[Dealer ""N""] 
[Vulnerable ""None""] 
[Deal ""N:.63.AKQ987.A9732 A8654.KQ5.T.QJT6 J973.J98742.3.K4 KQT2.AT.J6542.85""] 
[Scoring ""IMP""] 
[Declarer ""S""] 
[Contract ""5HX""] 
[Result ""9""] 
{ 
                S 
                H 6 3 
                D A K Q 9 8 7 
                C A 9 7 3 2 
  S K Q 10 2                    S A 8 6 5 4  
  H A 10                        H K Q 5 
  D J 6 5 4 2                   D 10 
  C 8 5                         C Q J 10 6 
                S J 9 7 3 
                H J 9 8 7 4 2 
                D 3 
                C K 4 
} 
[Auction ""N""] 
1D      1S      3H =1= 4S 
4NT =2= X       Pass   Pass 
5C      X       5H     X 
Pass    Pass    Pass 
[Note ""1:non-forcing 6-9 points, 6-card""] 
[Note ""2:two colors: clubs and diamonds""] 
[Play ""W""]
SK =1= H3 S4 S3 
C5 C2 C6 CK 
S2 H6 S5 S7 
C8 CA CT C4 
D2 DA DT D3 
D4 DK H5 H7 
-  -  -  H2 
* [Note ""1:highest of series""] 
";

    private const string ruterTableStyleExample_ =
    @"[Event ""Söndagsbarro""]
  [Site ""BK S:t Erik, Sverige""]
  [Date ""2015.03.29""]
  [Board ""1""]
  [West """"]
  [North """"]
  [East """"]
  [South """"]
  [Dealer ""N""]
  [Vulnerable ""None""]
  [Deal ""N:AQ84.Q843.A54.AQ T.KJ5.KJ2.T76432 KJ632.T97.93.985 975.A62.QT876.KJ""]
  [Scoring ""MatchPoints;MP1""]
  [Declarer """"]
  [Contract """"]
  [Result """"]
  [Competition ""Pairs""]
  [EventDate ""2015.03.29""]
  [Annotator ""BK S:t Erik - Söndagsbarro""]
  [AnnotatorNA ""mail@sterik.a.se""]
  [Application ""Ruter - Read more at www.brenning.se""]
  [TotalScoreTable ""Rank\2R;PairId\2R;Table\2R;Direction\5R;TotalScoreMP\5R;TotalPercentage\5R;Names\46L;NrBoards\2R;HandicapPercentage1\4R;HandicapPercentage2\4R;ScoreHandicap\5R;Club\32L;MemberID1\7R;MemberID2\7R;Sex1\3R;Sex2\3R;RankWithoutHcp\2R;RankWithoutHcpTie\2R;TotalScoreMPWithoutHcp\5R;TotalPercentageWithoutHcp\5R""]
  1 10 10 ""N-S"" 361.0 62.68 ""Magnus Arve - Robert Thorstenson""             24 29.7 46.9  -3.0 ""BK S:t Erik""                    ""40423"" ""56827"" ""M"" ""M""  4  - 364.0 63.19
  2  4  4 ""N-S"" 354.5 61.55 ""Biörn Torbiörnsson - Måns Torbiörnsson""       24 31.2 49.9   3.5 ""BK S:t Erik""                     ""8462"" ""23007"" ""M"" ""M""  5  - 351.0 60.94
  3 13 13 ""N-S"" 353.1 61.30 ""Eva Wiklander - Ingrid Linnarsson""            24 26.2 40.0 -17.9 ""BK S:t Erik""                    ""17165"" ""23525"" ""W"" ""W""  1  - 371.0 64.41
  4 17 12 ""E-W"" 324.2 56.29 ""Kerstin Zetterberg - Nils Wikstad""            24 51.3 47.7  29.2 ""BK S:t Erik""                    ""81094"" ""93666"" ""W"" ""M"" 12 13 295.0 51.22
  5  5  5 ""E-W"" 318.8 55.34 ""Hans Nerell - Anders Björkman""                24 17.0 27.4 -49.2 ""Västra Bjäre BK - BK S:t Erik""  ""85810"" ""56099"" ""M"" ""M""  2  - 368.0 63.89
  6  8  8 ""E-W"" 316.2 54.90 ""Lars Thånell - Lars-Erik Rudström""            24 23.1 21.0 -49.8 ""BK Stjärnskotten - BK S:t Erik""  ""9236"" ""10725"" ""M"" ""M""  3  - 366.0 63.54
  7 16 13 ""E-W"" 310.9 53.98 ""Anita Brynje - Lars Nilsson""                  24 46.6 29.6  -3.4 ""BK S:t Erik""                    ""44476"" ""43950"" ""W"" ""M""  9  - 314.3 54.56
  8  9  9 ""E-W"" 310.4 53.89 ""Lennart Roth - Marie Roth""                    24 33.3 34.5 -15.6 ""BK S:t Erik""                    ""92055"" ""92056"" ""M"" ""W""  6  - 326.0 56.60
  9 19 10 ""E-W"" 310.3 53.87 ""Tor Simensen - Olle Fahlén""                   24 32.2 37.0 -13.7 ""BK S:t Erik""                    ""28600"" ""30826"" ""M"" ""M""  7  - 324.0 56.25
  10 11 11 ""E-W"" 305.2 52.99 ""Anders Hellström - Caroline Tesch""            24 31.5 38.2 -12.8 ""BK S:t Erik""                    ""83725"" ""80715"" ""M"" ""W""  8  - 318.0 55.21
  11 21  8 ""N-S"" 298.2 51.78 ""Maria Calissendorff - Annie Gahm""             24 31.3 42.7  -6.8 ""BK S:t Erik""                    ""79968"" ""32797"" ""W"" ""W"" 11  - 305.0 52.95
  12 23  6 ""E-W"" 297.9 51.72 ""Mona Hellingh - Gunilla Fredriksson""          24 32.8 37.4 -12.1 ""BK S:t Erik""                    ""22782"" ""15240"" ""W"" ""W"" 10  - 310.0 53.82
  13  3  3 ""N-S"" 292.6 50.81 ""Siv Holst - Hans Holst""                       24 40.1 40.4   2.6 ""BK S:t Erik""                    ""35492"" ""35493"" ""W"" ""M"" 14  - 290.0 50.35
  14  2  2 ""E-W"" 291.4 50.58 ""Birgit Franklin - Mari-Anne Elsasdotter""      24 37.8 38.3  -3.6 ""BK S:t Erik""                    ""56546"" ""89478"" ""W"" ""W"" 12 13 295.0 51.22
  15 18 11 ""N-S"" 286.0 49.64 ""Allan Stoltenborg - Lena Sandquist""           24 44.5 47.0  18.5 ""BK S:t Erik""                    ""46114"" ""44630"" ""M"" ""W"" 16  - 267.4 46.43
  16 26  3 ""E-W"" 285.7 49.60 ""Agneta Tjus-Hampf - Ulla Tjus""                24 52.0 52.0  36.6 ""BK S:t Erik""                    ""25625"" ""12005"" ""W"" ""W"" 19  - 249.1 43.25
  17 25  4 ""E-W"" 273.9 47.55 ""Birgitta Horn af Åminne - Elisabeth Lalin""    24 34.2 51.3   9.9 ""BK S:t Erik""                    ""49512"" ""35845"" ""W"" ""W"" 17  - 264.0 45.83
  18 24  5 ""N-S"" 273.1 47.41 ""Ann-Cathrin Svensson - Ann-Margreth Thornell"" 24 51.5 51.7  35.4 ""BK S:t Erik""                    ""43546"" ""56932"" ""W"" ""W"" 23  - 237.7 41.27
  19 12 12 ""N-S"" 271.8 47.18 ""Castor Mann - Williem Berner""                 24 44.5 52.0  25.8 ""BK S:t Erik""                    ""58464""       - ""M""   - 21  - 246.0 42.71
  20  6  6 ""N-S"" 267.6 46.45 ""Anna-Lena Björk - Dan Johansson""              24 51.6 46.2  27.6 ""BK S:t Erik""                    ""46251"" ""79807"" ""W"" ""M"" 22  - 240.0 41.67
  21  1  1 ""E-W"" 263.5 45.74 ""Gunvor Sjöström - Disa Nilsson""               24 51.8 51.8  36.0 ""BK S:t Erik""                    ""87917"" ""87918"" ""W"" ""W"" 25  - 227.4 39.48
  22 27  2 ""N-S"" 258.1 44.81 ""Mårten Hilding - Kerstin Grandell""            24 35.3 33.7 -13.9 ""BK S:t Erik""                    ""42670"" ""36056"" ""M"" ""W"" 15  - 272.0 47.22
  23 14 14 ""E-W"" 254.8 44.24 ""Nillan Essén - Magnus Backlung""               24 52.0 52.0  36.6 ""BK S:t Erik""                    ""59143"" ""59151"" ""W"" ""M"" 26  - 218.3 37.90
  24 28  1 ""N-S"" 228.6 39.69 ""Göran Wallgren - Ulla Wallgren""               24 31.1 34.0 -19.4 ""Västra Bjäre BK""                ""85991"" ""85992"" ""M"" ""W"" 20  - 248.0 43.06
  25 20  9 ""N-S"" 227.4 39.49 ""Stig Lanfelt - Göran Nilsson""                 24 40.5 21.4 -24.0 ""BK S:t Erik""                    ""10922""  ""3068"" ""M"" ""M"" 18  - 251.4 43.65
  26  7  7 ""E-W"" 200.9 34.87 ""Tomoko Hansen - Peter Flodqvist""              24 35.2 22.5 -30.1 ""Arla BK - BK S:t Erik""          ""84545"" ""14778"" ""W"" ""M"" 24  - 231.0 40.10
  27 22  7 ""N-S"" 195.2 33.89 ""Eva Klingspor - Agneta Seth""                  24 36.0 52.0  13.5 ""BK S:t Erik""                    ""26851"" ""93564"" ""W"" ""W"" 27  - 181.7 31.55
  [ScoreTable ""Table\2R;Round\1R;PairId_NS\2R;PairId_EW\2R;Contract\4L;Declarer\1R;Result\2R;Lead\3L;Score_NS\5R;Score_EW\6R;MP_NS\4R;MP_EW\4R;Percentage_NS\3R;Percentage_EW\3R""]
  11 1 18 11 3N W  5 H3  ""200""      - 24.0  0.0 100   0
  3 1  3 26 2S N 10 H5  ""170""      - 21.0  3.0  88  13
  10 1 10 19 3S S 10 CK  ""170""      - 21.0  3.0  88  13
  9 1 20  9 3S S  9 D8  ""140""      - 16.0  8.0  67  33
  8 1 21  8 3S S  9 D6  ""140""      - 16.0  8.0  67  33
  5 1 24  5 3S N  9 D2  ""140""      - 16.0  8.0  67  33
  6 1  6 23 1N N  8 ST  ""120""      - 11.0 13.0  46  54
  7 1 22  7 2N N  8 D2  ""120""      - 11.0 13.0  46  54
  4 1  4 25 2S N  8 DK  ""110""      -  6.0 18.0  25  75
  12 1 12 17 1H N  8 CT  ""110""      -  6.0 18.0  25  75
  13 1 13 16 1H N  8 ST  ""110""      -  6.0 18.0  25  75
  2 1 27  2 3H S  8 ST      -   ""50""  2.0 22.0   8  92
  1 1 28  1 1D   N  5 ST      -  ""100""  0.0 24.0   0 100
  14 1  - 14 -    -  - -   ""Bye""  ""Bye""    -  9.1   -  38

  [Event ""Söndagsbarro""]
  [Site ""BK S:t Erik, Sverige""]
  [Date ""2015.03.29""]
  [Board ""2""]
  [West """"]
  [North """"]
  [East """"]
  [South """"]
  [Dealer ""E""]
  [Vulnerable ""NS""]
  [Deal ""E:QJ874.KQT76.K7.A 52.832.A432.8653 K9.AJ.QT6.QJ9742 AT63.954.J985.KT""]
  [Scoring ""MatchPoints;MP1""]
  [Declarer """"]
  [Contract """"]
  [Result """"]
  [Competition ""Pairs""]
  [EventDate ""2015.03.29""]
  [Annotator ""BK S:t Erik - Söndagsbarro""]
  [AnnotatorNA ""mail@sterik.a.se""]
  [Application ""Ruter - Read more at www.brenning.se""]
  [ScoreTable ""Table\2R;Round\1R;PairId_NS\2R;PairId_EW\2R;Contract\4L;Declarer\1R;Result\2R;Lead\3L;Score_NS\5R;Score_EW\6R;MP_NS\4R;MP_EW\4R;Percentage_NS\3R;Percentage_EW\3R""]
  13 1 13 16 3N W  9 S6      -  ""400"" 23.0  1.0  96   4
  1 1 28  1 3N W  9 S3      -  ""400"" 23.0  1.0  96   4
  10 1 10 19 4S E 10 C5      -  ""420"" 20.0  4.0  83  17
  3 1  3 26 3N W 10 D5      -  ""430"" 15.0  9.0  63  38
  4 1  4 25 3N E 10 D2      -  ""430"" 15.0  9.0  63  38
  11 1 18 11 3N W 10 D9      -  ""430"" 15.0  9.0  63  38
  7 1 22  7 3N W 10 D8      -  ""430"" 15.0  9.0  63  38
  8 1 21  8 4S E 11 C5      -  ""450"" 10.0 14.0  42  58
  6 1  6 23 3N W 11 D5      -  ""460""  4.0 20.0  17  83
  12 1 12 14 3N E 11 D5      -  ""460""  4.0 20.0  17  83
  9 1 20  9 3N W 11 S2      -  ""460""  4.0 20.0  17  83
  5 1 24  5 3N W 11 D8      -  ""460""  4.0 20.0  17  83
  2 1 27  2 3N W 11 D8      -  ""460""  4.0 20.0  17  83
  14 1  - 17 -    -  - -   ""Bye""  ""Bye""    -  9.1   -  38
  ";
  }
}
