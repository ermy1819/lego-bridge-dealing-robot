using System.Collections.Generic;
using LegoBridgeDealer.PbnModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using LegoBridgeDealer.PbnParser;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  internal class ParsingDealFixture
  {
    [TearDown]
    public void TearDown()
    {
      LegoBridgeDealer.PbnParser.PbnParser.Clean();
    }

    [Test]
    public void ShouldFailAssertWhenStateIsWrong()
    {
      var deal = new Game();
      var @event = new Event();
      var state = new Stack<IParserState>();
      var sut = new ParsingDeal(deal, @event, new Repository<Player>(), new Queue<(int GameId, string Line)>());
      state.Push(Substitute.For<IParserState>());

      Assert.Throws<AssertionException>(() => sut.ParseLine("text only", state));

      state.Push(sut);
      state.Push(Substitute.For<IParserState>());
      Assert.Throws<AssertionException>(() => sut.ParseLine("text only", state));
    }

    [TestCase(null)]
    [TestCase("")]
    [TestCase(" ")]
    [TestCase("        ")]
    public void ShouldEndDelWhenLineIsNullOrWhiteSpace(string line)
    {
      var deal = new Game();
      var @event = new Event();
      var state = new Stack<IParserState>();
      var sut = new ParsingDeal(deal, @event, new Repository<Player>(), new Queue<(int GameId, string Line)>());
      var parserState = Substitute.For<IParserState>();
      state.Push(parserState);
      state.Push(sut);

      Assert.That(sut.ParseLine(line, state), Is.Null);
      Assert.That(state.Pop, Is.SameAs(parserState));
      Assert.That(state.Count, Is.Zero);
    }

    [TestCase("[", null)]
    [TestCase("  [", null)]
    [TestCase("[some text", "some text")]
    [TestCase(" [test]  [tag2]        ", "test]  [tag2]        ")]
    public void ShouldPushReadTagStateWhenLineStartsWithTag(string line, string result)
    {
      var deal = new Game();
      var @event = new Event();
      var state = new Stack<IParserState>();
      var sut = new ParsingDeal(deal, @event, new Repository<Player>(), new Queue<(int GameId, string Line)>());
      var parserState = Substitute.For<IParserState>();
      state.Push(parserState);
      state.Push(sut);

      Assert.That(sut.ParseLine(line, state), Is.EqualTo(result));
      Assert.That(state.Pop(), Is.TypeOf<ReadTag>());
      Assert.That(state.Pop, Is.SameAs(sut));
      Assert.That(state.Pop, Is.SameAs(parserState));
      Assert.That(state.Count, Is.Zero);
    }

    [TestCase("{", null)]
    [TestCase("  {", null)]
    [TestCase("{some text", "some text")]
    [TestCase(" {test}  [tag2]        ", "test}  [tag2]        ")]
    public void ShouldPushBlockCommentStateWhenLineStartsWithBlockComment(string line, string result)
    {
      var deal = new Game();
      var @event = new Event();
      var state = new Stack<IParserState>();
      var sut = new ParsingDeal(deal, @event, new Repository<Player>(), new Queue<(int GameId, string Line)>());
      var parserState = Substitute.For<IParserState>();
      state.Push(parserState);
      state.Push(sut);

      Assert.That(sut.ParseLine(line, state), Is.EqualTo(result));
      Assert.That(state.Pop(), Is.TypeOf<BlockComment>());
      Assert.That(state.Pop, Is.SameAs(sut));
      Assert.That(state.Pop, Is.SameAs(parserState));
      Assert.That(state.Count, Is.Zero);
    }
  }
}