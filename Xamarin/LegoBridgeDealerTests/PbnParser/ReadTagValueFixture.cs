﻿using System;
using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using LegoBridgeDealer.PbnParser;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  internal class ReadTagValueFixture
  {
    [TestCase(null, "", "]")]
    [TestCase(null, "value", "\"value\"]")]
    [TestCase(null, "va lue", "\"va lue\"]")]
    [TestCase(null, "value", "\"value\"   ]")]
    [TestCase("aaa", "value", "\"value\" ]aaa")]
    [TestCase("aaa ", "value", "\"value\" ]aaa ")]
    [TestCase("[Site \"Site\" ] ", "value", "\"value\" ][Site \"Site\" ] ")]
    [TestCase(null, "a\r\nb\r\nc ", "\"a", "b", "c \"]")]
    public void ShouldHandleCorrectlyFormattedStringInput(string returnValue, string parsedValue, params string[] input)
    {
      TestValidCase_("Event", (deal, @event, players) => @event.Name, returnValue, parsedValue, input);
    }

    [Test]
    public void ShouldFailAssertWhenCurrentStateIsIncorrect()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      var currentEvent = new Event();

      var sut = new ReadTagValue("Event", null, currentEvent, new Repository<Player>(), new Queue<(int GameId, string Line)>());

      Assert.Throws<AssertionException>(() => sut.ParseLine(" ", state));
    }

    [TestCase(1, 1, 1, "\"\"]")]
    [TestCase(1, 1, 1, "\"?\"]")]
    [TestCase(1, 1, 1, "\"#\"]")]
    [TestCase(1987, 08, 14, "\"1987.08.14\"]")]
    [TestCase(2000, 01, 01, "\"2000.??.??\"]")]
    public void ShouldBeAbleToParseDate(int year, int month, int day, params string[] input)
    {
      TestValidCase_("Date", (deal, @event, players) => @event.StartDate, null, new DateTime(year, month, day), input);
      TestValidCase_("Date", (deal, @event, players) => @event.EndDate, null, new DateTime(year, month, day), input);
      TestValidCase_("Date", (deal, @event, players) => deal.Date, null, new DateTime(year, month, day), input);
    }

    [Test]
    public void ShouldBeAbleToParseSite()
    {
      TestValidCase_("Site", (deal, @event, players) => @event.Site, null, "some location", "\"some location\"]");
    }

    [Test]
    public void ShouldBeAbleToParseBoard()
    {
      TestValidCase_("Board", (deal, @event, players) => deal.Board, null, 173, "\"173\"]");
    }

    [Test]
    public void ShouldBeAbleToParseWest()
    {
      TestValidCase_("West", (deal, @event, players) => players[deal.WestId].Name, null, "Player Name", "\"Player Name\"]");
    }

    [Test]
    public void ShouldBeAbleToParseNorth()
    {
      TestValidCase_("North", (deal, @event, players) => players[deal.NorthId].Name, null, "Player1; Player2", "\"Player1; Player2\"]");
    }

    [Test]
    public void ShouldBeAbleToParseEast()
    {
      TestValidCase_("East", (deal, @event, players) => players[deal.EastId].Name, null, "player a. NAME", "\"player a. NAME\"]");
    }

    [Test]
    public void ShouldBeAbleToParseSouth()
    {
      TestValidCase_("South", (deal, @event, players) => players[deal.SouthId].Name, null, "Player, Name", "\"Player, Name\"]");
    }

    [Test]
    public void ShouldBeAbleToParseVulnerable()
    {
      TestValidCase_("Vulnerable", (deal, @event, players) => deal.Vulnerable, null, Vulnerability.None, "\"None\"]");
      TestValidCase_("Vulnerable", (deal, @event, players) => deal.Vulnerable, null, Vulnerability.None, "\"Love\"]");
      TestValidCase_("Vulnerable", (deal, @event, players) => deal.Vulnerable, null, Vulnerability.None, "\"-\"]");
      TestValidCase_("Vulnerable", (deal, @event, players) => deal.Vulnerable, null, Vulnerability.NorthSouth, "\"NS\"]");
      TestValidCase_("Vulnerable", (deal, @event, players) => deal.Vulnerable, null, Vulnerability.EastWest, "\"EW\"]");
      TestValidCase_("Vulnerable", (deal, @event, players) => deal.Vulnerable, null, Vulnerability.All, "\"All\"]");
      TestValidCase_("Vulnerable", (deal, @event, players) => deal.Vulnerable, null, Vulnerability.All, "\"Both\"]");
    }

    [Test]
    public void ShouldBeAbleToParseDeal()
    {
      TestValidCase_("Deal", (deal, @event, players) => deal.Deal, null, "N:.63.AKQ987.A9732 A8654.KQ5.T.QJT6 J973.J98742.3.K4 KQT2.AT.J6542.85", "\"N:.63.AKQ987.A9732 A8654.KQ5.T.QJT6 J973.J98742.3.K4 KQT2.AT.J6542.85\"]");
    }

    [Test]
    public void ShouldBeAbleToParseScoring()
    {
      TestValidCase_("Scoring", (deal, @event, players) => deal.Scoring, null, "MatchPoints", "\"MatchPoints\"]");
    }

    [Test]
    public void ShouldBeAbleToParseDeclarer()
    {
      TestValidCase_("Declarer", (deal, @event, players) => deal.Declarer, null, PlayerDirection.North, "\"N\"]");
      TestValidCase_("Declarer", (deal, @event, players) => deal.Declarer, null, PlayerDirection.South, "\"S\"]");
      TestValidCase_("Declarer", (deal, @event, players) => deal.Declarer, null, PlayerDirection.East, "\"E\"]");
      TestValidCase_("Declarer", (deal, @event, players) => deal.Declarer, null, PlayerDirection.West, "\"W\"]");
    }

    [Test]
    public void ShouldBeAbleToParseContract()
    {
      TestValidCase_("Contract", (deal, @event, players) => deal.Contract, null, new Contract(), "\"Pass\"]");
      TestValidCase_("Contract", (deal, @event, players) => deal.Contract, null, new Contract(), "\"PAS\"]");
      TestValidCase_("Contract", (deal, @event, players) => deal.Contract, null, new Contract(1, Suit.Hearts, Risk.Undoubled), "\"1H\"]");
      TestValidCase_("Contract", (deal, @event, players) => deal.Contract, null, new Contract(2, Suit.Spades, Risk.Doubled), "\"2SX\"]");
      TestValidCase_("Contract", (deal, @event, players) => deal.Contract, null, new Contract(3, Suit.Clubs, Risk.Redoubled), "\"3CXX\"]");
      TestValidCase_("Contract", (deal, @event, players) => deal.Contract, null, new Contract(4, Suit.Diamonds, Risk.Undoubled), "\"4D\"]");
      TestValidCase_("Contract", (deal, @event, players) => deal.Contract, null, new Contract(5, Suit.NoTrump, Risk.Doubled), "\"5NTX\"]");
      TestValidCase_("Contract", (deal, @event, players) => deal.Contract, null, new Contract(6, Suit.Hearts, Risk.Redoubled), "\"6HXX\"]");
      TestValidCase_("Contract", (deal, @event, players) => deal.Contract, null, new Contract(7, Suit.Spades, Risk.Undoubled), "\"7S\"]");
      TestValidCase_("Contract", (deal, @event, players) => deal.Contract, null, new Contract(-1, default(Suit), default(Risk)), "\"\"]");
    }

    [Test]
    public void ShouldBeAbleToParseResult()
    {
      TestValidCase_("Result", (deal, @event, players) => deal.Result, null, 7, "\"7\"]");
      TestValidCase_("Result", (deal, @event, players) => deal.Result, null, 10, "\"^10\"]");
      TestValidCase_("Result", (deal, @event, players) => deal.Result, null, 0, "\"5*\"]");
      TestValidCase_("Result", (deal, @event, players) => deal.Result, null, 0, "\"?\"]");
      TestValidCase_("Result", (deal, @event, players) => deal.Result, null, 0, "\"\"]");
    }

    [Test]
    public void ShouldBeAbleToParseScore()
    {
      TestValidCase_("Score", (deal, @event, players) => deal.Score, null, new Score(620), "\"620\"]");
      TestValidCase_("Score", (deal, @event, players) => deal.Score, null, new Score(-100), "\"-100\"]");
      TestValidCase_("Score", (deal, @event, players) => deal.Score, null, new Score(620, PlayerDirection.North), "\"NS620\"]");
      TestValidCase_("Score", (deal, @event, players) => deal.Score, null, new Score(-620, PlayerDirection.East), "\"EW-620\"]");
      TestValidCase_("Score", (deal, @event, players) => deal.Score, null, new Score(-620, PlayerDirection.North), "\"NS-620EW620\"]");
      TestValidCase_("Score", (deal, @event, players) => deal.Score, null, new Score(620, PlayerDirection.East), "\"EW620NS-620\"]");
    }

    [Test]
    public void ShouldBeAbleToParseScoreIMP()
    {
      TestValidCase_("ScoreIMP", (deal, @event, players) => deal.ScoreIMP, null, -6.5, "\"-6.5\"]");
    }

    [Test]
    public void ShouldBeAbleToParseScoreMP()
    {
      TestValidCase_("ScoreMP", (deal, @event, players) => deal.ScoreMP, null, 18.5, "\"18.5\"]");
    }

    [Test]
    public void ShouldBeAbleToParseScorePercentage()
    {
      TestValidCase_("ScorePercentage", (deal, @event, players) => deal.ScorePercentage, null, 88.5, "\"88.5\"]");
    }

    [Test]
    public void ShouldPushReadTotalScoreTableStateWhenTagNameIsTotalScoreTable()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      var currentEvent = new Event();
      var currentDeal = new Game();
      var players = new Repository<Player>();
      var sut = new ReadTagValue("TotalScoreTable", currentDeal, currentEvent, players, new Queue<(int GameId, string Line)>());
      state.Push(sut);

      var returned2 = sut.ParseLine("\"TableHeader\\4R\"]", state);
      Assert.That(returned2, Is.EqualTo(null));
      Assert.That(state.Count, Is.EqualTo(2));
      Assert.That(state.Pop(), Is.TypeOf<ParseTotalScoreTable>());
      Assert.That(state.Pop(), Is.SameAs(previousState));
    }

    [Test]
    public void ShouldPushReadScoreTableStateWhenTagNameIsScoreTable()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      var currentEvent = new Event();
      var currentDeal = new Game();
      var players = new Repository<Player>();
      var sut = new ReadTagValue("ScoreTable", currentDeal, currentEvent, players, new Queue<(int GameId, string Line)>());
      state.Push(sut);

      var returned2 = sut.ParseLine("\"TableHeader\\4R\"]", state);
      Assert.That(returned2, Is.EqualTo(null));
      Assert.That(state.Count, Is.EqualTo(2));
      Assert.That(state.Pop(), Is.TypeOf<ParseScoreTable>());
      Assert.That(state.Pop(), Is.SameAs(previousState));
    }

    private static void TestValidCase_(string tagName, Func<Game, Event, Repository<Player>, object> modelValue, string returnValue, object parsedValue, params string[] input)
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      var currentEvent = new Event();
      var currentDeal = new Game();
      var players = new Repository<Player>();
      var sut = new ReadTagValue(tagName, currentDeal, currentEvent, players, new Queue<(int GameId, string Line)>());
      state.Push(sut);

      foreach (var line in input.Take(input.Length - 1))
      {
        var returned = sut.ParseLine(line, state);
        Assert.That(returned, Is.Null);
        Assert.That(state.Count, Is.EqualTo(2));
        Assert.That(state.Peek(), Is.SameAs(sut));
        Assert.That(currentEvent.Name, Is.Null);
      }

      var returned2 = sut.ParseLine(input.Last(), state);
      Assert.That(returned2, Is.EqualTo(returnValue));
      Assert.That(state.Count, Is.EqualTo(1));
      Assert.That(state.Peek(), Is.SameAs(previousState));
      Assert.That(modelValue(currentDeal, currentEvent, players), Is.EqualTo(parsedValue));
    }
  }
}