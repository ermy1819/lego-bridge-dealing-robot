using System.Collections.Generic;
using LegoBridgeDealer.PbnParser;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  internal class ReadToEndOfTagFixture
  {
    [Test]
    public void ShouldReturnNullWhenNoEndCommentOnLine()
    {
      var state = new Stack<IParserState>();
      var sut = new ReadToEndOfTag();
      state.Push(sut);

      Assert.That(sut.ParseLine("text only", state), Is.Null);
      Assert.That(sut.ParseLine("\\{[}():,;:,a %&/@!\"'<>", state), Is.Null);
      Assert.That(state.Pop(), Is.SameAs(sut));
      Assert.That(state.Count, Is.Zero);
    }

    [Test]
    public void ShouldReturnNullAndPopStateWhenLineEndsWithEndOfComment()
    {
      var state = new Stack<IParserState>();
      var sut = new ReadToEndOfTag();
      state.Push(sut);

      Assert.That(sut.ParseLine("text only]", state), Is.Null);
      Assert.That(state.Count, Is.Zero);
    }

    [Test]
    public void ShouldReturnNullAndPopStateWhenLineEndsWithEndOfCommentAndWhiteSpaces()
    {
      var state = new Stack<IParserState>();
      var sut = new ReadToEndOfTag();
      state.Push(sut);

      Assert.That(sut.ParseLine("text only]   ", state), Is.Null);
      Assert.That(state.Count, Is.Zero);
    }

    [Test]
    public void ShouldReturnRestOfLineWhenEndOfCommentIsInMiddleOfLine()
    {
      var state = new Stack<IParserState>();
      var sut = new ReadToEndOfTag();
      state.Push(sut);

      Assert.That(sut.ParseLine("text]only]   ", state), Is.EqualTo("only]   "));
      Assert.That(state.Count, Is.Zero);
    }

    [Test]
    public void ShouldFailAssertWhenStateIsWrong()
    {
      var state = new Stack<IParserState>();
      var sut = new ReadToEndOfTag();
      state.Push(Substitute.For<IParserState>());

      Assert.Throws<AssertionException>(() => sut.ParseLine("text only", state));

      state.Push(sut);
      state.Push(Substitute.For<IParserState>());
      Assert.Throws<AssertionException>(() => sut.ParseLine("text only", state));
    }
  }
}