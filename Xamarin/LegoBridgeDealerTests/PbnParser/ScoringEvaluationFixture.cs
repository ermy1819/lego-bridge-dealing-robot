﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  [Category("IntegrationTest")]
  internal class ScoringEvaluationFixture
  {
    // This file is "tests" for evaluating scoring systems. Temporary changes to check different configurations are expected.
    // It makes sense to have commented out code in this file to more quickly switch between what is checked.
    #pragma warning disable S125 // Sections of code should not be "commented out"
    [Test]
    public void FindParTableBasedOnPbnData()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();

      var statistics =
        new Dictionary<(Suit Suit, int Strength, bool Zon), Dictionary<int, (int Frequency, int Score)>>();

      var numDeals = 0;
      var numPlayerDeals = 0;

      var numPass = 0;
      var numReallyBad = 0;

      foreach (var file in Directory.EnumerateFiles(TestContext.CurrentContext.TestDirectory + @"\..\..\TestData"))
      {
        if (!int.TryParse(Path.GetFileNameWithoutExtension(file), out _))
          continue;

        ////TestContext.Progress.WriteLine(file);
        using (var fileStream = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
          var result = sut.Parse(new StreamReader(fileStream));
          var model = new SelectionFactory().CreateSelection(result, Substitute.For<IFilter>());
          model.Repopulate();
          foreach (var deal in model.Deals)
          {
            ++numDeals;
            foreach (var playerDeal in deal.PlayerDeals)
            {
              if (playerDeal.Role.IsDefending)
                continue;
              if (playerDeal.Contract.IsPass)
              {
                numPass++;
                continue;
              }

              if (playerDeal.Result.RelativeTricks < -5)
              {
                numReallyBad++;
                continue;
              }

              ++numPlayerDeals;
              var suit = playerDeal.Contract.Suit.Suit;
              Func<Suit, int?> strengthDelegate = playerDeal.Position.IsNorthOrSouth
                ? (Func<Suit, int?>)deal.Cards.NorthSouthTotalPoints
                : deal.Cards.EastWestTotalPoints;
              var key = (suit, strengthDelegate(suit).Value, deal.Vulnerability.IsVulnerable(playerDeal.Position));
              if (!statistics.ContainsKey(key))
                statistics.Add(key, new Dictionary<int, (int frequency, int score)> { { -5, (0, 0) }, { -4, (0, 0) }, { -3, (0, 0) }, { -2, (0, 0) }, { -1, (0, 0) }, { 7, (0, 0) }, { 8, (0, 0) }, { 9, (0, 0) }, { 10, (0, 0) }, { 11, (0, 0) }, { 12, (0, 0) }, { 13, (0, 0) } });

              var tricks = playerDeal.Result.IsDefeated ? playerDeal.Result.RelativeTricks : playerDeal.Result.Tricks;
              statistics[key][tricks] = (statistics[key][tricks].Frequency + 1, statistics[key][tricks].Score + playerDeal.Score);
            }
          }
        }
      }

      TestContext.Out.WriteLine($"{numPlayerDeals} deals in {numDeals} different deals analyzed. {numPass} pass and {numReallyBad} deals worse than -5 ignored.");
      TestContext.Out.WriteLine("Suit;Vulnerable;Strength;Avg. Score;-5;-4;-3;-2;-1;7;8;9;10;11;12;13");
      foreach (var line in statistics.OrderBy(kv => kv.Key.Zon).ThenBy(kv => kv.Key.Suit).ThenBy(kv => kv.Key.Strength))
      {
        var sum = line.Value.Sum(kv => kv.Value.Frequency);
        var avgScore = line.Value.Sum(kv => kv.Value.Score) / sum;
        var frequencies = string.Join(";", line.Value.Select(kv => (kv.Value.Frequency * 100) / sum));
        TestContext.Out.WriteLine($"{line.Key.Suit};{line.Key.Zon};{line.Key.Strength};{avgScore};" + frequencies);
      }
    }

    [Test]
    public void FindAverageTricksPerStrengthBasedOnPbnData()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();

      var statistics2 =
        new Dictionary<(Suit Suit, int Strength, bool Zon), (int Frequency, int Tricks)>();

      var numDeals = 0;
      var numPlayerDeals = 0;

      var numPass = 0;

      foreach (var file in Directory.EnumerateFiles(TestContext.CurrentContext.TestDirectory + @"\..\..\TestData"))
      {
        if (!int.TryParse(Path.GetFileNameWithoutExtension(file), out _))
          continue;

        ////TestContext.Progress.WriteLine(file);
        using (var fileStream = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
          var result = sut.Parse(new StreamReader(fileStream));
          var model = new SelectionFactory().CreateSelection(result, Substitute.For<IFilter>());
          model.Repopulate();
          foreach (var deal in model.Deals)
          {
            ++numDeals;
            foreach (var playerDeal in deal.PlayerDeals)
            {
              if (playerDeal.Role.IsDefending)
                continue;
              if (playerDeal.Contract.IsPass)
              {
                numPass++;
                continue;
              }

              ++numPlayerDeals;
              var suit = playerDeal.Contract.Suit.Suit;
              Func<Suit, int?> strengthDelegate = playerDeal.Position.IsNorthOrSouth
                ? (Func<Suit, int?>)deal.Cards.NorthSouthTotalPoints
                : deal.Cards.EastWestTotalPoints;
              var key = (suit, strengthDelegate(suit).Value, deal.Vulnerability.IsVulnerable(playerDeal.Position));
              if (!statistics2.ContainsKey(key))
                statistics2.Add(key, (0, 0));

              statistics2[key] = (statistics2[key].Frequency + 1, statistics2[key].Tricks + playerDeal.Result.Tricks);
            }
          }
        }
      }

      TestContext.Out.WriteLine(
        $"{numPlayerDeals} deals in {numDeals} different deals analyzed. {numPass} pass ignored.");
      TestContext.Out.WriteLine("Suit;Vulnerable;Strength;Avg. Tricks;");
      foreach (var line in statistics2.OrderBy(kv => kv.Key.Zon).ThenBy(kv => kv.Key.Suit)
        .ThenBy(kv => kv.Key.Strength))
      {
        var avgTricks = line.Value.Tricks / (float)line.Value.Frequency;
        TestContext.Out.WriteLine($"{line.Key.Suit};{line.Key.Zon};{line.Key.Strength};{avgTricks:F1}");
      }
    }

    [Test]
    [Category("IntegrationTest")]
    public void CompareScoringSystems()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var numDeals = 0;
      var numEvents = 0;

      var eventPositions = new ScoringSystems_();
      var dealPositions = new ScoringSystems_();

      var random = new Random(745);

      foreach (var file in Directory.EnumerateFiles(TestContext.CurrentContext.TestDirectory + @"\..\..\TestData"))
      {
        if (!int.TryParse(Path.GetFileNameWithoutExtension(file), out _))
          continue;

        ////TestContext.Progress.WriteLine(file);
        using (var fileStream = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
          var result = sut.Parse(new StreamReader(fileStream));
          var model = new SelectionFactory().CreateSelection(result, Substitute.For<IFilter>());
          model.Repopulate();
          
          ////if (model.Deals.All(d => d.PlayerDeals.Count != 8))
          ////  continue;
          ++numEvents;
          var eventScores = new Dictionary<string, ScoringSystems_>();
          foreach (var player in result.Players)
          {
            if (result.Games.Any(g => g.Value.NorthId == player.Key || g.Value.EastId == player.Key))
              eventScores.Add(player.Value.Name, new ScoringSystems_());
          }

          foreach (var deal in model.Deals)
          {
            ////if (deal.PlayerDeals.Count != 8)
            ////  continue;
            if (deal.Cards.EastWestTotalPoints(Suit.NoTrump) == null)
              continue;
            ++numDeals;
            var dealScoresNS = new Dictionary<string, ScoringSystems_>();
            var dealScoresEW = new Dictionary<string, ScoringSystems_>();

            foreach (var playerDeal in deal.PlayerDeals)
            {
              if (deal.PlayerDeals.Count != deal.NumberOfPairs && playerDeal.Position.SortIndex > 1)
                continue;

              var playerScores = new ScoringSystems_();
              if (playerDeal.Position.IsNorthOrSouth)
                dealScoresNS.Add(playerDeal.Player.Name, playerScores);
              else
                dealScoresEW.Add(playerDeal.Player.Name, playerScores);
              playerScores.Raw = playerDeal.Score;
              playerScores.FairBridge = playerDeal.ScoreFairBridge;
              playerScores.Hcp = deal.Cards.EastWestTotalPoints(Suit.NoTrump).Value;
              playerScores.OpponentHcp = deal.Cards.NorthSouthTotalPoints(Suit.NoTrump).Value;
              playerScores.Imp = (int)(playerDeal.ScoreIMP * 100);
              playerScores.MatchPoint = (int)(playerDeal.ScoreMP * 100);
              playerScores.Tricks = playerDeal.Result.Tricks;
              playerScores.Wallin = playerDeal.ScoreWallin;
              playerScores.Random = random.Next(999999);

              if (playerDeal.Position.IsNorthOrSouth)
              {
                playerScores.Hcp = 40 - playerScores.Hcp;
                playerScores.OpponentHcp = 40 - playerScores.OpponentHcp;
              }

              if (!playerDeal.Contract.IsPass && playerDeal.Role.IsDefending)
              {
                playerScores.Tricks = 13 - playerScores.Tricks;
              }
            }

            foreach (var kvp in dealScoresNS.Concat(dealScoresEW))
            {
              eventScores[kvp.Key].Add(kvp.Value);
            }

            dealPositions.Add(ScoringSystems_.GetPositionChanges(dealScoresNS)
              .Modify(s => (s * 10000) / dealScoresNS.Count / dealScoresNS.Count));
            dealPositions.Add(ScoringSystems_.GetPositionChanges(dealScoresEW)
              .Modify(s => (s * 10000) / dealScoresEW.Count / dealScoresEW.Count));
          }

          eventPositions.Add(ScoringSystems_.GetPositionChanges(eventScores)
            .Modify(s => (s * 10000) / eventScores.Count / eventScores.Count));
        }
      }

      dealPositions.Modify(s => 3 * s / numDeals / 200);
      eventPositions.Modify(s => 3 * s / numEvents / 100);
      TestContext.Out.WriteLine($"{numEvents} events with {numDeals} total deals analyzed.");
      TestContext.Out.WriteLine("Positional changes per deal compared to match point scoring:");
      dealPositions.Print("{0}: {1}%");
      TestContext.Out.WriteLine("Positional changes per event compared to match point scoring:");
      eventPositions.Print("{0}: {1}%");
    }

    private class ScoringSystems_
    {
      public int Raw { get; set; }
      public int MatchPoint { get; set; }
      public int Imp { get; set; }
      public int Wallin { get; set; }
      public int FairBridge { get; set; }
      public int Hcp { get; set; }
      public int OpponentHcp { get; set; }
      public int Random { get; set; }
      public int Tricks { get; set; }

      private int Alphabetical_ { get; set; }

      public static ScoringSystems_ GetPositionChanges(Dictionary<string, ScoringSystems_> scores)
      {
        var realPosition = scores.OrderBy(s => s.Value.MatchPoint).ThenBy(s => s.Key).Select((s, i) => (s.Key, i))
          .ToDictionary(i => i.Item1, i => i.Item2);
        var result = new ScoringSystems_
                     {
                       Raw = SortAndCompare_(scores, s => s.Value.Raw, realPosition),
                       MatchPoint = SortAndCompare_(scores, s => s.Value.MatchPoint, realPosition),
                       Imp = SortAndCompare_(scores, s => s.Value.Imp, realPosition),
                       Wallin = SortAndCompare_(scores, s => s.Value.Wallin, realPosition),
                       FairBridge = SortAndCompare_(scores, s => s.Value.FairBridge, realPosition),
                       Hcp = SortAndCompare_(scores, s => s.Value.Hcp, realPosition),
                       OpponentHcp = SortAndCompare_(scores, s => s.Value.OpponentHcp, realPosition),
                       Random = SortAndCompare_(scores, s => s.Value.Random, realPosition),
                       Tricks = SortAndCompare_(scores, s => s.Value.Tricks, realPosition),
                       Alphabetical_ = SortAndCompare_(scores, s => s.Key, realPosition)
                     };

        return result;
      }

      public void Add(ScoringSystems_ toAdd)
      {
        Raw += toAdd.Raw;
        MatchPoint += toAdd.MatchPoint;
        Imp += toAdd.Imp;
        Wallin += toAdd.Wallin;
        FairBridge += toAdd.FairBridge;
        Hcp += toAdd.Hcp;
        OpponentHcp += toAdd.OpponentHcp;
        Random += toAdd.Random;
        Tricks += toAdd.Tricks;
        Alphabetical_ += toAdd.Alphabetical_;
      }

      public ScoringSystems_ Modify(Func<int, int> modification)
      {
        Raw = modification(Raw);
        MatchPoint = modification(MatchPoint);
        Imp = modification(Imp);
        Wallin = modification(Wallin);
        FairBridge = modification(FairBridge);
        Hcp = modification(Hcp);
        OpponentHcp = modification(OpponentHcp);
        Random = modification(Random);
        Tricks = modification(Tricks);
        Alphabetical_ = modification(Alphabetical_);

        return this;
      }

      public void Print(string format)
      {
        TestContext.Out.WriteLine(format, "Raw", Raw);
        TestContext.Out.WriteLine(format, "MatchPoint", MatchPoint);
        TestContext.Out.WriteLine(format, "Imp", Imp);
        TestContext.Out.WriteLine(format, "Wallin+", Wallin);
        TestContext.Out.WriteLine(format, "FAIRbridge", FairBridge);
        TestContext.Out.WriteLine(format, "Hcp", Hcp);
        TestContext.Out.WriteLine(format, "Opp. Hcp", OpponentHcp);
        TestContext.Out.WriteLine(format, "Random", Random);
        TestContext.Out.WriteLine(format, "Won tricks", Tricks);
        TestContext.Out.WriteLine(format, "Alphabetical", Alphabetical_);
      }

      private static int SortAndCompare_(
        Dictionary<string, ScoringSystems_> scores,
        Func<KeyValuePair<string, ScoringSystems_>, object> sortingFunc,
        Dictionary<string, int> realPosition)
      {
        var positions = scores.OrderBy(sortingFunc).ThenBy(s => s.Key).Select((s, i) => (s.Key, i));
        var totalDiff = 0;
        foreach (var position in positions)
        {
          totalDiff += Math.Abs(position.Item2 - realPosition[position.Item1]);
        }

        return totalDiff;
      }
    }

#pragma warning restore S125 // Sections of code should not be "commented out"
  }
}