using System.Collections.Generic;
using LegoBridgeDealer.PbnModel.WriteModel;
using LegoBridgeDealer.PbnParser;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  internal class ParseScoreTableFixture
  {
    [Test]
    public void ShouldFailAssertWhenCurrentStateIsIncorrect()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);

      var sut = new ParseScoreTable("some;header", new Game(), new Queue<(int GameId, string Line)>());

      Assert.Throws<AssertionException>(() => sut.ParseLine(" ", state));
    }

    [Test]
    public void ShouldReturnPopStateAndReturnInputWhenLineIsWhiteSpace()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      Queue<(int GameId, string Line)> parseLaterQueue = new Queue<(int GameId, string Line)>();
      parseLaterQueue.Enqueue((3, "text"));
      var sut = new ParseScoreTable("some;header", new Game(), parseLaterQueue);
      state.Push(sut);

      var line = "  \t ";
      var result = sut.ParseLine(line, state);

      Assert.That(state.Peek(), Is.SameAs(previousState));
      Assert.That(parseLaterQueue.Count, Is.EqualTo(1));
      Assert.That(result, Is.EqualTo(line));
    }

    [Test]
    public void ShouldPopStateAndReturnInputWhenLineStartsWithNewTag()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      Queue<(int GameId, string Line)> parseLaterQueue = new Queue<(int GameId, string Line)>();
      parseLaterQueue.Enqueue((3, "text"));
      var sut = new ParseScoreTable("some;header", new Game(), parseLaterQueue);
      state.Push(sut);

      var line = " [Something else]";
      var result = sut.ParseLine(line, state);

      Assert.That(state.Peek(), Is.SameAs(previousState));
      Assert.That(parseLaterQueue.Count, Is.EqualTo(1));
      Assert.That(result, Is.EqualTo(line));
    }

    [Test]
    public void ShouldAddReformattedLineToParseLaterQueueAssociatedWithCurrentUserWhenReadingTableRow()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      Queue<(int GameId, string Line)> parseLaterQueue = new Queue<(int GameId, string Line)>();
      parseLaterQueue.Enqueue((3, "text"));
      var sut = new ParseScoreTable(@"Table\2R;Round\1R;PairId_NS\2R;PairId_EW\2R;Contract\4L;Declarer\1R;Result\2R;Lead\3L;Score_NS\5R;Score_EW\6R;MP_NS\4R;MP_EW\4R;Percentage_NS\3R;Percentage_EW\3R", new Game { Id = 7 }, parseLaterQueue);
      state.Push(sut);

      var line = " 11 1 18 11 3N   W  5 H3  \"200\"      - 24.0  0.0 100   0 ";
      var result = sut.ParseLine(line, state);

      Assert.That(state.Peek(), Is.SameAs(sut));
      Assert.That(result, Is.EqualTo(null));
      Assert.That(parseLaterQueue.Count, Is.EqualTo(2));
      Assert.That(parseLaterQueue.Dequeue().Line, Is.EqualTo("text"));
      var newLine = parseLaterQueue.Dequeue();
      Assert.That(newLine.GameId, Is.EqualTo(7));
      Assert.That(newLine.Line, Is.EqualTo("ScoreTable;18;11;3N;W;5;-200;0"));
    }

    [Test]
    public void ShouldPickNorthsResultWhenBiddingIsPass()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      Queue<(int GameId, string Line)> parseLaterQueue = new Queue<(int GameId, string Line)>();
      var sut = new ParseScoreTable(@"Section\\3R;Table\\1R;Round\\1R;PairId_NS\\2R;PairId_EW\\2R;Contract\\4L;Declarer\\1R;Result\\2R;Lead\\3L;Score_NS\\6R;Score_EW\\6R;MP_NS\\4R;MP_EW\\4R;Percentage_NS\\3R;Percentage_EW\\3R", new Game { Id = 7 }, parseLaterQueue);
      state.Push(sut);

      var line = "\"B\" 2 9 41 32 Pass -  - -      \"0\"      - 10.0 20.0  33  67";
      var result = sut.ParseLine(line, state);

      Assert.That(state.Peek(), Is.SameAs(sut));
      Assert.That(result, Is.EqualTo(null));
      var newLine = parseLaterQueue.Dequeue();
      Assert.That(newLine.GameId, Is.EqualTo(7));
      Assert.That(newLine.Line, Is.EqualTo("ScoreTable;41;32;Pass;-;-;0;33"));
    }
  }
}