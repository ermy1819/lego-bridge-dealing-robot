using System.Collections.Generic;
using LegoBridgeDealer.PbnModel.WriteModel;
using LegoBridgeDealer.PbnParser;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  internal class LookingForDealFixture
  {
    [Test]
    public void ShouldFailAssertWhenStateIsWrong()
    {
      var root = new Root();
      var state = new Stack<IParserState>();
      var sut = new LookingForDeal(root, new Queue<(int GameId, string Line)>());
      state.Push(Substitute.For<IParserState>());

      Assert.Throws<AssertionException>(() => sut.ParseLine("text only", state));

      state.Push(sut);
      state.Push(Substitute.For<IParserState>());
      Assert.Throws<AssertionException>(() => sut.ParseLine("text only", state));
    }

    [Test]
    public void ShouldReturnNullWhenLineIsWhiteSpaceOnly()
    {
      var root = new Root();
      var state = new Stack<IParserState>();
      var sut = new LookingForDeal(root, new Queue<(int GameId, string Line)>());
      state.Push(sut);

      Assert.That(sut.ParseLine(string.Empty, state), Is.Null);
      Assert.That(sut.ParseLine("     ", state), Is.Null);
      Assert.That(state.Pop(), Is.SameAs(sut));
      Assert.That(state.Count, Is.Zero);
    }

    [Test]
    public void ShouldReturnNullWhenLineIsControlCharacterOnly()
    {
      var root = new Root();
      var state = new Stack<IParserState>();
      var sut = new LookingForDeal(root, new Queue<(int GameId, string Line)>());
      state.Push(sut);

      Assert.That(sut.ParseLine(new string(new[] { (char)0x001a } ), state), Is.Null);
      Assert.That(state.Pop(), Is.SameAs(sut));
      Assert.That(state.Count, Is.Zero);
    }

    [Test]
    public void ShouldAddDealAndEventWhenLineIsNotEmpty()
    {
      var root = new Root();
      var state = new Stack<IParserState>();
      var sut = new LookingForDeal(root, new Queue<(int GameId, string Line)>());
      state.Push(sut);

      Assert.That(sut.ParseLine("Not empty", state), Is.EqualTo("Not empty"));
      Assert.That(root.Games.Count, Is.EqualTo(1));
      Assert.That(root.Events.Count, Is.EqualTo(1));
    }

    [Test]
    public void ShouldPushParserDealStateWhenLineIsNotEmpty()
    {
      var root = new Root();
      var state = new Stack<IParserState>();
      var sut = new LookingForDeal(root, new Queue<(int GameId, string Line)>());
      state.Push(sut);

      Assert.That(sut.ParseLine("Not empty", state), Is.EqualTo("Not empty"));
      Assert.That(state.Pop(), Is.TypeOf<ParsingDeal>());
      Assert.That(state.Pop(), Is.SameAs(sut));
      Assert.That(state.Count, Is.Zero);
    }

    [Test]
    public void ShouldAddDifferentIdsForEachGame()
    {
      var root = new Root();
      var state = new Stack<IParserState>();
      var sut = new LookingForDeal(root, new Queue<(int GameId, string Line)>());
      state.Push(sut);

      sut.ParseLine("Not empty", state);
      Assert.That(root.Games.Count, Is.EqualTo(1));
      Assert.That(root.Events.Count, Is.EqualTo(1));
      Assert.That(root.Events[0].Id, Is.EqualTo(0));
      Assert.That(root.Games[0].EventId, Is.EqualTo(0));

      state.Pop();

      sut.ParseLine("Not empty", state);
      Assert.That(root.Games.Count, Is.EqualTo(2));
      Assert.That(root.Events.Count, Is.EqualTo(2));
      Assert.That(root.Events[1].Id, Is.EqualTo(1));
      Assert.That(root.Games[1].EventId, Is.EqualTo(1));
    }
  }
}