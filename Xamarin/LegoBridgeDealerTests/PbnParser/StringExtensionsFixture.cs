﻿using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  internal class StringExtensionsFixture
  {
    [TestCase("")]
    [TestCase(" ")]
    [TestCase("\t")]
    [TestCase("\r\n")]
    [TestCase("                         ")]
    [TestCase(" \t \n \r ")]
    [TestCase(null)]
    public void ShouldReturnNull(string input)
    {
      Assert.That(LegoBridgeDealer.PbnParser.StringExtensions.TrimOrNull(input), Is.Null);
    }

    [TestCase(" a")]
    [TestCase(" a ")]
    [TestCase(" \ta")]
    [TestCase("\r a")]
    [TestCase(" \na  ")]
    [TestCase(" a b c ")]
    [TestCase("        a\t\t")]
    public void ShouldTrimStart(string input)
    {
      Assert.That(LegoBridgeDealer.PbnParser.StringExtensions.TrimOrNull(input), Is.EqualTo(input.TrimStart()));
    }
  }
}