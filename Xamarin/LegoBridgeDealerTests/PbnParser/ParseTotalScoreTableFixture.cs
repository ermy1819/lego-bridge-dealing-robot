using System.Collections.Generic;
using LegoBridgeDealer.PbnModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using LegoBridgeDealer.PbnParser;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  internal class ParseTotalScoreTableFixture
  {
    [Test]
    public void ShouldFailAssertWhenCurrentStateIsIncorrect()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);

      var sut = new ParseTotalScoreTable("some;header", new Repository<Player>());

      Assert.Throws<AssertionException>(() => sut.ParseLine(" ", state));
    }

    [Test]
    public void ShouldReturnNullWhenLineIsWhiteSpace()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      var repository = new Repository<Player>();
      repository.Add(1, new Player());
      repository.Add(2, new Player());
      repository.Add(3, new Player());
      var sut = new ParseTotalScoreTable("some;header", repository);
      state.Push(sut);

      var result = sut.ParseLine("  \t ", state);

      Assert.That(state.Peek(), Is.SameAs(sut));
      Assert.That(repository.Count, Is.EqualTo(3));
      Assert.That(result, Is.EqualTo(null));
    }

    [Test]
    public void ShouldPopStateAndReturnInputWhenLineStartsWithNewTag()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      var repository = new Repository<Player>();
      repository.Add(1, new Player());
      repository.Add(2, new Player());
      repository.Add(3, new Player());
      var sut = new ParseTotalScoreTable("some;header", repository);
      state.Push(sut);

      var line = " [Something else]";
      var result = sut.ParseLine(line, state);

      Assert.That(state.Peek(), Is.SameAs(previousState));
      Assert.That(repository.Count, Is.EqualTo(3));
      Assert.That(result, Is.EqualTo(line));
    }

    [Test]
    public void ShouldAddUserWhenReadingTableRow()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      var repository = new Repository<Player>();
      repository.Add(1, new Player());
      repository.Add(2, new Player());
      repository.Add(3, new Player());
      var sut = new ParseTotalScoreTable("Rank;;Number;PairId\\2R;field;;Names\\44L;;", repository);
      state.Push(sut);

      var line = "  3 10  1 7 \"text\" \"text with space\" \"Name Middle - Two Three\" 25.6 338.7 ";
      var result = sut.ParseLine(line, state);

      Assert.That(state.Peek(), Is.SameAs(sut));
      Assert.That(result, Is.EqualTo(null));
      Assert.That(repository.Count, Is.EqualTo(4));
      Assert.That(repository[7].Name, Is.EqualTo("Name Middle - Two Three"));
    }

    [Test]
    public void ShouldIgnoreAlreadyAddedUserWhenReadingTableRow()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      var repository = new Repository<Player>();
      repository.Add(1, new Player());
      repository.Add(2, new Player());
      repository.Add(7, new Player { Name = "oldValue" });
      var sut = new ParseTotalScoreTable("Rank;;Number;PairId\\2R;field;;Names\\44L;;", repository);
      state.Push(sut);

      var line = "  3 10  1 7 \"text\" \"text with space\" \"Name Middle - Two Three\" 25.6 338.7 ";
      var result = sut.ParseLine(line, state);

      Assert.That(state.Peek(), Is.SameAs(sut));
      Assert.That(result, Is.EqualTo(null));
      Assert.That(repository.Count, Is.EqualTo(3));
      Assert.That(repository[7].Name, Is.EqualTo("oldValue"));
    }

    [Test]
    public void ShouldBeAbleToHandleNonNumericPairId()
    {
      var previousState = Substitute.For<IParserState>();
      var state = new Stack<IParserState>();
      state.Push(previousState);
      var repository = new Repository<Player>();
      var sut = new ParseTotalScoreTable("Rank;;Number;PairId\\2R;field;;Names\\44L;;", repository);
      state.Push(sut);

      var line = "  3 10  1 A17 \"text\" \"text with space\" \"Name Middle - Two Three\" 25.6 338.7 ";
      var result = sut.ParseLine(line, state);

      Assert.That(state.Peek(), Is.SameAs(sut));
      Assert.That(result, Is.EqualTo(null));
      Assert.That(repository.Count, Is.EqualTo(1));
      Assert.That(repository["A17".GetHashCode()].Name, Is.EqualTo("Name Middle - Two Three"));
    }
  }
}