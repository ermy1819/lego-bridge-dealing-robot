﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnParser
{
  internal partial class PbnParserFixture
  {
    [Test]
    public void ShouldReturnNoDataFromEmptyInput()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(new StringReader(" "));

      Assert.That(result.Games, Has.Count.EqualTo(0));
      Assert.That(result.Events, Has.Count.EqualTo(0));
    }

    [Test]
    public void ShouldReturnOneGameFromPbn21SpecificationExample()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(new StringReader(pbn21SpecificationExample_));

      Assert.That(result.Games, Has.Count.EqualTo(1));
      Assert.That(result.Events, Has.Count.EqualTo(1));
    }

    [Test]
    public void ShouldGetCorrectEventDataWhenParsingPbn21SpecificationExample()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(new StringReader(pbn21SpecificationExample_));
      var @event = result.Events.Single().Value;

      Assert.That(@event.Name, Is.EqualTo("International Amsterdam Airport Schiphol Bridgetournament"));
      Assert.That(@event.Site, Is.EqualTo("Amsterdam, The Netherlands NLD"));
      Assert.That(@event.StartDate, Is.EqualTo(new DateTime(1995, 06, 10)));
      Assert.That(@event.EndDate, Is.EqualTo(new DateTime(1995, 06, 10)));
    }

    [Test]
    public void ShouldCreateTwoGamesWhenTwogamesAreSeparatedByEmptyLine()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(
        new StringReader(pbn21SpecificationExample_ + Environment.NewLine + pbn21SpecificationExample_));

      Assert.That(result.Games, Has.Count.EqualTo(2));
    }

    [Test]
    public void ShouldIgnoreDuplicateTags()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(
        new StringReader(pbn21SpecificationExample_ + pbn21SpecificationExample_.Replace("Netherlands", "Sweden")));

      Assert.That(result.Events, Has.Count.EqualTo(1));
      Assert.That(result.Events.Single().Value.Site, Is.EqualTo("Amsterdam, The Netherlands NLD"));
    }

    [Test]
    public void ShouldStoreUnmodifiedInputForEachGameAsRawPbn()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(
        new StringReader(
          pbn21SpecificationExample_ + Environment.NewLine
          + pbn21SpecificationExample_.Replace("Netherlands", "Sweden")));

      Assert.That(result.Games[0].RawPbn.TrimEnd(), Is.EqualTo(pbn21SpecificationExample_.TrimEnd()));
      Assert.That(
        result.Games[1].RawPbn.TrimEnd(),
        Is.EqualTo(pbn21SpecificationExample_.Replace("Netherlands", "Sweden").TrimEnd()));
    }

    [Test]
    public void ShouldMergeEventsWhenNamesAreEqual()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(
        new StringReader(
          pbn21SpecificationExample_ + Environment.NewLine
          + pbn21SpecificationExample_.Replace("1995.06.10", "1995.06.08")));

      Assert.That(result.Events, Has.Count.EqualTo(1));
      Assert.That(result.Events[0].StartDate, Is.EqualTo(new DateTime(1995, 06, 08)));
      Assert.That(result.Events[0].EndDate, Is.EqualTo(new DateTime(1995, 06, 10)));
      Assert.That(result.Games[0].EventId, Is.EqualTo(result.Events[0].Id));
      Assert.That(result.Games[1].EventId, Is.EqualTo(result.Events[0].Id));
    }

    [Test]
    public void ShouldKeepEventsSeparateWhenNamesAreDifferent()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(
        new StringReader(
          pbn21SpecificationExample_ + Environment.NewLine + pbn21SpecificationExample_.Replace(
            @"Event ""International Amsterdam Airport Schiphol Bridgetournament""",
            @"Event ""Some other Bridgetournament""")));

      Assert.That(result.Events, Has.Count.EqualTo(2));
      Assert.That(result.Events[0].Name, Is.EqualTo("International Amsterdam Airport Schiphol Bridgetournament"));
      Assert.That(result.Events[1].Name, Is.EqualTo("Some other Bridgetournament"));
      Assert.That(result.Games[0].EventId, Is.EqualTo(result.Events[0].Id));
      Assert.That(result.Games[1].EventId, Is.EqualTo(result.Events[1].Id));
    }

    [Test]
    public void ShouldParsePlayerNamesAsDifferentPlayerAggregates()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(new StringReader(pbn21SpecificationExample_));
      var game = result.Games.Single().Value;

      Assert.That(result.Players[game.WestId].Name, Is.EqualTo("Podgor"));
      Assert.That(result.Players[game.NorthId].Name, Is.EqualTo("Westra"));
      Assert.That(result.Players[game.EastId].Name, Is.EqualTo("Kalish"));
      Assert.That(result.Players[game.SouthId].Name, Is.EqualTo("Leufkens"));

      Assert.That(result.Players.All(p => p.Key == p.Value.Id));
      Assert.That(result.Players.Count, Is.EqualTo(4));
    }

    [Test]
    public void ShouldReusePlayerNamesWhenParsingSeveralGames()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(
        new StringReader(pbn21SpecificationExample_ + Environment.NewLine + pbn21SpecificationExample_));

      Assert.That(result.Players.Count, Is.EqualTo(4));
      Assert.That(result.Games[0].WestId, Is.EqualTo(result.Games[1].WestId));
      Assert.That(result.Games[0].NorthId, Is.EqualTo(result.Games[1].NorthId));
      Assert.That(result.Games[0].EastId, Is.EqualTo(result.Games[1].EastId));
      Assert.That(result.Games[0].SouthId, Is.EqualTo(result.Games[1].SouthId));
    }

    [Test]
    public void ShouldUsePreviousEventNameWhenEventNameIsHash()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(
        new StringReader(
          pbn21SpecificationExample_ + Environment.NewLine + pbn21SpecificationExample_.Replace(
            @"Event ""International Amsterdam Airport Schiphol Bridgetournament""",
            @"Event ""#""").Replace("1995.06.10", "1995.06.08")));

      Assert.That(result.Events.Count, Is.EqualTo(1));
      Assert.That(result.Games[0].EventId, Is.EqualTo(result.Games[1].EventId));
      Assert.That(result.Events[0].StartDate, Is.EqualTo(new DateTime(1995, 06, 08)));
      Assert.That(result.Events[0].EndDate, Is.EqualTo(new DateTime(1995, 06, 10)));
    }

    [Test]
    public void ShouldUsePreviousSiteNameWhenSiteNameIsHash()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(
        new StringReader(
          pbn21SpecificationExample_ + Environment.NewLine + pbn21SpecificationExample_
            .Replace(@"Site ""Amsterdam, The Netherlands NLD""", @"Site ""#""").Replace("1995.06.10", "1995.06.08")));

      Assert.That(result.Events.Count, Is.EqualTo(1));
      Assert.That(result.Games[0].EventId, Is.EqualTo(result.Games[1].EventId));
      Assert.That(result.Events[0].StartDate, Is.EqualTo(new DateTime(1995, 06, 08)));
      Assert.That(result.Events[0].EndDate, Is.EqualTo(new DateTime(1995, 06, 10)));
    }

    [Test]
    public void ShouldUsePreviousDateWhenDateIsHash()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(
        new StringReader(
          pbn21SpecificationExample_ + Environment.NewLine + pbn21SpecificationExample_.Replace("1995.06.10", "#")));

      Assert.That(result.Events.Count, Is.EqualTo(1));
      Assert.That(result.Games[0].EventId, Is.EqualTo(result.Games[1].EventId));
      Assert.That(result.Events[0].StartDate, Is.EqualTo(new DateTime(1995, 06, 10)));
      Assert.That(result.Events[0].EndDate, Is.EqualTo(new DateTime(1995, 06, 10)));
    }

    [Test]
    public void ShouldReturn26GamesAnd27PlayersFromTableStyleExample()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(new StringReader(ruterTableStyleExample_));

      Assert.That(result.Players, Has.Count.EqualTo(27));
      Assert.That(result.Games, Has.Count.EqualTo(26));
      Assert.That(result.Events, Has.Count.EqualTo(1));
    }

    [Test]
    public void ShouldHaveSameEventForAllGamesForTableStyleExample()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(new StringReader(ruterTableStyleExample_));

      foreach (var game in result.Games.Values)
      {
        Assert.That(game.EventId, Is.EqualTo(result.Events[0].Id));
      }
    }

    [Test]
    public void ShouldNotDestroyNonAsciiCharactersDuringParsing()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();
      var result = sut.Parse(new StringReader(pbn21SpecificationExample_.Replace("Westra", "åäöéèü€Юԇێظרּﯤ")));

      Assert.That(result.Players[1].Name, Is.EqualTo("åäöéèü€Юԇێظרּﯤ"));
      Assert.That(result.Events, Has.Count.EqualTo(1));
    }

    [Test]
    [Category("IntegrationTest")]
    public void ShouldParseFilesWithoutExceptions()
    {
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();

      foreach (var file in Directory.EnumerateFiles(TestContext.CurrentContext.TestDirectory + @"\..\..\TestData"))
      {
        TestContext.Progress.WriteLine(file);
        using (var fileStream = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
          var result = sut.Parse(new StreamReader(fileStream));
          Assert.That(result.Players, Has.Count.GreaterThan(0));
          Assert.That(result.Events, Has.Count.GreaterThan(0));
          Assert.That(result.Games, Has.Count.GreaterThan(0));
        }
      }
    }

    [Test]
    [Category("IntegrationTest")]
    public void ShouldParseNonAsciiCharactersFromFile()
    {
      // This test is really a fail. Can't read correctly without explicitly stating the code table used.
      // No simple solution to this problem.
      var sut = new LegoBridgeDealer.PbnParser.PbnParser();

      using (var fileStream = File.Open(
        TestContext.CurrentContext.TestDirectory + @"\..\..\TestData\187038.pbn",
        FileMode.Open,
        FileAccess.Read,
        FileShare.ReadWrite))
      {
        var result = sut.Parse(new StreamReader(fileStream, Encoding.GetEncoding(1252)));
        Assert.That(result.Players[4].Name, Is.EqualTo("Biörn Torbiörnsson - Måns Torbiörnsson"));
        Assert.That(result.Events[0].Name, Is.EqualTo("Söndagsbarro"));
      }
    }
  }
}
