﻿using System.Collections.Generic;
using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.ReadModel.Services;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel.Services
{
  internal class MatchPointScoreServiceFixture
  {
    [Test]
    public void ShouldFailAssertWhenGettingScoreBeforeInitializing()
    {
      var sut = new MatchPointScoreServiceFactory().CreateService();
      Assert.Throws<AssertionException>(() => sut.GetPercentage(100, Substitute.For<IPosition>()));
    }

    [Test]
    public void ShouldGet50PercentWhenAllScoresAreTheSame()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (140, position),
                          (140, position),
                          (140, position),
                          (140, position),
                          (140, position),
                          (140, position),
                        };
      var sut = new MatchPointScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetPercentage(140, position), Is.EqualTo(50m));
    }

    [Test]
    public void ShouldFailAssertWhenScoreIsNotPartOfRawScores()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (-100, position),
                          (-50, position),
                          (70, position),
                          (110, position),
                          (140, position),
                          (200, position),
                        };
      var sut = new MatchPointScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.Throws<AssertionException>(() => sut.GetPercentage(-140, position));
    }

    [Test]
    public void ShouldGet100PercentWhenScoreIsTheBest()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (-100, position),
                          (-50, position),
                          (70, position),
                          (110, position),
                          (140, position),
                          (200, position),
                        };
      var sut = new MatchPointScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetPercentage(200, position), Is.EqualTo(100m));
    }

    [Test]
    public void ShouldGet0PercentWhenScoreIsTheWorst()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (-100, position),
                          (-50, position),
                          (70, position),
                          (110, position),
                          (140, position),
                          (200, position),
                        };
      var sut = new MatchPointScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetPercentage(-100, position), Is.EqualTo(0m));
    }

    [Test]
    public void ShouldConvertScoresCorrectly()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (-100, position),
                          (140, position),
                          (-100, position),
                          (-50, position),
                          (200, position),
                          (70, position),
                          (110, position),
                          (140, position),
                          (-100, position),
                          (140, position),
                          (140, position),
                        };
      var sut = new MatchPointScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetPercentage(-100, position), Is.EqualTo(10m));
      Assert.That(sut.GetPercentage(-50, position), Is.EqualTo(30m));
      Assert.That(sut.GetPercentage(70, position), Is.EqualTo(40m));
      Assert.That(sut.GetPercentage(110, position), Is.EqualTo(50m));
      Assert.That(sut.GetPercentage(140, position), Is.EqualTo(75m));
      Assert.That(sut.GetPercentage(200, position), Is.EqualTo(100m));
    }

    [Test]
    public void ShouldBeAbleToHandleDifferentDeclarers()
    {
      var ns = Substitute.For<IPosition>();
      var ew = Substitute.For<IPosition>();
      ns.IsNorthOrSouth.Returns(true);
      ew.IsNorthOrSouth.Returns(false);
      var rawScores = new List<(int, IPosition)>
                        {
                          (-100, ns),
                          (100, ew),
                          (-100, ns),
                          (50, ew ),
                          (70, ns ),
                          (-110, ew ),
                          (140, ns ),
                          (-140, ew ),
                          (140, ns ),
                          (-140, ew ),
                          (200, ns ),
                        };
      var sut = new MatchPointScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetPercentage(-100, ns), Is.EqualTo(10m));
      Assert.That(sut.GetPercentage(-50, ns), Is.EqualTo(30m));
      Assert.That(sut.GetPercentage(70, ns), Is.EqualTo(40m));
      Assert.That(sut.GetPercentage(110, ns), Is.EqualTo(50m));
      Assert.That(sut.GetPercentage(140, ns), Is.EqualTo(75m));
      Assert.That(sut.GetPercentage(200, ns), Is.EqualTo(100m));

      Assert.That(sut.GetPercentage(100, ew), Is.EqualTo(90m));
      Assert.That(sut.GetPercentage(50, ew), Is.EqualTo(70m));
      Assert.That(sut.GetPercentage(-70, ew), Is.EqualTo(60m));
      Assert.That(sut.GetPercentage(-110, ew), Is.EqualTo(50m));
      Assert.That(sut.GetPercentage(-140, ew), Is.EqualTo(25m));
      Assert.That(sut.GetPercentage(-200, ew), Is.EqualTo(0m));
    }

    [Test]
    public void ShouldReturn50WhenThereIsOnlyOneScore()
    {
      var ns = Substitute.For<IPosition>();
      ns.IsNorthOrSouth.Returns(true);
      var rawScores = new List<(int, IPosition)>
      {
        (-100, ns),
      };
      var sut = new MatchPointScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetPercentage(-100, ns), Is.EqualTo(50m));
    }
  }
}