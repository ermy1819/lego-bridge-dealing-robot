using System;
using System.Collections.Generic;
using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.ReadModel.Services;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel.Services
{
  internal class ImpScoreServiceFixture
  {
    [Test]
    public void ShouldFailAssertWhenGettingScoreBeforeInitializing()
    {
      var sut = new ImpScoreServiceFactory().CreateService();
      Assert.Throws<AssertionException>(() => sut.GetAverageImp(100, Substitute.For<IPosition>()));
    }

    [Test]
    public void ShouldGet0WhenAllScoresAreTheSame()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (140, position),
                          (140, position),
                          (140, position),
                          (140, position),
                          (140, position),
                          (140, position),
                        };
      var sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetAverageImp(140, position), Is.EqualTo(0m));
    }

    [Test]
    public void ShouldFailAssertWhenScoreIsNotPartOfRawScores()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (-100, position),
                          (-50, position),
                          (70, position),
                          (110, position),
                          (140, position),
                          (200, position),
                        };
      var sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.Throws<AssertionException>(() => sut.GetAverageImp(-140, position));
    }

    [Test]
    public void ShouldGet2WhenScoreIs50BetterThanAllOther()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (190, position),
                          (140, position),
                          (140, position),
                          (140, position),
                          (140, position),
                          (140, position),
                        };
      var sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetAverageImp(190, position), Is.EqualTo(2m));
    }

    [Test]
    public void ShouldGetMinus3WhenScoreIs100LessThanAllOther()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (-40, position),
                          (60, position),
                          (60, position),
                          (60, position),
                          (60, position),
                          (60, position),
                        };
      var sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetAverageImp(-40, position), Is.EqualTo(-3m));
    }

    [TestCase(0, 10, 0)]
    [TestCase(20, 40, 1)]
    [TestCase(50, 80, 2)]
    [TestCase(90, 120, 3)]
    [TestCase(130, 160, 4)]
    [TestCase(170, 210, 5)]
    [TestCase(220, 260, 6)]
    [TestCase(270, 310, 7)]
    [TestCase(320, 360, 8)]
    [TestCase(370, 420, 9)]
    [TestCase(430, 490, 10)]
    [TestCase(500, 590, 11)]
    [TestCase(600, 740, 12)]
    [TestCase(750, 890, 13)]
    [TestCase(900, 1090, 14)]
    [TestCase(1100, 1290, 15)]
    [TestCase(1300, 1490, 16)]
    [TestCase(1500, 1740, 17)]
    [TestCase(1750, 1990, 18)]
    [TestCase(2000, 2240, 19)]
    [TestCase(2250, 2490, 20)]
    [TestCase(2500, 2990, 21)]
    [TestCase(3000, 3490, 22)]
    [TestCase(3500, 3990, 23)]
    [TestCase(4000, 9999, 24)]
    public void ShouldConvertPositiveScoreCorrectlyForSingleDeal(int minDiff, int maxDiff, decimal imps)
    {
      var score = new Random().Next(-1000, 1000);
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (score, position),
                          (score - minDiff, position),
                        };

      var sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetAverageImp(score, position), Is.EqualTo(imps));

      rawScores = new List<(int, IPosition)>
                        {
                          (score, position),
                          (score - maxDiff, position),
                        };

      sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetAverageImp(score, position), Is.EqualTo(imps));
    }

    [TestCase(0, 10, 0)]
    [TestCase(20, 40, 1)]
    [TestCase(50, 80, 2)]
    [TestCase(90, 120, 3)]
    [TestCase(130, 160, 4)]
    [TestCase(170, 210, 5)]
    [TestCase(220, 260, 6)]
    [TestCase(270, 310, 7)]
    [TestCase(320, 360, 8)]
    [TestCase(370, 420, 9)]
    [TestCase(430, 490, 10)]
    [TestCase(500, 590, 11)]
    [TestCase(600, 740, 12)]
    [TestCase(750, 890, 13)]
    [TestCase(900, 1090, 14)]
    [TestCase(1100, 1290, 15)]
    [TestCase(1300, 1490, 16)]
    [TestCase(1500, 1740, 17)]
    [TestCase(1750, 1990, 18)]
    [TestCase(2000, 2240, 19)]
    [TestCase(2250, 2490, 20)]
    [TestCase(2500, 2990, 21)]
    [TestCase(3000, 3490, 22)]
    [TestCase(3500, 3990, 23)]
    [TestCase(4000, 9999, 24)]
    public void ShouldConvertNegativeScoreCorrectlyForSingleDeal(int minDiff, int maxDiff, decimal imps)
    {
      var score = new Random().Next(-1000, 1000);
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (score, position),
                          (score + minDiff, position),
                        };

      var sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetAverageImp(score, position), Is.EqualTo(-imps));

      rawScores = new List<(int, IPosition)>
                    {
                      (score, position),
                      (score + maxDiff, position),
                    };

      sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetAverageImp(score, position), Is.EqualTo(-imps));
    }

    [Test]
    public void ShouldConvertScoresCorrectly()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
                        {
                          (-100, position),
                          (140, position),
                          (-100, position),
                          (-50, position),
                          (200, position),
                          (70, position),
                          (110, position),
                          (140, position),
                          (-100, position),
                          (140, position),
                          (140, position),
                        };
      var sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetAverageImp(-100, position), Is.EqualTo(-4.3m));
      Assert.That(sut.GetAverageImp(-50, position), Is.EqualTo(-2.7m));
      Assert.That(sut.GetAverageImp(70, position), Is.EqualTo(0.5m));
      Assert.That(sut.GetAverageImp(110, position), Is.EqualTo(1.3m));
      Assert.That(sut.GetAverageImp(140, position), Is.EqualTo(2.4m));
      Assert.That(sut.GetAverageImp(200, position), Is.EqualTo(4.2m));
    }

    [Test]
    public void ShouldBeAbleToHandleDifferentDeclarers()
    {
      var ns = Substitute.For<IPosition>();
      var ew = Substitute.For<IPosition>();
      ns.IsNorthOrSouth.Returns(true);
      ew.IsNorthOrSouth.Returns(false);
      var rawScores = new List<(int, IPosition)>
                        {
                          (-100, ns),
                          (100, ew),
                          (-100, ns),
                          (50, ew),
                          (70, ns),
                          (-110, ew),
                          (140, ns),
                          (-140, ew),
                          (140, ns),
                          (-140, ew),
                          (200, ns),
                        };
      var sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetAverageImp(-100, ns), Is.EqualTo(-4.3m));
      Assert.That(sut.GetAverageImp(-50, ns), Is.EqualTo(-2.7m));
      Assert.That(sut.GetAverageImp(70, ns), Is.EqualTo(0.5m));
      Assert.That(sut.GetAverageImp(110, ns), Is.EqualTo(1.3m));
      Assert.That(sut.GetAverageImp(140, ns), Is.EqualTo(2.4m));
      Assert.That(sut.GetAverageImp(200, ns), Is.EqualTo(4.2m));

      Assert.That(sut.GetAverageImp(100, ew), Is.EqualTo(4.3m));
      Assert.That(sut.GetAverageImp(50, ew), Is.EqualTo(2.7m));
      Assert.That(sut.GetAverageImp(-70, ew), Is.EqualTo(-0.5m));
      Assert.That(sut.GetAverageImp(-110, ew), Is.EqualTo(-1.3m));
      Assert.That(sut.GetAverageImp(-140, ew), Is.EqualTo(-2.4m));
      Assert.That(sut.GetAverageImp(-200, ew), Is.EqualTo(-4.2m));
    }

    [Test]
    public void ShouldGet0WhenThereIsOnlyOneScore()
    {
      var position = Substitute.For<IPosition>();
      var rawScores = new List<(int, IPosition)>
      {
        (140, position),
      };
      var sut = new ImpScoreServiceFactory().CreateService();
      sut.Initialize(rawScores);

      Assert.That(sut.GetAverageImp(140, position), Is.EqualTo(0m));
    }
  }
}