﻿using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.ReadModel.Services;
using LegoBridgeDealer.PbnModel.WriteModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel.Services
{
  internal class FairBridgeServiceFixture
  {
    [TestCase(0, 0)]
    [TestCase(1, 0)]
    [TestCase(2, 0)]
    [TestCase(3, 0)]
    [TestCase(4, 0)]
    [TestCase(5, 0)]
    [TestCase(6, 0)]
    [TestCase(7, 0)]
    [TestCase(8, 0)]
    [TestCase(9, 0)]
    [TestCase(10, 0)]
    [TestCase(11, 0)]
    [TestCase(12, 0)]
    [TestCase(13, 0)]
    [TestCase(14, 0)]
    [TestCase(15, 0)]
    [TestCase(16, 0)]
    [TestCase(17, 0)]
    [TestCase(18, 0)]
    [TestCase(19, 0)]
    [TestCase(20, 0)]
    [TestCase(21, 25)]
    [TestCase(22, 38)]
    [TestCase(23, 67)]
    [TestCase(24, 84)]
    [TestCase(25, 90)]
    [TestCase(26, 110)]
    [TestCase(27, 168)]
    [TestCase(28, 274)]
    [TestCase(29, 355)]
    [TestCase(30, 386)]
    [TestCase(31, 416)]
    [TestCase(32, 466)]
    [TestCase(33, 570)]
    [TestCase(34, 719)]
    [TestCase(35, 823)]
    [TestCase(36, 875)]
    [TestCase(37, 1031)]
    [TestCase(38, 1187)]
    [TestCase(39, 1291)]
    [TestCase(40, 1440)]
    [TestCase(41, 1440)]
    [TestCase(42, 1440)]
    [TestCase(43, 1440)]
    [TestCase(44, 1440)]
    [TestCase(45, 1440)]
    [TestCase(46, 1440)]
    [TestCase(47, 1440)]
    [TestCase(48, 1440)]
    [TestCase(49, 1440)]
    [TestCase(50, 1440)]
    [TestCase(51, 1440)]
    [TestCase(52, 1440)]
    [TestCase(53, 1440)]
    [TestCase(54, 1440)]
    public void ShouldGetCorrectParScoreForNonVulnerableMinors(int totalPoints, int expectedPar)
    {
      var sut = new FairBridgeScoreServiceFactory().CreateService(Substitute.For<IDeal>());
      Assert.That(sut.ParScore(totalPoints, Suit.Clubs, false), Is.EqualTo(expectedPar));
      Assert.That(sut.ParScore(totalPoints, Suit.Diamonds, false), Is.EqualTo(expectedPar));
    }

    [TestCase(0, 0)]
    [TestCase(1, 0)]
    [TestCase(2, 0)]
    [TestCase(3, 0)]
    [TestCase(4, 0)]
    [TestCase(5, 0)]
    [TestCase(6, 0)]
    [TestCase(7, 0)]
    [TestCase(8, 0)]
    [TestCase(9, 0)]
    [TestCase(10, 0)]
    [TestCase(11, 0)]
    [TestCase(12, 0)]
    [TestCase(13, 0)]
    [TestCase(14, 0)]
    [TestCase(15, 0)]
    [TestCase(16, 0)]
    [TestCase(17, 0)]
    [TestCase(18, 0)]
    [TestCase(19, 0)]
    [TestCase(20, 0)]
    [TestCase(21, 00)]
    [TestCase(22, 18)]
    [TestCase(23, 57)]
    [TestCase(24, 74)]
    [TestCase(25, 80)]
    [TestCase(26, 105)]
    [TestCase(27, 203)]
    [TestCase(28, 389)]
    [TestCase(29, 522)]
    [TestCase(30, 573)]
    [TestCase(31, 616)]
    [TestCase(32, 691)]
    [TestCase(33, 845)]
    [TestCase(34, 1069)]
    [TestCase(35, 1223)]
    [TestCase(36, 1300)]
    [TestCase(37, 1531)]
    [TestCase(38, 1762)]
    [TestCase(39, 1916)]
    [TestCase(40, 2140)]
    [TestCase(41, 2140)]
    [TestCase(42, 2140)]
    [TestCase(43, 2140)]
    [TestCase(44, 2140)]
    [TestCase(45, 2140)]
    [TestCase(46, 2140)]
    [TestCase(47, 2140)]
    [TestCase(48, 2140)]
    [TestCase(49, 2140)]
    [TestCase(50, 2140)]
    [TestCase(51, 2140)]
    [TestCase(52, 2140)]
    [TestCase(53, 2140)]
    [TestCase(54, 2140)]
    public void ShouldGetCorrectParScoreForVulnerableMinors(int totalPoints, int expectedPar)
    {
      var sut = new FairBridgeScoreServiceFactory().CreateService(Substitute.For<IDeal>());
      Assert.That(sut.ParScore(totalPoints, Suit.Clubs, true), Is.EqualTo(expectedPar));
      Assert.That(sut.ParScore(totalPoints, Suit.Diamonds, true), Is.EqualTo(expectedPar));
    }

    [TestCase(0, 0)]
    [TestCase(1, 0)]
    [TestCase(2, 0)]
    [TestCase(3, 0)]
    [TestCase(4, 0)]
    [TestCase(5, 0)]
    [TestCase(6, 0)]
    [TestCase(7, 0)]
    [TestCase(8, 0)]
    [TestCase(9, 0)]
    [TestCase(10, 0)]
    [TestCase(11, 0)]
    [TestCase(12, 0)]
    [TestCase(13, 0)]
    [TestCase(14, 0)]
    [TestCase(15, 0)]
    [TestCase(16, 0)]
    [TestCase(17, 0)]
    [TestCase(18, 0)]
    [TestCase(19, 0)]
    [TestCase(20, 0)]
    [TestCase(21, 35)]
    [TestCase(22, 49)]
    [TestCase(23, 85)]
    [TestCase(24, 111)]
    [TestCase(25, 170)]
    [TestCase(26, 345)]
    [TestCase(27, 382)]
    [TestCase(28, 391)]
    [TestCase(29, 425)]
    [TestCase(30, 434)]
    [TestCase(31, 459)]
    [TestCase(32, 509)]
    [TestCase(33, 615)]
    [TestCase(34, 771)]
    [TestCase(35, 877)]
    [TestCase(36, 930)]
    [TestCase(37, 1089)]
    [TestCase(38, 1248)]
    [TestCase(39, 1354)]
    [TestCase(40, 1510)]
    [TestCase(41, 1510)]
    [TestCase(42, 1510)]
    [TestCase(43, 1510)]
    [TestCase(44, 1510)]
    [TestCase(45, 1510)]
    [TestCase(46, 1510)]
    [TestCase(47, 1510)]
    [TestCase(48, 1510)]
    [TestCase(49, 1510)]
    [TestCase(50, 1510)]
    [TestCase(51, 1510)]
    [TestCase(52, 1510)]
    [TestCase(53, 1510)]
    [TestCase(54, 1510)]
    public void ShouldGetCorrectParScoreForNonVulnerableMajors(int totalPoints, int expectedPar)
    {
      var sut = new FairBridgeScoreServiceFactory().CreateService(Substitute.For<IDeal>());
      Assert.That(sut.ParScore(totalPoints, Suit.Hearts, false), Is.EqualTo(expectedPar));
      Assert.That(sut.ParScore(totalPoints, Suit.Spades, false), Is.EqualTo(expectedPar));
    }

    [TestCase(0, 0)]
    [TestCase(1, 0)]
    [TestCase(2, 0)]
    [TestCase(3, 0)]
    [TestCase(4, 0)]
    [TestCase(5, 0)]
    [TestCase(6, 0)]
    [TestCase(7, 0)]
    [TestCase(8, 0)]
    [TestCase(9, 0)]
    [TestCase(10, 0)]
    [TestCase(11, 0)]
    [TestCase(12, 0)]
    [TestCase(13, 0)]
    [TestCase(14, 0)]
    [TestCase(15, 0)]
    [TestCase(16, 0)]
    [TestCase(17, 0)]
    [TestCase(18, 0)]
    [TestCase(19, 0)]
    [TestCase(20, 0)]
    [TestCase(21, 10)]
    [TestCase(22, 29)]
    [TestCase(23, 75)]
    [TestCase(24, 101)]
    [TestCase(25, 200)]
    [TestCase(26, 500)]
    [TestCase(27, 557)]
    [TestCase(28, 566)]
    [TestCase(29, 612)]
    [TestCase(30, 621)]
    [TestCase(31, 659)]
    [TestCase(32, 734)]
    [TestCase(33, 890)]
    [TestCase(34, 1121)]
    [TestCase(35, 1277)]
    [TestCase(36, 1355)]
    [TestCase(37, 1589)]
    [TestCase(38, 1823)]
    [TestCase(39, 1979)]
    [TestCase(40, 2210)]
    [TestCase(41, 2210)]
    [TestCase(42, 2210)]
    [TestCase(43, 2210)]
    [TestCase(44, 2210)]
    [TestCase(45, 2210)]
    [TestCase(46, 2210)]
    [TestCase(47, 2210)]
    [TestCase(48, 2210)]
    [TestCase(49, 2210)]
    [TestCase(50, 2210)]
    [TestCase(51, 2210)]
    [TestCase(52, 2210)]
    [TestCase(53, 2210)]
    [TestCase(54, 2210)]
    public void ShouldGetCorrectParScoreForVulnerableMajors(int totalPoints, int expectedPar)
    {
      var sut = new FairBridgeScoreServiceFactory().CreateService(Substitute.For<IDeal>());
      Assert.That(sut.ParScore(totalPoints, Suit.Hearts, true), Is.EqualTo(expectedPar));
      Assert.That(sut.ParScore(totalPoints, Suit.Spades, true), Is.EqualTo(expectedPar));
    }

    [TestCase(0, 0)]
    [TestCase(1, 0)]
    [TestCase(2, 0)]
    [TestCase(3, 0)]
    [TestCase(4, 0)]
    [TestCase(5, 0)]
    [TestCase(6, 0)]
    [TestCase(7, 0)]
    [TestCase(8, 0)]
    [TestCase(9, 0)]
    [TestCase(10, 0)]
    [TestCase(11, 0)]
    [TestCase(12, 0)]
    [TestCase(13, 0)]
    [TestCase(14, 0)]
    [TestCase(15, 0)]
    [TestCase(16, 0)]
    [TestCase(17, 0)]
    [TestCase(18, 0)]
    [TestCase(19, 0)]
    [TestCase(20, 0)]
    [TestCase(21, 32)]
    [TestCase(22, 46)]
    [TestCase(23, 75)]
    [TestCase(24, 120)]
    [TestCase(25, 254)]
    [TestCase(26, 327)]
    [TestCase(27, 364)]
    [TestCase(28, 373)]
    [TestCase(29, 410)]
    [TestCase(30, 450)]
    [TestCase(31, 452)]
    [TestCase(32, 502)]
    [TestCase(33, 614)]
    [TestCase(34, 774)]
    [TestCase(35, 886)]
    [TestCase(36, 939)]
    [TestCase(37, 1098)]
    [TestCase(38, 1257)]
    [TestCase(39, 1363)]
    [TestCase(40, 1520)]
    [TestCase(41, 1520)]
    [TestCase(42, 1520)]
    [TestCase(43, 1520)]
    [TestCase(44, 1520)]
    [TestCase(45, 1520)]
    [TestCase(46, 1520)]
    [TestCase(47, 1520)]
    [TestCase(48, 1520)]
    [TestCase(49, 1520)]
    [TestCase(50, 1520)]
    [TestCase(51, 1520)]
    [TestCase(52, 1520)]
    [TestCase(53, 1520)]
    [TestCase(54, 1520)]
    public void ShouldGetCorrectParScoreForNonVulnerableNoTrump(int totalPoints, int expectedPar)
    {
      var sut = new FairBridgeScoreServiceFactory().CreateService(Substitute.For<IDeal>());
      Assert.That(sut.ParScore(totalPoints, Suit.NoTrump, false), Is.EqualTo(expectedPar));
    }

    [TestCase(0, 0)]
    [TestCase(1, 0)]
    [TestCase(2, 0)]
    [TestCase(3, 0)]
    [TestCase(4, 0)]
    [TestCase(5, 0)]
    [TestCase(6, 0)]
    [TestCase(7, 0)]
    [TestCase(8, 0)]
    [TestCase(9, 0)]
    [TestCase(10, 0)]
    [TestCase(11, 0)]
    [TestCase(12, 0)]
    [TestCase(13, 0)]
    [TestCase(14, 0)]
    [TestCase(15, 0)]
    [TestCase(16, 0)]
    [TestCase(17, 0)]
    [TestCase(18, 0)]
    [TestCase(19, 0)]
    [TestCase(20, 0)]
    [TestCase(21, 07)]
    [TestCase(22, 26)]
    [TestCase(23, 65)]
    [TestCase(24, 130)]
    [TestCase(25, 364)]
    [TestCase(26, 482)]
    [TestCase(27, 539)]
    [TestCase(28, 548)]
    [TestCase(29, 597)]
    [TestCase(30, 650)]
    [TestCase(31, 652)]
    [TestCase(32, 727)]
    [TestCase(33, 889)]
    [TestCase(34, 1124)]
    [TestCase(35, 1286)]
    [TestCase(36, 1364)]
    [TestCase(37, 1598)]
    [TestCase(38, 1832)]
    [TestCase(39, 1988)]
    [TestCase(40, 2220)]
    [TestCase(41, 2220)]
    [TestCase(42, 2220)]
    [TestCase(43, 2220)]
    [TestCase(44, 2220)]
    [TestCase(45, 2220)]
    [TestCase(46, 2220)]
    [TestCase(47, 2220)]
    [TestCase(48, 2220)]
    [TestCase(49, 2220)]
    [TestCase(50, 2220)]
    [TestCase(51, 2220)]
    [TestCase(52, 2220)]
    [TestCase(53, 2220)]
    [TestCase(54, 2220)]
    public void ShouldGetCorrectParScoreForVulnerableNoTrump(int totalPoints, int expectedPar)
    {
      var sut = new FairBridgeScoreServiceFactory().CreateService(Substitute.For<IDeal>());
      Assert.That(sut.ParScore(totalPoints, Suit.NoTrump, true), Is.EqualTo(expectedPar));
    }

    [Test]
    public void PrintScoreTable()
    {
      var sut = new FairBridgeScoreServiceFactory().CreateService(Substitute.For<IDeal>());
      TestContext.Out.WriteLine("Poäng Lågfärg(ozon) Högfärg(ozon) Sang(ozon) Lågfärg(zon) Högfärg(son) Sang(zon)");
      for (int totalPoints = 20; totalPoints < 41; totalPoints++)
        TestContext.Out.WriteLine($"{totalPoints} {sut.ParScore(totalPoints, Suit.Clubs, false)} {sut.ParScore(totalPoints, Suit.Hearts, false)} {sut.ParScore(totalPoints, Suit.NoTrump, false)} {sut.ParScore(totalPoints, Suit.Clubs, true)} {sut.ParScore(totalPoints, Suit.Hearts, true)} {sut.ParScore(totalPoints, Suit.NoTrump, true)}");
    }

    [TestCase(150, 150, 0)]
    [TestCase(50, -50, 100)]
    [TestCase(240, 130, 109)]
    [TestCase(-50, 150, -190)]
    [TestCase(-100, 110, -196)]
    [TestCase(520, 220, 250)]
    [TestCase(-800, -490, -254)]
    [TestCase(920, 520, 290)]
    [TestCase(-50, 360, -292)]
    [TestCase(-100, 400, -315)]
    [TestCase(420, -90, 317)]
    [TestCase(-100, 600, -355)]
    [TestCase(-100, 610, -356)]
    [TestCase(-800, 200, -400)]
    [TestCase(1100, 90, 401)]
    [TestCase(-500, 2000, -550)]
    [TestCase(-7600, 2220, -1282)]
    public void ShouldReduceLargeScores(int contractScore, int parScore, int result)
    {
      var sut = new FairBridgeScoreServiceFactory().CreateService(Substitute.For<IDeal>());
      Assert.That(sut.GetDealScore(contractScore, parScore), Is.EqualTo(result));
    }

    [TestCase(Suit.Hearts, false, true, 24, 26, 345)]
    [TestCase(Suit.Spades, true, false, 18, 23, 75)]
    [TestCase(Suit.Clubs, true, true, 21, 23, 57)]
    [TestCase(Suit.Diamonds, false, false, 25, 26, 254)]
    [TestCase(Suit.NoTrump, false, true, 24, 24, 120)]
    [TestCase(Suit.NoTrump, false, true, 19, 19, -7)]
    [TestCase(Suit.Spades, true, false, 18, 19, -46)]
    [TestCase(Suit.Clubs, true, true, 17, 22, -65)]
    [TestCase(Suit.Hearts, false, true, 12, 18, -548)]
    public void ShouldGetParForPlayedSuitWhenGettingFairBridgeScore(Suit playedSuit, bool vulnerable, bool opponentsVulnerable, int hcp, int tp, int score)
    {
      var declarer = Substitute.For<IPosition>();
      declarer.IsNorthOrSouth.Returns(true);
      var opponent = Substitute.For<IPosition>();
      opponent.IsNorthOrSouth.Returns(false);
      var contract = Substitute.For<IContract>();
      var suit = Substitute.For<ISuit>();
      contract.Suit.Returns(suit);
      suit.Suit.Returns(playedSuit);
      suit.IsMinor.Returns(playedSuit == Suit.Clubs || playedSuit == Suit.Diamonds);
      contract.Declarer.Returns(declarer);
      contract.Opponent.Returns(opponent);
      var deal = Substitute.For<IDeal>();
      var vulnerability = Substitute.For<IVulnerability>();
      deal.Vulnerability.Returns(vulnerability);
      vulnerability.IsVulnerable(declarer).Returns(vulnerable);
      vulnerability.IsVulnerable(opponent).Returns(opponentsVulnerable);
      var cards = Substitute.For<ICards>();
      deal.Cards.Returns(cards);
      cards.NorthSouthTotalPoints(playedSuit).Returns(tp);
      cards.NorthSouthTotalPoints(Suit.NoTrump).Returns(hcp);

      var sut = new FairBridgeScoreServiceFactory().CreateService(deal);
      Assert.That(sut.GetFairBridgePar(declarer, contract), Is.EqualTo(score));
      Assert.That(sut.GetFairBridgePar(opponent, contract), Is.EqualTo(-score));
    }

    [TestCase(false, true,  24, 2, 1, 1, 3, 0, 3, 0, 2, 345)]
    [TestCase(true,  true,  37, 0, 0, 2, 1, 0, 0, 0, 0, 1916)]
    [TestCase(false, false, 12, 3, 0, 1, 0, 0, 0, 1, 0, -373)]
    [TestCase(true,  false, 23, 0, 0, 0, 0, 0, 5, 0, 0, 65)]
    [TestCase(false, true,  27, 0, 1, 0, 3, 2, 0, 4, 0, 391)]
    [TestCase(true,  true,  20, 6, 0, 2, 0, 7, 0, 0, 0, -200)]
    [TestCase(false, false, 32, 0, 1, 0, 1, 5, 0, 0, 0, 615)]
    [TestCase(true,  false, 18, 0, 0, 5, 0, 0, 0, 0, 0, 57)]
    [TestCase(true,  false, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
    [TestCase(true,  true,  20, 0, 6, 0, 0, 6, 0, 0, 0, -200)]
    [TestCase(false, false, 21, 4, 0, 0, 0, 0, 6, 0, 2, 100)]
    [TestCase(true,  true,  20, 0, 0, 0, 5, 0, 0, 5, 0, -80)]
    [TestCase(false, false, 21, 0, 0, 0, 4, 0, 2, 6, 0, -90)]
    [TestCase(true,  true,  22, 0, 0, 0, 1, 0, 0, 0, 5, 0)]
    public void ShouldGetParForBestSuitWhenGettingWallinScore(bool vulnerable, bool opponentsVulnerable, int hcp, int epSpades, int epHearts, int epDiamonds, int epClubs, int epOpSpades, int epOpHearts, int epOpDiamonds, int epOpClubs, int score)
    {
      var declarer = Substitute.For<IPosition>();
      declarer.IsNorthOrSouth.Returns(true);
      var opponent = Substitute.For<IPosition>();
      opponent.IsNorthOrSouth.Returns(false);
      var deal = Substitute.For<IDeal>();
      var vulnerability = Substitute.For<IVulnerability>();
      deal.Vulnerability.Returns(vulnerability);
      vulnerability.IsVulnerable(PlayerDirection.North).Returns(vulnerable);
      vulnerability.IsVulnerable(PlayerDirection.East).Returns(opponentsVulnerable);
      var cards = Substitute.For<ICards>();
      deal.Cards.Returns(cards);
      cards.NorthSouthTotalPoints(Suit.NoTrump).Returns(hcp);
      cards.NorthSouthTotalPoints(Suit.Spades).Returns(hcp + epSpades);
      cards.NorthSouthTotalPoints(Suit.Hearts).Returns(hcp + epHearts);
      cards.NorthSouthTotalPoints(Suit.Diamonds).Returns(hcp + epDiamonds);
      cards.NorthSouthTotalPoints(Suit.Clubs).Returns(hcp + epClubs);
      cards.EastWestTotalPoints(Suit.NoTrump).Returns(40 - hcp);
      cards.EastWestTotalPoints(Suit.Spades).Returns(40 - hcp + epOpSpades);
      cards.EastWestTotalPoints(Suit.Hearts).Returns(40 - hcp + epOpHearts);
      cards.EastWestTotalPoints(Suit.Diamonds).Returns(40 - hcp + epOpDiamonds);
      cards.EastWestTotalPoints(Suit.Clubs).Returns(40 - hcp + epOpClubs);

      var sut = new FairBridgeScoreServiceFactory().CreateService(deal);
      Assert.That(sut.GetWallinPar(declarer), Is.EqualTo(score));
      Assert.That(sut.GetWallinPar(opponent), Is.EqualTo(-score));
    }
  }
}