﻿using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.ReadModel.Services;
using LegoBridgeDealer.PbnModel.WriteModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class PlayerDealFixture
  {
    [Test]
    public void ShouldCallPlayerFactoryWhenConstructing()
    {
      var sut = new PlayerDealFactory { PlayerFactory = Substitute.For<IPlayerFactory>() };
      var player = new Player();
      var createdPlayer = Substitute.For<IPlayer>();
      sut.PlayerFactory.CreatePlayer(player).Returns(createdPlayer);

      var result = sut.CreatePlayerDeal(new Game(), player, Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());

      Assert.That(result.Player, Is.SameAs(createdPlayer));
    }

    [Test]
    public void ShouldCallDealResultFactoryWhenConstructing()
    {
      var sut = new PlayerDealFactory { DealResultFactory = Substitute.For<IDealResultFactory>() };
      var game = new Game { Result = 737, Contract = new Contract() };
      var dealResult = Substitute.For<IDealResult>();
      sut.DealResultFactory.CreateDealResult(game.Result, game.Contract).Returns(dealResult);

      var result = sut.CreatePlayerDeal(game, new Player(), Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());

      Assert.That(result.Result, Is.SameAs(dealResult));
    }

    [Test]
    public void ShouldCallContractFactoryWhenConstructing()
    {
      var sut = new PlayerDealFactory { ContractFactory = Substitute.For<IContractFactory>() };
      var game = new Game
      {
        Contract = new Contract(),
        Declarer = PlayerDirection.South,
        Vulnerable = Vulnerability.EastWest
      };
      var contract = Substitute.For<IContract>();
      sut.ContractFactory.CreateContract(game.Contract, game.Declarer, game.Vulnerable).Returns(contract);

      var result = sut.CreatePlayerDeal(game, new Player(), Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());

      Assert.That(result.Contract, Is.SameAs(contract));
    }

    [Test]
    public void ShouldCallPositionFactoryWhenConstructing()
    {
      var sut = new PlayerDealFactory { PositionFactory = Substitute.For<IPositionFactory>() };
      var player = new Player { Id = 2 };
      var game = new Game { Declarer = PlayerDirection.North, EastId = 2 };

      var declarer = Substitute.For<IPosition>();
      var position = Substitute.For<IPosition>();
      var role = Substitute.For<IPosition>();

      sut.PositionFactory.CreatePosition(game.Declarer).Returns(declarer);
      sut.PositionFactory.CreatePosition(PlayerDirection.East).Returns(position);
      sut.PositionFactory.CreatePosition(PlayerDirection.RightHandDefender).Returns(role);

      var result = sut.CreatePlayerDeal(game, player, Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());

      Assert.That(result.Position, Is.SameAs(position));
      Assert.That(result.Role, Is.SameAs(role));
    }

    [TestCase(1, PlayerDirection.North)]
    [TestCase(2, PlayerDirection.East)]
    [TestCase(3, PlayerDirection.South)]
    [TestCase(4, PlayerDirection.West)]
    public void ShouldFindPlayerPositionWhenPlayerIsUnique(int playerId, PlayerDirection expectedDirection)
    {
      var sut = new PlayerDealFactory { PositionFactory = Substitute.For<IPositionFactory>() };
      var player = new Player { Id = playerId };
      var game = new Game { NorthId = 1, EastId = 2, SouthId = 3, WestId = 4 };

      var position = Substitute.For<IPosition>();

      sut.PositionFactory.CreatePosition(expectedDirection).Returns(position);

      var result = sut.CreatePlayerDeal(game, player, Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());

      Assert.That(result.Position, Is.SameAs(position));
    }

    [TestCase(1, PlayerDirection.North, PlayerDirection.Declarer)]
    [TestCase(1, PlayerDirection.East, PlayerDirection.LeftHandDefender)]
    [TestCase(1, PlayerDirection.South, PlayerDirection.Dummy)]
    [TestCase(1, PlayerDirection.West, PlayerDirection.RightHandDefender)]
    [TestCase(2, PlayerDirection.North, PlayerDirection.RightHandDefender)]
    [TestCase(2, PlayerDirection.East, PlayerDirection.Declarer)]
    [TestCase(2, PlayerDirection.South, PlayerDirection.LeftHandDefender)]
    [TestCase(2, PlayerDirection.West, PlayerDirection.Dummy)]
    [TestCase(3, PlayerDirection.North, PlayerDirection.Dummy)]
    [TestCase(3, PlayerDirection.East, PlayerDirection.RightHandDefender)]
    [TestCase(3, PlayerDirection.South, PlayerDirection.Declarer)]
    [TestCase(3, PlayerDirection.West, PlayerDirection.LeftHandDefender)]
    [TestCase(4, PlayerDirection.North, PlayerDirection.LeftHandDefender)]
    [TestCase(4, PlayerDirection.East, PlayerDirection.Dummy)]
    [TestCase(4, PlayerDirection.South, PlayerDirection.RightHandDefender)]
    [TestCase(4, PlayerDirection.West, PlayerDirection.Declarer)]
    public void ShouldFindPlayerRoleWhenPlayerIsUnique(int playerId, PlayerDirection declarer, PlayerDirection expectedRole)
    {
      var sut = new PlayerDealFactory { PositionFactory = Substitute.For<IPositionFactory>() };
      var player = new Player { Id = playerId };
      var game = new Game { NorthId = 1, EastId = 2, SouthId = 3, WestId = 4, Declarer = declarer };

      var position = Substitute.For<IPosition>();

      sut.PositionFactory.CreatePosition(expectedRole).Returns(position);

      var result = sut.CreatePlayerDeal(game, player, Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());

      Assert.That(result.Role, Is.SameAs(position));
    }

    [Test]
    public void ShouldGetScoreFromWriteModelGameScore()
    {
      var sut = new PlayerDealFactory();
      var game = new Game { Score = new Score(345), ScoreMP = 12, ScoreIMP = -2, ScorePercentage = 35.25f };

      var result = sut.CreatePlayerDeal(game, new Player(), Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());
      Assert.That(result.Score, Is.EqualTo(345));
      Assert.That(result.ScoreMP, Is.EqualTo(35.25));
      Assert.That(result.ScoreIMP, Is.EqualTo(-2));
    }

    [Test]
    public void ShouldGetNegativeScoreFromWriteModelGameScoreWhenPlayerIsDefender()
    {
      // Note: For percentage score, the defenders score is 100-ScorePercentage.
      var sut = new PlayerDealFactory();
      var game = new Game { Score = new Score(345), ScoreMP = 12, ScoreIMP = -2, ScorePercentage = 35.25f, EastId = 1 };

      var result = sut.CreatePlayerDeal(game, new Player { Id = 1 }, Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());
      Assert.That(result.Score, Is.EqualTo(-345));
      Assert.That(result.ScoreMP, Is.EqualTo(64.75));
      Assert.That(result.ScoreIMP, Is.EqualTo(2));
    }

    [Test]
    public void ShouldGetScoreFromScoreServiceWhenScoreIsZero()
    {
      var sut = new PlayerDealFactory
      {
        DealResultFactory = Substitute.For<IDealResultFactory>(),
        ContractFactory = Substitute.For<IContractFactory>(),
        BridgeScoreService = Substitute.For<IBridgeScoreService>()
      };

      var dealResult = Substitute.For<IDealResult>();
      sut.DealResultFactory.CreateDealResult(8, new Contract()).Returns(dealResult);
      var contract = Substitute.For<IContract>();
      sut.ContractFactory.CreateContract(new Contract(), PlayerDirection.East, Vulnerability.EastWest)
        .Returns(contract);
      sut.BridgeScoreService.Compute(dealResult, contract).Returns(768);
      var game = new Game
      {
        EastId = 1,
        Score = new Score(0),
        Result = 8,
        Declarer = PlayerDirection.East,
        Vulnerable = Vulnerability.EastWest
      };

      var result = sut.CreatePlayerDeal(game, new Player { Id = 1 }, Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());
      Assert.That(result.Score, Is.EqualTo(768));
    }

    [Test]
    public void ShouldNegateScoreFromScoreServiceWhenPlayerIsDefender()
    {
      var sut = new PlayerDealFactory
      {
        DealResultFactory = Substitute.For<IDealResultFactory>(),
        ContractFactory = Substitute.For<IContractFactory>(),
        BridgeScoreService = Substitute.For<IBridgeScoreService>()
      };

      var dealResult = Substitute.For<IDealResult>();
      sut.DealResultFactory.CreateDealResult(8, new Contract()).Returns(dealResult);
      var contract = Substitute.For<IContract>();
      sut.ContractFactory.CreateContract(new Contract(), PlayerDirection.East, Vulnerability.EastWest)
        .Returns(contract);
      sut.BridgeScoreService.Compute(dealResult, contract).Returns(768);
      var game = new Game
      {
        NorthId = 1,
        Score = new Score(0),
        Result = 8,
        Declarer = PlayerDirection.East,
        Vulnerable = Vulnerability.EastWest
      };

      var result = sut.CreatePlayerDeal(game, new Player { Id = 1 }, Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());
      Assert.That(result.Score, Is.EqualTo(-768));
    }

    [Test]
    public void ShouldSetPositionToNorthIfPlayerIsNull()
    {
      var sut = new PlayerDealFactory { PositionFactory = Substitute.For<IPositionFactory>() };
      var position = Substitute.For<IPosition>();
      sut.PositionFactory.CreatePosition(PlayerDirection.North).Returns(position);

      var result = sut.CreatePlayerDeal(new Game(), null, Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());

      Assert.That(result.Position, Is.SameAs(position));
    }

    [Test]
    public void ShouldCreatePatientWithNorthIdAsNameWhenPatientIsNull()
    {
      var sut = new PlayerDealFactory();

      var result = sut.CreatePlayerDeal(new Game { NorthId = 17 }, null, Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());

      Assert.That(result.Player.Name, Is.EqualTo("17"));
    }

    [Test]
    public void ShouldGetMatchPointsFromMatchPointServiceWhenScorePercentageIsNull()
    {
      var sut = new PlayerDealFactory
      {
        PositionFactory = Substitute.For<IPositionFactory>()
      };

      var position = Substitute.For<IPosition>();
      sut.PositionFactory.CreatePosition(PlayerDirection.East).Returns(position);
      var game = new Game { Score = new Score(173), ScorePercentage = null, EastId = 1 };

      var matchPointScoreService = Substitute.For<IMatchPointScoreService>();
      matchPointScoreService.GetPercentage(173, position).Returns(12.5m);
      var result = sut.CreatePlayerDeal(game, new Player { Id = 1 }, matchPointScoreService, Substitute.For<IImpScoreService>(), Substitute.For<IFairBridgeScoreService>());
      Assert.That(result.ScoreMP, Is.EqualTo(12.5m));
    }

    [Test]
    public void ShouldGetImpFromImpServiceWhenScoreImpIsNull()
    {
      var sut = new PlayerDealFactory
      {
        PositionFactory = Substitute.For<IPositionFactory>()
      };

      var position = Substitute.For<IPosition>();
      sut.PositionFactory.CreatePosition(PlayerDirection.East).Returns(position);
      var game = new Game { Score = new Score(173), ScoreIMP = null, EastId = 1 };

      var scoreService = Substitute.For<IImpScoreService>();
      scoreService.GetAverageImp(173, position).Returns(12.5m);
      var result = sut.CreatePlayerDeal(game, new Player { Id = 1 }, Substitute.For<IMatchPointScoreService>(), scoreService, Substitute.For<IFairBridgeScoreService>());
      Assert.That(result.ScoreIMP, Is.EqualTo(12.5m));
    }

    [Test]
    public void ShouldGetFairBridgeAndWallinScoreFromFairBridgeService()
    {
      var sut = new PlayerDealFactory
      {
        PositionFactory = Substitute.For<IPositionFactory>(),
        ContractFactory = Substitute.For<IContractFactory>()
      };

      var position = Substitute.For<IPosition>();
      sut.PositionFactory.CreatePosition(PlayerDirection.East).Returns(position);
      var contract = Substitute.For<IContract>();
      sut.ContractFactory.CreateContract(Arg.Any<Contract>(), Arg.Any<PlayerDirection>(), Arg.Any<Vulnerability>())
        .Returns(contract);
      var player1 = new Player { Id = 1 };
      var game = new Game { Score = new Score(173), EastId = 1 };

      var scoreService = Substitute.For<IFairBridgeScoreService>();
      scoreService.GetFairBridgePar(position, contract).Returns(125);
      scoreService.GetWallinPar(position).Returns(111);
      scoreService.GetDealScore(173, 111).Returns(55);
      scoreService.GetDealScore(173, 125).Returns(44);

      var result = sut.CreatePlayerDeal(game, player1, Substitute.For<IMatchPointScoreService>(), Substitute.For<IImpScoreService>(), scoreService);
      Assert.That(result.ParScoreFairBridge, Is.EqualTo(125));
      Assert.That(result.ParScoreWallin, Is.EqualTo(111));
      Assert.That(result.ScoreFairBridge, Is.EqualTo(44));
      Assert.That(result.ScoreWallin, Is.EqualTo(55));
    }  
  }
}