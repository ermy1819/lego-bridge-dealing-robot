using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class CardsFixture
  {
    [TestCase("N:north east south west")]
    [TestCase("W:west north east south")]
    [TestCase("S:south west north east")]
    [TestCase("E:east south west north")]
    public void ShouldSplitDealIntoFourHandsWhenCreatingCards(string input)
    {
      var sut = new CardsFactory
                  {
                    HandFactory = Substitute.For<IHandFactory>(),
                  };

      var north = Substitute.For<IHand>();
      var east = Substitute.For<IHand>();
      var south = Substitute.For<IHand>();
      var west = Substitute.For<IHand>();
      sut.HandFactory.CreateHand("north").Returns(north);
      sut.HandFactory.CreateHand("east").Returns(east);
      sut.HandFactory.CreateHand("south").Returns(south);
      sut.HandFactory.CreateHand("west").Returns(west);

      var cards = sut.CreateCards(input);
      Assert.That(cards.North, Is.SameAs(north));
      Assert.That(cards.East, Is.SameAs(east));
      Assert.That(cards.South, Is.SameAs(south));
      Assert.That(cards.West, Is.SameAs(west));
    }

    [Test]
    public void ShouldSumHighCardPointsAndExtraPointsToTotalPoints()
    {
      var sut = new CardsFactory
                  {
                    HandFactory = Substitute.For<IHandFactory>(),
                  };

      var north = Substitute.For<IHand>();
      var east = Substitute.For<IHand>();
      var south = Substitute.For<IHand>();
      var west = Substitute.For<IHand>();
      sut.HandFactory.CreateHand("north").Returns(north);
      sut.HandFactory.CreateHand("east").Returns(east);
      sut.HandFactory.CreateHand("south").Returns(south);
      sut.HandFactory.CreateHand("west").Returns(west);
      north.HighCardPoints.Returns(13);
      north.ExtraPoints(Suit.Hearts, 3).Returns((2, 1, 0, 3));
      north.SuitLength(Suit.Hearts).Returns(5);
      south.HighCardPoints.Returns(8);
      south.ExtraPoints(Suit.Hearts, 5).Returns((0, 2, 0, 1));
      south.SuitLength(Suit.Hearts).Returns(3);

      var cards = sut.CreateCards("N:north east south west");
      Assert.That(cards.NorthSouthTotalPoints(Suit.Hearts), Is.EqualTo(28));

      east.HighCardPoints.Returns(7);
      east.ExtraPoints(Suit.Diamonds, 6).Returns((0, 1, 2, 0));
      east.SuitLength(Suit.Diamonds).Returns(4);
      west.HighCardPoints.Returns(14);
      west.ExtraPoints(Suit.Diamonds, 4).Returns((0, 0, 2, 0));
      west.SuitLength(Suit.Diamonds).Returns(6);

      Assert.That(cards.EastWestTotalPoints(Suit.Diamonds), Is.EqualTo(24));
    }

    [Test]
    public void ShouldReturnNullTotalPointWhenNorthHighCardPointsIsNull()
    {
      var sut = new CardsFactory
                  {
                    HandFactory = Substitute.For<IHandFactory>(),
                  };

      var north = Substitute.For<IHand>();
      var south = Substitute.For<IHand>();
      sut.HandFactory.CreateHand("north").Returns(north);
      sut.HandFactory.CreateHand("south").Returns(south);
      north.HighCardPoints.Returns((int?)null);
      north.ExtraPoints(Suit.Hearts, 3).Returns((2, 1, 0, 3));
      north.SuitLength(Suit.Hearts).Returns(5);
      south.HighCardPoints.Returns(8);
      south.ExtraPoints(Suit.Hearts, 5).Returns((0, 2, 0, 1));
      south.SuitLength(Suit.Hearts).Returns(3);

      var cards = sut.CreateCards("N:north east south west");
      Assert.That(cards.NorthSouthTotalPoints(Suit.Hearts), Is.EqualTo(null));
    }

    [Test]
    public void ShouldReturnNullTotalPointWhenSouthHighCardPointsIsNull()
    {
      var sut = new CardsFactory
                  {
                    HandFactory = Substitute.For<IHandFactory>(),
                  };

      var north = Substitute.For<IHand>();
      var south = Substitute.For<IHand>();
      sut.HandFactory.CreateHand("north").Returns(north);
      sut.HandFactory.CreateHand("south").Returns(south);
      north.HighCardPoints.Returns(13);
      north.ExtraPoints(Suit.Hearts, 3).Returns((2, 1, 0, 3));
      north.SuitLength(Suit.Hearts).Returns(5);
      south.HighCardPoints.Returns((int?)null);
      south.ExtraPoints(Suit.Hearts, 5).Returns((0, 2, 0, 1));
      south.SuitLength(Suit.Hearts).Returns(3);

      var cards = sut.CreateCards("N:north east south west");
      Assert.That(cards.NorthSouthTotalPoints(Suit.Hearts), Is.EqualTo(null));
    }

    [Test]
    public void ShouldReturnNullTotalPointWhenNorthExtraPointsIsNull()
    {
      var sut = new CardsFactory
                  {
                    HandFactory = Substitute.For<IHandFactory>(),
                  };

      var north = Substitute.For<IHand>();
      var south = Substitute.For<IHand>();
      sut.HandFactory.CreateHand("north").Returns(north);
      sut.HandFactory.CreateHand("south").Returns(south);
      north.HighCardPoints.Returns(13);
      north.ExtraPoints(Suit.Hearts, 3).Returns(((int, int, int, int)?)null);
      north.SuitLength(Suit.Hearts).Returns(5);
      south.HighCardPoints.Returns(8);
      south.ExtraPoints(Suit.Hearts, 5).Returns((0, 2, 0, 1));
      south.SuitLength(Suit.Hearts).Returns(3);

      var cards = sut.CreateCards("N:north east south west");
      Assert.That(cards.NorthSouthTotalPoints(Suit.Hearts), Is.EqualTo(null));
    }

    [Test]
    public void ShouldReturnNullTotalPointWhenSouthExtraPointsIsNull()
    {
      var sut = new CardsFactory
                  {
                    HandFactory = Substitute.For<IHandFactory>(),
                  };

      var north = Substitute.For<IHand>();
      var south = Substitute.For<IHand>();
      sut.HandFactory.CreateHand("north").Returns(north);
      sut.HandFactory.CreateHand("south").Returns(south);
      north.HighCardPoints.Returns(13);
      north.ExtraPoints(Suit.Hearts, 3).Returns((2, 1, 0, 3));
      north.SuitLength(Suit.Hearts).Returns(5);
      south.HighCardPoints.Returns(8);
      south.ExtraPoints(Suit.Hearts, 5).Returns(((int, int, int, int)?)null);
      south.SuitLength(Suit.Hearts).Returns(3);

      var cards = sut.CreateCards("N:north east south west");
      Assert.That(cards.NorthSouthTotalPoints(Suit.Hearts), Is.EqualTo(null));
    }
  }
}