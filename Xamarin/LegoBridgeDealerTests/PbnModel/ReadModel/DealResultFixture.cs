﻿using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class DealResultFixture
  {
    [TestCase(9, 3, "=")]
    [TestCase(8, 1, "+1")]
    [TestCase(12, 3, "+3")]
    [TestCase(9, 4, "-1")]
    [TestCase(5, 1, "-2")]
    [TestCase(0, 0, "-")]
    public void ShouldBeReadableWhenConvertingToString(int result, int bid, string expected)
    {
      var sut = new DealResultFactory();
      var contract = new Contract(bid, Suit.NoTrump, Risk.Undoubled);
      Assert.That(sut.CreateDealResult(result, contract).Tricks, Is.EqualTo(result));
      Assert.That(sut.CreateDealResult(result, contract).ToString(), Is.EqualTo(expected));
    }

    [Test]
    public void ShouldSortFromWorstToBest()
    {
      var sut = new DealResultFactory();
      var expectedOrder = new List<IDealResult>
                            {
                              sut.CreateDealResult(0, new Contract(7, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(1, new Contract(7, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(0, new Contract(5, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(2, new Contract(6, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(4, new Contract(7, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(2, new Contract(4, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(4, new Contract(5, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(6, new Contract(6, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(2, new Contract(1, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(6, new Contract(4, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(8, new Contract(5, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(5, new Contract(1, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(10, new Contract(5, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(10, new Contract(4, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(10, new Contract(3, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(12, new Contract(4, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(10, new Contract(1, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(13, new Contract(3, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(12, new Contract(1, Suit.Hearts, Risk.Undoubled)),
                              sut.CreateDealResult(13, new Contract(1, Suit.Hearts, Risk.Undoubled)),
                            };
      Assert.That(expectedOrder.Select(r => r.RelativeTricks), Is.EqualTo(Enumerable.Range(-13, 20)));
    }

    [Test]
    public void ShouldReturnDefeatedWhenRelativeTrickScoreIsNegative()
    {
      var sut = new DealResultFactory();
      Assert.That(sut.CreateDealResult(8, new Contract(3, Suit.Spades, Risk.Doubled)).IsDefeated, Is.True);
      Assert.That(sut.CreateDealResult(10, new Contract(6, Suit.Hearts, Risk.Undoubled)).IsDefeated, Is.True);
      Assert.That(sut.CreateDealResult(8, new Contract(2, Suit.Spades, Risk.Undoubled)).IsDefeated, Is.False);
      Assert.That(sut.CreateDealResult(8, new Contract(1, Suit.Spades, Risk.Doubled)).IsDefeated, Is.False);
    }
  }
}