using System.Linq;
using LegoBridgeDealer.PbnModel.ReadModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class ResultFilterFixture
  {
    [Test]
    public void ShouldIncludeDealsWithCorrectResultWhenFilteringOnAbsoluteResult()
    {
      var sut = new ResultFilter();
      var filterData = Substitute.For<IResultFilterData>();
      sut.SetData(filterData);

      var playerDeal = Substitute.For<IPlayerDeal>();
      var contract = Substitute.For<IContract>();
      var dealResult = Substitute.For<IDealResult>();
      playerDeal.Contract.Returns(contract);
      playerDeal.Result.Returns(dealResult);
      contract.IsPass.Returns(false);

      foreach (var result in Enumerable.Range(8, 4))
      {
        foreach (var min in Enumerable.Range(8, 4))
        {
          foreach (var max in Enumerable.Range(min, 12 - min))
          {
            filterData.PlayerAbsoluteMin.Returns((int?)null);
            filterData.PlayerAbsoluteMax.Returns((int?)null);
            dealResult.Tricks.Returns(result);
            Assert.That(sut.Excluded(playerDeal), Is.False);

            filterData.PlayerAbsoluteMin.Returns(min);
            Assert.That(sut.Excluded(playerDeal), Is.EqualTo(result < min));

            filterData.PlayerAbsoluteMax.Returns(max);
            Assert.That(sut.Excluded(playerDeal), Is.EqualTo(result < min || result > max));

            filterData.PlayerAbsoluteMin.Returns((int?)null);
            Assert.That(sut.Excluded(playerDeal), Is.EqualTo(result > max));
          }
        }
      }
    }
  }
}