﻿using LegoBridgeDealer.PbnModel.ReadModel;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class PlayerFixture
  {
    [Test]
    public void ShouldBeReadableWhenConvertingToString()
    {
      var sut = new PlayerFactory();
      var player = new LegoBridgeDealer.PbnModel.WriteModel.Player
                     {
                       Id = 5, Name = "some name"
                     };
      Assert.That(sut.CreatePlayer(player).ToString(), Is.EqualTo(player.Name));
    }

    [Test]
    public void ShouldUseWriteModelNameWhenGettingName()
    {
      var sut = new PlayerFactory();
      var player = new LegoBridgeDealer.PbnModel.WriteModel.Player
                     {
                       Id = 5,
                       Name = "some name"
                     };
      Assert.That(sut.CreatePlayer(player).Name, Is.EqualTo(player.Name));
    }
  }
}