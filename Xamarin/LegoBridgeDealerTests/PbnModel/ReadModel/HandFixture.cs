﻿using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class HandFixture
  {
    [TestCase("KQT2.AT.J6542.85", "10HP: ♠KQT2 ♥AT ♦J6542 ♣85")]
    [TestCase("-", "-")]
    [TestCase(".63.AKQ987.A9732", "13HP: ♠ ♥63 ♦AKQ987 ♣A9732")]
    [TestCase("A8654.KQT5..QJT6", "12HP: ♠A8654 ♥KQT5 ♦ ♣QJT6")]
    [TestCase("A8654.KQT5.QJT6.", "12HP: ♠A8654 ♥KQT5 ♦QJT6 ♣")]
    [TestCase("2QKT.TA.54J62.58", "10HP: ♠KQT2 ♥AT ♦J6542 ♣85")]
    public void ShouldBeReadableWhenConvertingToString(string pbnString, string readableString)
    {
      var sut = new HandFactory();
      Assert.That(sut.CreateHand(pbnString).ToString(), Is.EqualTo(readableString));
    }

    [TestCase("KQT2.AT.J6542.85", 10)]
    [TestCase("-", null)]
    [TestCase(".63.AKQ987.A9732", 13)]
    [TestCase("A8654.KQT5..QJT6", 12)]
    [TestCase("A8654.KQT5.QJT6.", 12)]
    [TestCase("2QKT.TA.54J62.58", 10)]
    public void ShouldComputeHighCardPointWhenConvertingToString(string pbnString, int? expected)
    {
      var sut = new HandFactory();
      Assert.That(sut.CreateHand(pbnString).HighCardPoints, Is.EqualTo(expected));
    }

    [TestCase("35", Suit.Hearts, 5, null, 0, 0, 0)]
    [TestCase("KQT2.AT.J6542.85", Suit.Hearts, 5, 0, 0, 0, 0)]
    [TestCase("KQT2.AT.J6542.85", Suit.Hearts, 6, 0, 0, 0, 1)]
    [TestCase("KQT2.AT.J6542.85", Suit.Hearts, 7, 0, 1, 0, 1)]
    [TestCase("KQT2.AT.J6542.85", Suit.Hearts, 8, 0, 2, 0, 1)]
    [TestCase("KQT2.AT.J6542.J5", Suit.Hearts, 6, 0, 0, 0, 0)]
    [TestCase("KQT2.AT.J6542.Q5", Suit.Hearts, 6, 0, 0, 0, 0)]
    [TestCase("KQT2.AT.J6542.K5", Suit.Hearts, 6, 0, 0, 0, 1)]
    [TestCase("KQT2.AT.J6542.A5", Suit.Hearts, 6, 0, 0, 0, 1)]
    [TestCase("KQT2.AT8.J6542.5", Suit.Hearts, 6, 0, 1, 0, 2)]
    [TestCase("KQT2.AT8.J6542.J", Suit.Hearts, 6, 0, 1, 0, 1)]
    [TestCase("KQT2.AT8.J6542.Q", Suit.Hearts, 6, 0, 1, 0, 0)]
    [TestCase("KQT2.AT8.J6542.K", Suit.Hearts, 6, 0, 1, 0, 0)]
    [TestCase("KQT2.AT8.J6542.A", Suit.Hearts, 6, 0, 1, 0, 2)]
    [TestCase("KQT52.AT8.J6542.", Suit.Hearts, 5, 0, 0, 0, 3)]
    [TestCase("KQT52.AT.JT6542.", Suit.Spades, 5, 2, 1, 0, 3)]
    [TestCase("KQT52.AT.JT6542.", Suit.Spades, 2, 0, 0, 0, 0)]
    [TestCase("KQT752.AT.JT642.", Suit.Spades, 0, 0, 0, 0, 0)]
    [TestCase("KQT752.AT.JT642.", Suit.NoTrump, 0, 0, 0, 0, 0)]
    [TestCase("KQT2.AT.J6542.KQ", Suit.Diamonds, 4, 0, 1, 1, 0)]
    [TestCase("KQT.AT.J67542.AK", Suit.Diamonds, 5, 0, 1, 3, 1)]
    [TestCase("K2.AT64.J.KQ5432", Suit.Clubs, 4, 1, 0, 1, 2)]
    public void ShouldComputeExtraPoints(string pbnString, Suit trump, int partnerTrumpLength, int? spades, int hearts, int diamonds, int clubs)
    {
      var expected = spades != null ? ((int, int, int, int)?)(spades.Value, hearts, diamonds, clubs) : null;
      var sut = new HandFactory();
      Assert.That(sut.CreateHand(pbnString).ExtraPoints(trump, partnerTrumpLength), Is.EqualTo(expected));
    }

    [TestCase("KQT2.AT8.J6542.5", Suit.Spades, 4)]
    [TestCase("KQT2.AT8.J6542.5", Suit.Hearts, 3)]
    [TestCase("KQT2.AT8.J6542.5", Suit.Diamonds, 5)]
    [TestCase("KQT2.AT8.J6542.5", Suit.Clubs, 1)]
    [TestCase("KQT2.AT8.J6542.5", Suit.NoTrump, 0)]
    public void ShouldReturnLengthOfSuit(string pbnString, Suit trump, int expected)
    {
      var sut = new HandFactory();
      Assert.That(sut.CreateHand(pbnString).SuitLength(trump), Is.EqualTo(expected));
    }
  }
}