﻿using System;
using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class ContractFixture
  {
    [Test]
    public void ShouldBeReadableWhenConvertingToString([Values(0, 1, 2, 3, 4, 5, 6, 7)]int tricks)
    {
      var sut = new ContractFactory();
      foreach (var suit in new[] { (Suit.Clubs, "♣"), (Suit.Diamonds, "♦"), (Suit.Hearts, "♥"), (Suit.Spades, "♠"), (Suit.NoTrump, "NT") } )
      {
        foreach (var risk in new[] { (Risk.Undoubled, ""), (Risk.Doubled, "X"), (Risk.Redoubled, "XX") })
        {
          foreach (var declarer in new[] { (PlayerDirection.North, "N"), (PlayerDirection.East, "E"), (PlayerDirection.West, "W"), (PlayerDirection.South, "S") } )
          {
            var expectedString = tricks == 0 ? "Pass" : tricks + suit.Item2 + risk.Item2 + " " + declarer.Item2;
            Assert.That(
              sut.CreateContract(new Contract(tricks, suit.Item1, risk.Item1), declarer.Item1, Vulnerability.None)
                .ToString(),
              Is.EqualTo(expectedString));
          }
        }
      }
    }

    [Test]
    public void ShouldSortAccordingToBidOrder()
    {
      var sut = new ContractFactory();
      var expectedOrder =
        new List<IContract> { sut.CreateContract(new Contract(), PlayerDirection.South, Vulnerability.All) };
      expectedOrder.AddRange(from tricks in Enumerable.Range(1, 7) from suit in new[] { Suit.Clubs, Suit.Diamonds, Suit.Hearts, Suit.Spades, Suit.NoTrump } from risk in new[] { Risk.Undoubled, Risk.Doubled, Risk.Redoubled } select sut.CreateContract(new Contract(tricks, suit, risk), PlayerDirection.East, Vulnerability.None));

      var random = new Random();
      var scrambledList = expectedOrder.OrderBy(_ => random.Next(0, 1000));
      Assert.That(scrambledList.OrderBy(c => c.SortIndex), Is.EqualTo(expectedOrder));
    }

    [Test]
    public void ShouldReturnIsPassValueFromWriteModel()
    {
      var sut = new ContractFactory { PositionFactory = Substitute.For<IPositionFactory>() };
      var declarer = Substitute.For<IPosition>();
      declarer.IsNorthOrSouth.Returns(true);
      sut.PositionFactory.CreatePosition(PlayerDirection.North).Returns(declarer);
      Assert.That(sut.CreateContract(new Contract(), PlayerDirection.North, Vulnerability.None).IsPass, Is.EqualTo(true));
      Assert.That(sut.CreateContract(new Contract(1, Suit.Clubs, Risk.Doubled), PlayerDirection.North, Vulnerability.EastWest).IsPass, Is.EqualTo(false));
    }

    [TestCase(PlayerDirection.North, Vulnerability.None, false)]
    [TestCase(PlayerDirection.East, Vulnerability.None, false)]
    [TestCase(PlayerDirection.South, Vulnerability.None, false)]
    [TestCase(PlayerDirection.West, Vulnerability.None, false)]
    [TestCase(PlayerDirection.North, Vulnerability.EastWest, false)]
    [TestCase(PlayerDirection.East,  Vulnerability.EastWest, true)]
    [TestCase(PlayerDirection.South, Vulnerability.EastWest, false)]
    [TestCase(PlayerDirection.West,  Vulnerability.EastWest, true)]
    [TestCase(PlayerDirection.North, Vulnerability.NorthSouth, true)]
    [TestCase(PlayerDirection.East,  Vulnerability.NorthSouth, false)]
    [TestCase(PlayerDirection.South, Vulnerability.NorthSouth, true)]
    [TestCase(PlayerDirection.West,  Vulnerability.NorthSouth, false)]
    [TestCase(PlayerDirection.North, Vulnerability.All, true)]
    [TestCase(PlayerDirection.East,  Vulnerability.All, true)]
    [TestCase(PlayerDirection.South, Vulnerability.All, true)]
    [TestCase(PlayerDirection.West,  Vulnerability.All, true)]
    public void ShouldReturnIsVulnerableWhenDeclarerIsVulnerable(PlayerDirection declarer, Vulnerability vulnerable, bool expected)
    {
      var sut = new ContractFactory().CreateContract(new Contract(), declarer, vulnerable).IsVulnerable;
      Assert.That(sut, Is.EqualTo(expected));
    }

    [Test]
    public void ShouldUseSuitFactory()
    {
      var sut = new ContractFactory { SuitFactory = Substitute.For<ISuitFactory>(), PositionFactory = Substitute.For<IPositionFactory>() };
      var suit = Substitute.For<ISuit>();
      sut.SuitFactory.CreateSuit(Suit.Diamonds).Returns(suit);
      var declarer = Substitute.For<IPosition>();
      declarer.IsNorthOrSouth.Returns(true);
      sut.PositionFactory.CreatePosition(PlayerDirection.North).Returns(declarer);
      var contract = sut.CreateContract(new Contract(3, Suit.Diamonds, Risk.Doubled), PlayerDirection.North, Vulnerability.All);
      Assert.That(contract.Suit, Is.SameAs(suit));
    }

    [Test]
    public void ShouldUseRiskFactory()
    {
      var sut = new ContractFactory { RiskFactory = Substitute.For<IRiskFactory>(), PositionFactory = Substitute.For<IPositionFactory>() };
      var risk = Substitute.For<IRisk>();
      sut.RiskFactory.CreateRisk(Risk.Doubled).Returns(risk);
      var declarer = Substitute.For<IPosition>();
      declarer.IsNorthOrSouth.Returns(true);
      sut.PositionFactory.CreatePosition(PlayerDirection.North).Returns(declarer);
      var contract = sut.CreateContract(new Contract(3, Suit.Diamonds, Risk.Doubled), PlayerDirection.North, Vulnerability.All);
      Assert.That(contract.Risk, Is.SameAs(risk));
    }

    [Test]
    public void ShouldCallPositionFactoryWhenConstructing()
    {
      var sut = new ContractFactory { PositionFactory = Substitute.For<IPositionFactory>() };
      var declarer = Substitute.For<IPosition>();
      declarer.IsNorthOrSouth.Returns(true);
      sut.PositionFactory.CreatePosition(PlayerDirection.North).Returns(declarer);
      var opponent = Substitute.For<IPosition>();
      sut.PositionFactory.CreatePosition(PlayerDirection.East).Returns(opponent);

      var result = sut.CreateContract(new Contract(), PlayerDirection.North, Vulnerability.None);

      Assert.That(result.Declarer, Is.SameAs(declarer));
      Assert.That(result.Opponent, Is.SameAs(opponent));
    }

    [TestCase(PlayerDirection.North, PlayerDirection.East)]
    [TestCase(PlayerDirection.South, PlayerDirection.East)]
    [TestCase(PlayerDirection.East, PlayerDirection.North)]
    [TestCase(PlayerDirection.West, PlayerDirection.North)]
    public void ShouldCreatCorrectOpponent(PlayerDirection declarer, PlayerDirection opponent)
    {
      var sut = new ContractFactory { PositionFactory = Substitute.For<IPositionFactory>() };
      var declarer2 = Substitute.For<IPosition>();
      declarer2.IsNorthOrSouth.Returns(declarer == PlayerDirection.North || declarer == PlayerDirection.South);
      var opponent2 = Substitute.For<IPosition>();
      sut.PositionFactory.CreatePosition(declarer).Returns(declarer2);
      sut.PositionFactory.CreatePosition(opponent).Returns(opponent2);

      var result = sut.CreateContract(new Contract(), declarer, Vulnerability.None);

      Assert.That(result.Declarer, Is.SameAs(declarer2));
      Assert.That(result.Opponent, Is.SameAs(opponent2));
    }
  }
}