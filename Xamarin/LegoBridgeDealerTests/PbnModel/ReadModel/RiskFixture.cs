﻿using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class RiskFixture
  {
    [Test]
    public void ShouldFailAssertWhenCostIsCalledWithNonNegativeResult()
    {
      var sut = new RiskFactory().CreateRisk(Risk.Undoubled);
      Assert.Throws<AssertionException>(() => sut.Cost(0, true));
      Assert.Throws<AssertionException>(() => sut.Cost(1, false));
    }

    [Test]
    public void ShouldTellIfContractIsDoubledOrRedoubled([Values(Risk.Doubled, Risk.Redoubled, Risk.Undoubled)]Risk risk)
    {
      var sut = new RiskFactory().CreateRisk(risk);
      Assert.That(sut.IsDoubled, Is.EqualTo(risk == Risk.Doubled));
      Assert.That(sut.IsRedoubled, Is.EqualTo(risk == Risk.Redoubled));
    }
  }
}