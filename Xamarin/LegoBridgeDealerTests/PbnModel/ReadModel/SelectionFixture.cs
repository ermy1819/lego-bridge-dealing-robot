﻿using System.Collections.Generic;
using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class SelectionFixture
  {
    [Test]
    public void ShouldGroupGamesByCardsToCreateDeals()
    {
      var root = new Root();
      var factory = new SelectionFactory { DealFactory = Substitute.For<IDealFactory>() };
      var filter = Substitute.For<IFilter>();
      var sut = factory.CreateSelection(root, filter);

      root.Games.Add(1, new Game { Deal = "1" });
      root.Games.Add(2, new Game { Deal = "2" });
      root.Games.Add(3, new Game { Deal = "3" });
      root.Games.Add(4, new Game { Deal = "2" });
      root.Games.Add(5, new Game { Deal = "2" });
      root.Games.Add(6, new Game { Deal = "1" });
      root.Games.Add(7, new Game { Deal = "1" });
      root.Players.Add(0, new Player());
      root.Events.Add(0, new Event());

      var deal1 = Substitute.For<IDeal>();
      var deal2 = Substitute.For<IDeal>();
      var deal3 = Substitute.For<IDeal>();

      factory.DealFactory.CreateDeal(
        Arg.Is<List<Game>>(l => l.TrueForAll(g => g.Deal == "1")),
        Arg.Any<List<Player>>(),
        Arg.Any<Event>(),
        Arg.Any<IFilter>()).Returns(deal1);
      factory.DealFactory.CreateDeal(
        Arg.Is<List<Game>>(l => l.TrueForAll(g => g.Deal == "2")),
        Arg.Any<List<Player>>(),
        Arg.Any<Event>(),
        Arg.Any<IFilter>()).Returns(deal2);
      factory.DealFactory.CreateDeal(
        Arg.Is<List<Game>>(l => l.TrueForAll(g => g.Deal == "3")),
        Arg.Any<List<Player>>(),
        Arg.Any<Event>(),
        Arg.Any<IFilter>()).Returns(deal3);

      sut.Repopulate();

      Assert.That(sut.Deals, Is.EquivalentTo(new[] { deal1, deal2, deal3 }));
    }

    [Test]
    public void GroupGamesByEventsToCreateDeals()
    {
      var root = new Root();
      var factory = new SelectionFactory { DealFactory = Substitute.For<IDealFactory>() };
      var filter = Substitute.For<IFilter>();
      var sut = factory.CreateSelection(root, filter);

      root.Games.Add(1, new Game { EventId = 1 });
      root.Games.Add(2, new Game { EventId = 2 });
      root.Games.Add(3, new Game { EventId = 3 });
      root.Games.Add(4, new Game { EventId = 2 });
      root.Games.Add(5, new Game { EventId = 2 });
      root.Games.Add(6, new Game { EventId = 1 });
      root.Games.Add(7, new Game { EventId = 1 });
      root.Players.Add(1, new Player());
      root.Events.Add(1, new Event { Id = 1 });
      root.Events.Add(2, new Event { Id = 2 });
      root.Events.Add(3, new Event { Id = 3 });

      var deal1 = Substitute.For<IDeal>();
      var deal2 = Substitute.For<IDeal>();
      var deal3 = Substitute.For<IDeal>();

      factory.DealFactory.CreateDeal(
        Arg.Any<List<Game>>(),
        Arg.Any<List<Player>>(),
        Arg.Is(root.Events[1]),
        Arg.Any<IFilter>()).Returns(deal1);
      factory.DealFactory.CreateDeal(
        Arg.Any<List<Game>>(),
        Arg.Any<List<Player>>(),
        Arg.Is(root.Events[2]),
        Arg.Any<IFilter>()).Returns(deal2);
      factory.DealFactory.CreateDeal(
        Arg.Any<List<Game>>(),
        Arg.Any<List<Player>>(),
        Arg.Is(root.Events[3]),
        Arg.Any<IFilter>()).Returns(deal3);

      sut.Repopulate();

      Assert.That(sut.Deals, Is.EquivalentTo(new[] { deal1, deal2, deal3 }));
    }

    [Test]
    public void ShouldRaiseModifiedEventWhenModelIsRepopulated()
    {
      var root = new Root();
      var factory = new SelectionFactory { DealFactory = Substitute.For<IDealFactory>() };
      var filter = Substitute.For<IFilter>();
      var sut = factory.CreateSelection(root, filter);
      var eventRaised = false;
      sut.Modified += (s, e) => eventRaised = true;
      sut.Repopulate();
      Assert.That(eventRaised);
    }

    [Test]
    public void ShouldPassFilterToDealFactoryWhenRepopulating()
    {
      var root = new Root();
      root.Games.Add(1, new Game { EventId = 1 });
      root.Events.Add(1, new Event { Id = 1 });
      var factory = new SelectionFactory { DealFactory = Substitute.For<IDealFactory>() };
      var filter = Substitute.For<IFilter>();
      var sut = factory.CreateSelection(root, filter);
      sut.Repopulate();
      factory.DealFactory.Received().CreateDeal(
        Arg.Any<List<Game>>(),
        Arg.Any<List<Player>>(),
        Arg.Is(root.Events[1]),
        Arg.Is(filter));
    }

    [Test]
    public void ShouldOnlyIncludeDealsAcceptedByFilterWhenRepopulating()
    {
      var root = new Root();
      var factory = new SelectionFactory { DealFactory = Substitute.For<IDealFactory>() };
      var filter = Substitute.For<IFilter>();
      var sut = factory.CreateSelection(root, filter);

      root.Games.Add(1, new Game { EventId = 1 });
      root.Games.Add(2, new Game { EventId = 2 });
      root.Events.Add(1, new Event { Id = 1 });
      root.Events.Add(2, new Event { Id = 2 });

      var deal1 = Substitute.For<IDeal>();
      var deal2 = Substitute.For<IDeal>();
      filter.Excluded(deal1).Returns(true);
      filter.Excluded(deal2).Returns(false);

      factory.DealFactory.CreateDeal(
        Arg.Any<List<Game>>(),
        Arg.Any<List<Player>>(),
        Arg.Is(root.Events[1]),
        Arg.Any<IFilter>()).Returns(deal1);
      factory.DealFactory.CreateDeal(
        Arg.Any<List<Game>>(),
        Arg.Any<List<Player>>(),
        Arg.Is(root.Events[2]),
        Arg.Any<IFilter>()).Returns(deal2);

      sut.Repopulate();

      Assert.That(sut.Deals, Is.EquivalentTo(new[] { deal2 } ));
    }
  }
}