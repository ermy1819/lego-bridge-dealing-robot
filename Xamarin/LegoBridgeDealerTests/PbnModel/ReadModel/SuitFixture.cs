﻿using System;
using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class SuitFixture
  {
    [TestCase(Suit.Clubs, "♣")]
    [TestCase(Suit.Diamonds, "♦")]
    [TestCase(Suit.Hearts, "♥")]
    [TestCase(Suit.Spades, "♠")]
    [TestCase(Suit.NoTrump, "NT")]
    public void ShouldBeReadableWhenConvertingToString(Suit suit, string symbol)
    {
      var sut = new SuitFactory();
      Assert.That(sut.CreateSuit(suit).ToString(), Is.EqualTo(symbol));
    }

    [Test]
    public void ShouldSortAccordingToBidOrder()
    {
      var sut = new SuitFactory();
      var expectedOrder = new List<ISuit>
                            {
                              sut.CreateSuit(Suit.Clubs),
                              sut.CreateSuit(Suit.Diamonds),
                              sut.CreateSuit(Suit.Hearts),
                              sut.CreateSuit(Suit.Spades),
                              sut.CreateSuit(Suit.NoTrump),
                            };
      var random = new Random();
      var scrambledList = expectedOrder.OrderBy(_ => random.Next(0, 1000));
      Assert.That(scrambledList.OrderBy(c => c.SortIndex), Is.EqualTo(expectedOrder));
    }

    [TestCase(Suit.Clubs)]
    [TestCase(Suit.Diamonds)]
    [TestCase(Suit.Hearts)]
    [TestCase(Suit.Spades)]
    [TestCase(Suit.NoTrump)]
    public void ShouldReturnCorrectSuit(Suit suit)
    {
      var sut = new SuitFactory();
      Assert.That(sut.CreateSuit(suit).Suit, Is.EqualTo(suit));
    }

    [Test]
    public void ShouldClassifyClubsAndDiamondsAsMinors()
    {
      var sut = new SuitFactory();
      Assert.That(sut.CreateSuit(Suit.Clubs).IsMinor, Is.True);
      Assert.That(sut.CreateSuit(Suit.Diamonds).IsMinor, Is.True);
      Assert.That(sut.CreateSuit(Suit.Hearts).IsMinor, Is.False);
      Assert.That(sut.CreateSuit(Suit.Spades).IsMinor, Is.False);
      Assert.That(sut.CreateSuit(Suit.NoTrump).IsMinor, Is.False);
    }

    [Test]
    public void ShouldClassifyHeartsAndSpadesAsMajors()
    {
      var sut = new SuitFactory();
      Assert.That(sut.CreateSuit(Suit.Clubs).IsMajor, Is.False);
      Assert.That(sut.CreateSuit(Suit.Diamonds).IsMajor, Is.False);
      Assert.That(sut.CreateSuit(Suit.Hearts).IsMajor, Is.True);
      Assert.That(sut.CreateSuit(Suit.Spades).IsMajor, Is.True);
      Assert.That(sut.CreateSuit(Suit.NoTrump).IsMajor, Is.False);
    }

    [Test]
    public void ShouldClassifyNoTrumoAsNoTrump()
    {
      var sut = new SuitFactory();
      Assert.That(sut.CreateSuit(Suit.Clubs).IsNoTrump, Is.False);
      Assert.That(sut.CreateSuit(Suit.Diamonds).IsNoTrump, Is.False);
      Assert.That(sut.CreateSuit(Suit.Hearts).IsNoTrump, Is.False);
      Assert.That(sut.CreateSuit(Suit.Spades).IsNoTrump, Is.False);
      Assert.That(sut.CreateSuit(Suit.NoTrump).IsNoTrump, Is.True);
    }

    [TestCase(Suit.Clubs, 5)]
    [TestCase(Suit.Diamonds, 5)]
    [TestCase(Suit.Hearts, 4)]
    [TestCase(Suit.Spades, 4)]
    [TestCase(Suit.NoTrump, 3)]
    public void ShouldNoTrickForGame(Suit suit, int tricks)
    {
      var sut = new SuitFactory();
      Assert.That(sut.CreateSuit(suit).TricksForGame, Is.EqualTo(tricks));
    }

    [TestCase(Suit.Clubs, 20)]
    [TestCase(Suit.Diamonds, 20)]
    [TestCase(Suit.Hearts, 30)]
    [TestCase(Suit.Spades, 30)]
    [TestCase(Suit.NoTrump, 30)]
    public void ShouldNoScorePerTrick(Suit suit, int points)
    {
      var sut = new SuitFactory();
      Assert.That(sut.CreateSuit(suit).ScorePerTrick, Is.EqualTo(points));
    }

    [TestCase(Suit.Clubs, 20)]
    [TestCase(Suit.Diamonds, 20)]
    [TestCase(Suit.Hearts, 30)]
    [TestCase(Suit.Spades, 30)]
    [TestCase(Suit.NoTrump, 40)]
    public void ShouldNoScoreForFirstTrick(Suit suit, int points)
    {
      var sut = new SuitFactory();
      Assert.That(sut.CreateSuit(suit).ScoreForFirstTrick, Is.EqualTo(points));
    }
  }
}