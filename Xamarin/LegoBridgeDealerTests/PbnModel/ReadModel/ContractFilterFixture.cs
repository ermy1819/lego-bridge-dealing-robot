﻿using System;
using System.Linq;
using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class ContractFilterFixture
  {
    [Test]
    public void ShouldIncludeDealsWithAllContractsWhenNoContractFilterSet()
    {
      var sut = new ContractFilter();
      var filterData = Substitute.For<IContractFilterData>();
      sut.SetData(filterData);

      var playerDeal = Substitute.For<IPlayerDeal>();
      var contract = Substitute.For<IContract>();
      var risk = Substitute.For<IRisk>();
      var suit = Substitute.For<ISuit>();
      playerDeal.Contract.Returns(contract);
      contract.Risk.Returns(risk);
      contract.Suit.Returns(suit);
      contract.IsPass.Returns(true);
      Assert.IsFalse(sut.Excluded(playerDeal));
      contract.IsPass.Returns(false);

      foreach (int tricks in Enumerable.Range(1, 7))
      {
        foreach (Risk r in Enum.GetValues(typeof(Risk)))
        {
          foreach (Suit s in Enum.GetValues(typeof(Suit)))
          {
            risk.IsDoubled.Returns(r == Risk.Doubled);
            risk.IsRedoubled.Returns(r == Risk.Redoubled);
            suit.Suit.Returns(s);
            contract.Tricks.Returns(tricks);
            Assert.IsFalse(sut.Excluded(playerDeal));
          }
        }
      }
    }

    [Test]
    public void ShouldIncludePlayerDealIffItsNumberOfTricksIsIncluded()
    {
      var sut = new ContractFilter();
      var filterData = Substitute.For<IContractFilterData>();
      sut.SetData(filterData);

      var playerDeal = Substitute.For<IPlayerDeal>();
      var contract = Substitute.For<IContract>();
      var risk = Substitute.For<IRisk>();
      var suit = Substitute.For<ISuit>();
      playerDeal.Contract.Returns(contract);
      contract.Risk.Returns(risk);
      contract.Suit.Returns(suit);

      filterData.Tricks0.Returns(true);

      contract.Tricks.Returns(1);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Tricks1.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      contract.Tricks.Returns(2);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Tricks2.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      contract.Tricks.Returns(3);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Tricks3.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      contract.Tricks.Returns(4);
      contract.IsGame.Returns(true);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Tricks4.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      contract.Tricks.Returns(5);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Tricks5.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      contract.Tricks.Returns(6);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Tricks6.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      contract.Tricks.Returns(7);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Tricks7.Returns(true);
      Assert.False(sut.Excluded(playerDeal));
    }

    [Test]
    public void ShouldIncludePlayerDealIffItsSuitIsIncluded()
    {
      var sut = new ContractFilter();
      var filterData = Substitute.For<IContractFilterData>();
      sut.SetData(filterData);

      var playerDeal = Substitute.For<IPlayerDeal>();
      var contract = Substitute.For<IContract>();
      var risk = Substitute.For<IRisk>();
      var suit = Substitute.For<ISuit>();
      playerDeal.Contract.Returns(contract);
      contract.Risk.Returns(risk);
      contract.Suit.Returns(suit);
      contract.Tricks.Returns(1);

      filterData.Pass.Returns(true);

      suit.Suit.Returns(Suit.Diamonds);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Diamonds.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      suit.Suit.Returns(Suit.Clubs);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Clubs.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      suit.Suit.Returns(Suit.Hearts);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Hearts.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      suit.Suit.Returns(Suit.Spades);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Spades.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      suit.Suit.Returns(Suit.NoTrump);
      Assert.True(sut.Excluded(playerDeal));
      filterData.NoTrump.Returns(true);
      Assert.False(sut.Excluded(playerDeal));
    }

    [Test]
    public void ShouldIncludePlayerDealIffItsRiskIsIncluded()
    {
      var sut = new ContractFilter();
      var filterData = Substitute.For<IContractFilterData>();
      sut.SetData(filterData);

      var playerDeal = Substitute.For<IPlayerDeal>();
      var contract = Substitute.For<IContract>();
      var risk = Substitute.For<IRisk>();
      var suit = Substitute.For<ISuit>();
      playerDeal.Contract.Returns(contract);
      contract.Tricks.Returns(1);
      contract.Risk.Returns(risk);
      contract.Suit.Returns(suit);

      filterData.Redoubled.Returns(true);

      Assert.True(sut.Excluded(playerDeal));
      filterData.Undoubled.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      risk.IsDoubled.Returns(true);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Doubled.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      risk.IsDoubled.Returns(false);
      risk.IsRedoubled.Returns(true);
      Assert.False(sut.Excluded(playerDeal));
      filterData.Redoubled.Returns(false);
      Assert.True(sut.Excluded(playerDeal));
    }

    [Test]
    public void ShouldIncludePassedContractsIff0TricksOrPassSuitIsChecked()
    {
      var sut = new ContractFilter();
      var filterData = Substitute.For<IContractFilterData>();
      sut.SetData(filterData);

      var playerDeal = Substitute.For<IPlayerDeal>();
      var contract = Substitute.For<IContract>();
      var risk = Substitute.For<IRisk>();
      var suit = Substitute.For<ISuit>();
      playerDeal.Contract.Returns(contract);
      contract.Risk.Returns(risk);
      contract.Suit.Returns(suit);
      contract.IsPass.Returns(true);

      filterData.Tricks2.Returns(true);

      Assert.True(sut.Excluded(playerDeal));
      filterData.Tricks0.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      filterData.Tricks2.Returns(false);
      filterData.Tricks0.Returns(false);

      filterData.Hearts.Returns(true);
      Assert.True(sut.Excluded(playerDeal));
      filterData.Pass.Returns(true);
      Assert.False(sut.Excluded(playerDeal));
    }

    [Test]
    public void ShouldIncludePlayersPlayingGameIffGameContractIsCheckedAndContractIsNotSlam()
    {
      var sut = new ContractFilter();
      var filterData = Substitute.For<IContractFilterData>();
      sut.SetData(filterData);

      var playerDeal = Substitute.For<IPlayerDeal>();
      var contract = Substitute.For<IContract>();
      var risk = Substitute.For<IRisk>();
      var suit = Substitute.For<ISuit>();
      playerDeal.Contract.Returns(contract);
      contract.Risk.Returns(risk);
      contract.Suit.Returns(suit);
      contract.IsGame.Returns(true);
      contract.Tricks.Returns(5);

      filterData.Tricks6.Returns(true);

      Assert.True(sut.Excluded(playerDeal));
      filterData.Game.Returns(true);
      Assert.False(sut.Excluded(playerDeal));

      contract.Tricks.Returns(6);
      Assert.False(sut.Excluded(playerDeal));
      filterData.Tricks6.Returns(false);
      Assert.True(sut.Excluded(playerDeal));
      contract.Tricks.Returns(7);
      Assert.True(sut.Excluded(playerDeal));
    }

    [Test]
    public void ShouldNotIncludePassedContractsWhenFilteringDoubledContractsOnly()
    {
      var sut = new ContractFilter();
      var filterData = Substitute.For<IContractFilterData>();
      sut.SetData(filterData);

      var playerDeal = Substitute.For<IPlayerDeal>();
      var contract = Substitute.For<IContract>();
      var risk = Substitute.For<IRisk>();
      var suit = Substitute.For<ISuit>();
      playerDeal.Contract.Returns(contract);
      contract.Risk.Returns(risk);
      contract.Suit.Returns(suit);
      contract.IsPass.Returns(true);
      filterData.Doubled.Returns(true);

      Assert.True(sut.Excluded(playerDeal));
    }
  }
}