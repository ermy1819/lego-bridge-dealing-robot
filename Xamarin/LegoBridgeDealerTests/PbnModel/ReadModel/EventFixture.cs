using LegoBridgeDealer.PbnModel.ReadModel;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class EventFixture
  {
    [Test]
    public void ShouldBeReadableWhenConvertingToString()
    {
      var sut = new EventFactory();
      var @event = new LegoBridgeDealer.PbnModel.WriteModel.Event
                     {
                       Id = 5,
                       Name = "some name",
                     };
      Assert.That(sut.CreateEvent(@event).ToString(), Is.EqualTo(@event.Name));
    }

    [Test]
    public void ShouldUseWriteModelNameWhenGettingName()
    {
      var sut = new EventFactory();
      var @event = new LegoBridgeDealer.PbnModel.WriteModel.Event
                     {
                       Id = 5,
                       Name = "some name",
                     };
      Assert.That(sut.CreateEvent(@event).Name, Is.EqualTo(@event.Name));
    }
  }
}