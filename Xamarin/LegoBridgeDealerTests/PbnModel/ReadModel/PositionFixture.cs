﻿using System;
using System.Linq;
using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class PositionFixture
  {
    [TestCase(PlayerDirection.North, "North")]
    [TestCase(PlayerDirection.West, "West")]
    [TestCase(PlayerDirection.East, "East")]
    [TestCase(PlayerDirection.South, "South")]
    [TestCase(PlayerDirection.Declarer, "Declarer")]
    [TestCase(PlayerDirection.Dummy, "Dummy")]
    [TestCase(PlayerDirection.LeftHandDefender, "LHD")]
    [TestCase(PlayerDirection.RightHandDefender, "RHD")]
    [TestCase(PlayerDirection.Me, "You")]
    [TestCase(PlayerDirection.Partner, "Partner")]
    [TestCase(PlayerDirection.LeftHandOpponent, "LHO")]
    [TestCase(PlayerDirection.RightHandOpponent, "RHO")]
    public void ShouldBeReadableWhenConvertingToString(PlayerDirection position, string name)
    {
      var sut = new PositionFactory();
      Assert.That(sut.CreatePosition(position).ToString(), Is.EqualTo(name));
    }

    [Test]
    public void ShouldBeDefendingWhenPlayerIsRightOrLEftDefender()
    {
      var sut = new PositionFactory();
      Assert.That(sut.CreatePosition(PlayerDirection.LeftHandDefender).IsDefending, Is.EqualTo(true));
      Assert.That(sut.CreatePosition(PlayerDirection.RightHandDefender).IsDefending, Is.EqualTo(true));
    }

    [Test]
    public void ShouldNotBeDefendingWhenPlayerIsDeclarerOrDummy()
    {
      var sut = new PositionFactory();
      Assert.That(sut.CreatePosition(PlayerDirection.Declarer).IsDefending, Is.EqualTo(false));
      Assert.That(sut.CreatePosition(PlayerDirection.Dummy).IsDefending, Is.EqualTo(false));
    }

    [Test]
    public void ShouldThrowExceptionWhenCheckingIsDefendingForFixedOrRelativePositions()
    {
      var sut = new PositionFactory();
      Assert.Throws<InvalidOperationException>(() => _ = sut.CreatePosition(PlayerDirection.South).IsDefending);
      Assert.Throws<InvalidOperationException>(() => _ = sut.CreatePosition(PlayerDirection.North).IsDefending);
      Assert.Throws<InvalidOperationException>(() => _ = sut.CreatePosition(PlayerDirection.East).IsDefending);
      Assert.Throws<InvalidOperationException>(() => _ = sut.CreatePosition(PlayerDirection.West).IsDefending);
      Assert.Throws<InvalidOperationException>(() => _ = sut.CreatePosition(PlayerDirection.Me).IsDefending);
      Assert.Throws<InvalidOperationException>(() => _ = sut.CreatePosition(PlayerDirection.Partner).IsDefending);
      Assert.Throws<InvalidOperationException>(() => _ = sut.CreatePosition(PlayerDirection.LeftHandOpponent).IsDefending);
      Assert.Throws<InvalidOperationException>(() => _ = sut.CreatePosition(PlayerDirection.RightHandOpponent).IsDefending);
    }

    [Test]
    public void ShouldSortClockwiseWhenSortingAbsolutePositions()
    {
      var list = new[] { PlayerDirection.East, PlayerDirection.North, PlayerDirection.South, PlayerDirection.West, };
      var sut = new PositionFactory();
      Assert.That(list.Select(d => sut.CreatePosition(d)).OrderBy(p => p.SortIndex).Select(p => p.ToString()), Is.EqualTo(new[] { "North", "East", "South", "West" }));
    }

    [Test]
    public void ShouldSortDeclarerDummyDefendersWhenSortingRoles()
    {
      var list = new[] { PlayerDirection.Dummy, PlayerDirection.Declarer, PlayerDirection.LeftHandDefender, PlayerDirection.RightHandDefender, };
      var sut = new PositionFactory();
      Assert.That(list.Select(d => sut.CreatePosition(d)).OrderBy(p => p.SortIndex).Select(p => p.ToString()), Is.EqualTo(new[] { "Declarer", "Dummy", "LHD", "RHD" }));
    }

    [Test]
    public void ShouldBeNorthOrSouthWhenPositionIsNorthOrSouth()
    {
      var sut = new PositionFactory();
      Assert.That(sut.CreatePosition(PlayerDirection.North).IsNorthOrSouth, Is.True);
      Assert.That(sut.CreatePosition(PlayerDirection.South).IsNorthOrSouth, Is.True);
    }

    [Test]
    public void ShouldNotBeNorthOrSouthWhenPositionIsEastOrWest()
    {
      var sut = new PositionFactory();
      Assert.That(sut.CreatePosition(PlayerDirection.East).IsNorthOrSouth, Is.False);
      Assert.That(sut.CreatePosition(PlayerDirection.West).IsNorthOrSouth, Is.False);
    }
  }
}