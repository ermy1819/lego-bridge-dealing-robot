using System;
using LegoBridgeDealer.PbnModel.ReadModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class FilterFixture
  {
    [Test]
    public void ShouldExcludeDealsWithNoPlayerDeals()
    {
      var deal = Substitute.For<IDeal>();
      var players = Array.Empty<IPlayerDeal>();
      deal.PlayerDeals.Returns(players);

      var sut = new Filter();

      Assert.IsTrue(sut.Excluded(deal));
    }

    [Test]
    public void ShouldIncludeDealsWithPlayerDeals()
    {
      var deal = Substitute.For<IDeal>();
      var players = new[] { Substitute.For<IPlayerDeal>(), Substitute.For<IPlayerDeal>(), Substitute.For<IPlayerDeal>() };
      deal.PlayerDeals.Returns(players);

      var sut = new Filter();

      Assert.IsFalse(sut.Excluded(deal));
    }

    [Test]
    public void ShouldCallSubFiltersWhenFilteringPlayerDeal()
    {
      var deal = Substitute.For<IPlayerDeal>();

      var contractFilter = Substitute.For<IContractFilter>();
      var resultFilter = Substitute.For<IResultFilter>();
      var roleFilter = Substitute.For<IRoleFilter>();
      var vulnerabilityFilter = Substitute.For<IVulnerabilityFilter>();
      var sut = new Filter(
        contractFilter,
        resultFilter,
        roleFilter,
        vulnerabilityFilter);

      sut.Excluded(deal);

      contractFilter.Received().Excluded(deal);
      resultFilter.Received().Excluded(deal);
    }

    [Test]
    public void ShouldSetDataForAllSubFiltersWhenSettingData()
    {
      var data = Substitute.For<IFilterData>();
      data.Contract.Returns(Substitute.For<IContractFilterData>());
      data.Result.Returns(Substitute.For<IResultFilterData>());
      data.Role.Returns(Substitute.For<IRoleFilterData>());
      data.Vulnerability.Returns(Substitute.For<IVulnerabilityFilterData>());

      var contractFilter = Substitute.For<IContractFilter>();
      var resultFilter = Substitute.For<IResultFilter>();
      var roleFilter = Substitute.For<IRoleFilter>();
      var vulnerabilityFilter = Substitute.For<IVulnerabilityFilter>();
      var sut = new Filter(
        contractFilter,
        resultFilter,
        roleFilter,
        vulnerabilityFilter);

      sut.SetData(data);

      contractFilter.Received().SetData(data.Contract);
      resultFilter.Received().SetData(data.Result);
      roleFilter.Received().SetData(data.Role);
      vulnerabilityFilter.Received().SetData(data.Vulnerability);
    }
  }
}
