﻿using System;
using System.Collections.Generic;
using System.Linq;
using LegoBridgeDealer.PbnModel.ReadModel;
using LegoBridgeDealer.PbnModel.ReadModel.Services;
using LegoBridgeDealer.PbnModel.WriteModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.ReadModel
{
  internal class DealFixture
  {
    [Test]
    public void ShouldCallEventFactoryWhenConstructing()
    {
      var sut = new DealFactory { EventFactory = Substitute.For<IEventFactory>(), CardsFactory = Substitute.For<ICardsFactory>() };
      var game = new Game();
      var player = new Player();
      var @event = new Event();
      var createdEvent = Substitute.For<IEvent>();
      var filter = Substitute.For<IFilter>();
      sut.EventFactory.CreateEvent(@event).Returns(createdEvent);

      var result = sut.CreateDeal(new List<Game> { game }, new List<Player> { player }, @event, filter);

      Assert.That(result.Event, Is.SameAs(createdEvent));
    }

    [Test]
    public void ShouldRequireAllGamesToHaveTheSameCards()
    {
      var sut = new DealFactory();
      var game1 = new Game { Deal = "deal1" };
      var game2 = new Game { Deal = "deal2" };
      var game3 = new Game { Deal = "deal1" };
      var player = new Player();
      var filter = Substitute.For<IFilter>();

      Assert.DoesNotThrow(() => sut.CreateDeal(new List<Game> { game1, game3 }, new List<Player> { player }, new Event(), filter));
      Assert.Throws<InvalidOperationException>(() => sut.CreateDeal(new List<Game> { game1, game2 }, new List<Player> { player }, new Event(), filter));
    }

    [Test]
    public void ShouldCallCardsFactoryWhenConstructing()
    {
      var sut = new DealFactory { CardsFactory = Substitute.For<ICardsFactory>() };
      var game1 = new Game { Deal = "deal" };
      var game2 = new Game { Deal = "deal" };
      var player = new Player();
      var cards = Substitute.For<ICards>();
      var filter = Substitute.For<IFilter>();
      sut.CardsFactory.CreateCards("deal").Returns(cards);
      var result = sut.CreateDeal(new List<Game> { game1, game2 }, new List<Player> { player }, new Event(), filter);

      Assert.That(result.Cards, Is.SameAs(cards));
    }

    [Test]
    public void ShouldSetNumberOfPairsToTwoTimesNumberOfGames()
    {
      var sut = new DealFactory { CardsFactory = Substitute.For<ICardsFactory>() };
      var game1 = new Game();
      var game2 = new Game();
      var game3 = new Game();
      var player = new Player();
      var filter = Substitute.For<IFilter>();

      var result = sut.CreateDeal(new List<Game> { game1, game2, game3 }, new List<Player> { player }, new Event(), filter);

      Assert.That(result.NumberOfPairs, Is.EqualTo(6));
    }

    [Test]
    public void ShouldCreateOnePlayerDealForEachPlayer()
    {
      var sut = new DealFactory { CardsFactory = Substitute.For<ICardsFactory>(), PlayerDealFactory = Substitute.For<IPlayerDealFactory>(), MatchPointScoreServiceFactory = Substitute.For<IMatchPointScoreServiceFactory>(), ImpScoreServiceFactory = Substitute.For<IImpScoreServiceFactory>(), FairBridgeScoreServiceFactory = Substitute.For<IFairBridgeScoreServiceFactory>() };
      var matchPointService = Substitute.For<IMatchPointScoreService>();
      sut.MatchPointScoreServiceFactory.CreateService().Returns(matchPointService);
      var impService = Substitute.For<IImpScoreService>();
      sut.ImpScoreServiceFactory.CreateService().Returns(impService);
      var fairBridgeScoreService = Substitute.For<IFairBridgeScoreService>();
      sut.FairBridgeScoreServiceFactory.CreateService(Arg.Any<IDeal>()).Returns(fairBridgeScoreService);
      var game1 = new Game { NorthId = 1 };
      var game2 = new Game { EastId = 2 };
      var game3 = new Game { WestId = 3 };
      var game4 = new Game { SouthId = 4 };
      var game5 = new Game { EastId = 5 };
      var game6 = new Game { SouthId = 6 };
      var player1 = new Player { Id = 1 };
      var player2 = new Player { Id = 2 };
      var player3 = new Player { Id = 3 };
      var player4 = new Player { Id = 4 };
      var filter = Substitute.For<IFilter>();

      var result = sut.CreateDeal(
        new List<Game> { game1, game2, game3, game4, game5, game6 },
        new List<Player> { player1, player2, player3, player4 },
        new Event(),
        filter);

      sut.PlayerDealFactory.Received(1).CreatePlayerDeal(game1, player1, matchPointService, impService, fairBridgeScoreService);
      sut.PlayerDealFactory.Received(1).CreatePlayerDeal(game2, player2, matchPointService, impService, fairBridgeScoreService);
      sut.PlayerDealFactory.Received(1).CreatePlayerDeal(game3, player3, matchPointService, impService, fairBridgeScoreService);
      sut.PlayerDealFactory.Received(1).CreatePlayerDeal(game4, player4, matchPointService, impService, fairBridgeScoreService);
      Assert.That(result.PlayerDeals, Has.Count.EqualTo(4));
    }

    [Test]
    public void ShouldSelectMostFrequentContract()
    {
      var sut = new DealFactory
                  {
                    CardsFactory = Substitute.For<ICardsFactory>(),
                    ContractFactory = Substitute.For<IContractFactory>()
                  };
      var games = new List<Game>
                    {
                      new Game { NorthId = 1, Contract = new Contract(2, Suit.Clubs, Risk.Undoubled) },
                      new Game { EastId = 2, Contract = new Contract(2, Suit.Clubs, Risk.Undoubled) },
                      new Game { WestId = 3, Contract = new Contract(1, Suit.Clubs, Risk.Undoubled) },
                      new Game { SouthId = 4, Contract = new Contract(2, Suit.Spades, Risk.Undoubled) },
                      new Game { EastId = 5, Contract = new Contract(3, Suit.Diamonds, Risk.Doubled) },
                      new Game { SouthId = 6, Contract = new Contract(3, Suit.Diamonds, Risk.Doubled) },
                      new Game { SouthId = 7, Contract = new Contract(3, Suit.Diamonds, Risk.Doubled) },
                      new Game { EastId = 8, Contract = new Contract(2, Suit.Clubs, Risk.Redoubled) },
                      new Game { EastId = 9, Contract = new Contract(2, Suit.Clubs, Risk.Redoubled) },
                    };
      var player1 = new Player { Id = 1 };
      var player2 = new Player { Id = 2 };
      var filter = Substitute.For<IFilter>();

      var createdContract = Substitute.For<IContract>();
      sut.ContractFactory.CreateContract(new Contract(3, Suit.Diamonds, Risk.Doubled), Arg.Any<PlayerDirection>(), Arg.Any<Vulnerability>()).Returns(createdContract);
      var result = sut.CreateDeal(games, new List<Player> { player1, player2 }, new Event(), filter);

      Assert.That(result.MostFrequentContract, Is.SameAs(createdContract));
    }

    [Test]
    public void ShouldCountNSAndEWDeclarerAsDifferentWhenSelectingMostFrequentContract()
    {
      var sut = new DealFactory
                  {
                    CardsFactory = Substitute.For<ICardsFactory>(),
                    ContractFactory = Substitute.For<IContractFactory>()
                  };
      var games = new List<Game>
                    {
                      new Game { Declarer = PlayerDirection.North, Contract = new Contract(2, Suit.Clubs, Risk.Undoubled) },
                      new Game { Declarer = PlayerDirection.North, Contract = new Contract(2, Suit.Clubs, Risk.Undoubled) },
                      new Game { Declarer = PlayerDirection.North, Contract = new Contract(2, Suit.Clubs, Risk.Undoubled) },
                      new Game { Declarer = PlayerDirection.North, Contract = new Contract(2, Suit.Clubs, Risk.Undoubled) },
                      new Game { Declarer = PlayerDirection.South, Contract = new Contract(3, Suit.Diamonds, Risk.Undoubled) },
                      new Game { Declarer = PlayerDirection.North, Contract = new Contract(3, Suit.Diamonds, Risk.Undoubled) },
                      new Game { Declarer = PlayerDirection.South, Contract = new Contract(3, Suit.Diamonds, Risk.Undoubled) },
                      new Game { Declarer = PlayerDirection.North, Contract = new Contract(3, Suit.Diamonds, Risk.Undoubled) },
                      new Game { Declarer = PlayerDirection.North, Contract = new Contract(3, Suit.Diamonds, Risk.Undoubled) },
                      new Game { Declarer = PlayerDirection.East, Contract = new Contract(2, Suit.Clubs, Risk.Undoubled) },
                      new Game { Declarer = PlayerDirection.West, Contract = new Contract(2, Suit.Clubs, Risk.Undoubled) },
                    };
      var createdContract = Substitute.For<IContract>();
      var filter = Substitute.For<IFilter>();
      sut.ContractFactory.CreateContract(new Contract(3, Suit.Diamonds, Risk.Undoubled), PlayerDirection.North, Arg.Any<Vulnerability>()).Returns(createdContract);
      var result = sut.CreateDeal(games, new List<Player>(), new Event(), filter);

      Assert.That(result.MostFrequentContract, Is.SameAs(createdContract));
    }

    [Test]
    public void ShouldSelectMostFrequentScore()
    {
      var sut = new DealFactory
                  {
                    CardsFactory = Substitute.For<ICardsFactory>(),
                    ContractFactory = Substitute.For<IContractFactory>()
                  };
      var games = new List<Game>
                    {
                      new Game { Score = new Score(140), Declarer = PlayerDirection.East },
                      new Game { Score = new Score(-100), Declarer = PlayerDirection.North },
                      new Game { Score = new Score(100), Declarer = PlayerDirection.East },
                      new Game { Score = new Score(100), Declarer = PlayerDirection.East },
                      new Game { Score = new Score(100), Declarer = PlayerDirection.East },
                      new Game { Score = new Score(140), Declarer = PlayerDirection.East },
                      new Game { Score = new Score(-100), Declarer = PlayerDirection.North },
                      new Game { Score = new Score(50), Declarer = PlayerDirection.East },
                      new Game { Score = new Score(140), Declarer = PlayerDirection.East },
                      new Game { Score = new Score(140), Declarer = PlayerDirection.East },
                    };
      var player1 = new Player { Id = 1 };
      var player2 = new Player { Id = 2 };
      var filter = Substitute.For<IFilter>();

      var result = sut.CreateDeal(games, new List<Player> { player1, player2 }, new Event(), filter);

      Assert.That(result.MostFrequentScore, Is.EqualTo(-100));
    }

    [Test]
    public void ShouldSelectMostFrequentSuit()
    {
      var sut = new DealFactory
                  {
                    CardsFactory = Substitute.For<ICardsFactory>(),
                    SuitFactory = Substitute.For<ISuitFactory>()
                  };
      var games = new List<Game>
                    {
                      new Game { NorthId = 1, Contract = new Contract(2, Suit.Clubs, Risk.Undoubled) },
                      new Game { EastId = 2, Contract = new Contract(2, Suit.Clubs, Risk.Undoubled) },
                      new Game { WestId = 3, Contract = new Contract(1, Suit.Clubs, Risk.Undoubled) },
                      new Game { SouthId = 4, Contract = new Contract(2, Suit.Spades, Risk.Undoubled) },
                      new Game { EastId = 5, Contract = new Contract(3, Suit.Diamonds, Risk.Doubled) },
                      new Game { SouthId = 6, Contract = new Contract(3, Suit.Diamonds, Risk.Doubled) },
                      new Game { SouthId = 7, Contract = new Contract(3, Suit.Diamonds, Risk.Doubled) },
                      new Game { EastId = 8, Contract = new Contract(2, Suit.Clubs, Risk.Redoubled) },
                      new Game { EastId = 9, Contract = new Contract(2, Suit.Clubs, Risk.Redoubled) },
                    };
      var player1 = new Player { Id = 1 };
      var player2 = new Player { Id = 2 };

      var createdSuit = Substitute.For<ISuit>();
      var filter = Substitute.For<IFilter>();
      sut.SuitFactory.CreateSuit(Suit.Clubs).Returns(createdSuit);
      var result = sut.CreateDeal(games, new List<Player> { player1, player2 }, new Event(), filter);

      Assert.That(result.MostFrequentSuit, Is.SameAs(createdSuit));
    }

    [Test]
    public void ShouldCallVulnerabilityFactoryWhenConstructing()
    {
      var sut = new DealFactory { CardsFactory = Substitute.For<ICardsFactory>(), VulnerabilityFactory = Substitute.For<IVulnerabilityFactory>() };
      var game1 = new Game { Vulnerable = Vulnerability.EastWest };
      var player = new Player();
      var vulnerability = Substitute.For<IVulnerability>();
      var filter = Substitute.For<IFilter>();
      sut.VulnerabilityFactory.CreateVulnerability(Vulnerability.EastWest).Returns(vulnerability);
      var result = sut.CreateDeal(new List<Game> { game1 }, new List<Player> { player }, new Event(), filter);

      Assert.That(result.Vulnerability, Is.SameAs(vulnerability));
    }

    [Test]
    public void ShouldInitializeMatchPointScoreService()
    {
      var sut = new DealFactory
                  {
                    CardsFactory = Substitute.For<ICardsFactory>(),
                    ImpScoreServiceFactory = Substitute.For<IImpScoreServiceFactory>(),
                    MatchPointScoreServiceFactory = Substitute.For<IMatchPointScoreServiceFactory>(),
                    PlayerDealFactory = Substitute.For<IPlayerDealFactory>(),
                  };
      var game1 = new Game();
      var game2 = new Game();
      var deal1 = Substitute.For<IPlayerDeal>();
      var deal2 = Substitute.For<IPlayerDeal>();
      var position1 = Substitute.For<IPosition>();
      var position2 = Substitute.For<IPosition>();
      var filter = Substitute.For<IFilter>();
      deal1.Score.Returns(123);
      deal2.Score.Returns(456);
      deal1.Position.Returns(position1);
      deal2.Position.Returns(position2);
      var expected = new[] { (123, position1), (456, position2) };
      var matchPointService = Substitute.For<IMatchPointScoreService>();
      sut.MatchPointScoreServiceFactory.CreateService().Returns(matchPointService);
      var impScoreService = Substitute.For<IImpScoreService>();
      sut.ImpScoreServiceFactory.CreateService().Returns(impScoreService);
      sut.PlayerDealFactory.CreatePlayerDeal(game1, null, matchPointService, impScoreService, null).Returns(deal1);
      sut.PlayerDealFactory.CreatePlayerDeal(game2, null, matchPointService, impScoreService, null).Returns(deal2);
      sut.CreateDeal(new List<Game> { game1, game2 }, new List<Player>(), new Event(), filter);

      matchPointService.Received().Initialize(Arg.Is<IEnumerable<(int, IPosition)>>(arg => arg.SequenceEqual(expected)));
    }

    [Test]
    public void ShouldInitializeImpScoreService()
    {
      var sut = new DealFactory
                  {
                    CardsFactory = Substitute.For<ICardsFactory>(),
                    ImpScoreServiceFactory = Substitute.For<IImpScoreServiceFactory>(),
                    MatchPointScoreServiceFactory = Substitute.For<IMatchPointScoreServiceFactory>(),
                    PlayerDealFactory = Substitute.For<IPlayerDealFactory>(),
                  };
      var game1 = new Game();
      var game2 = new Game();
      var deal1 = Substitute.For<IPlayerDeal>();
      var deal2 = Substitute.For<IPlayerDeal>();
      var position1 = Substitute.For<IPosition>();
      var position2 = Substitute.For<IPosition>();
      var filter = Substitute.For<IFilter>();
      deal1.Score.Returns(123);
      deal2.Score.Returns(456);
      deal1.Position.Returns(position1);
      deal2.Position.Returns(position2);
      var expected = new[] { (123, position1), (456, position2) };
      var matchPointService = Substitute.For<IMatchPointScoreService>();
      sut.MatchPointScoreServiceFactory.CreateService().Returns(matchPointService);
      var impScoreService = Substitute.For<IImpScoreService>();
      sut.ImpScoreServiceFactory.CreateService().Returns(impScoreService);
      sut.PlayerDealFactory.CreatePlayerDeal(game1, null, matchPointService, impScoreService, null).Returns(deal1);
      sut.PlayerDealFactory.CreatePlayerDeal(game2, null, matchPointService, impScoreService, null).Returns(deal2);
      sut.CreateDeal(new List<Game> { game1, game2 }, new List<Player>(), new Event(), filter);

      impScoreService.Received().Initialize(Arg.Is<IEnumerable<(int, IPosition)>>(arg => arg.SequenceEqual(expected)));
    }

    [Test]
    public void ShouldCreateFaiBridgeScoreService()
    {
      var sut = new DealFactory
      {
        CardsFactory = Substitute.For<ICardsFactory>(),
        ImpScoreServiceFactory = Substitute.For<IImpScoreServiceFactory>(),
        MatchPointScoreServiceFactory = Substitute.For<IMatchPointScoreServiceFactory>(),
        PlayerDealFactory = Substitute.For<IPlayerDealFactory>(),
        FairBridgeScoreServiceFactory = Substitute.For<IFairBridgeScoreServiceFactory>(),
      };
      var player1 = new Player { Id = 3 };
      var game1 = new Game { EastId = 3 };
      var game2 = new Game { NorthId = 3 };
      var deal1 = Substitute.For<IPlayerDeal>();
      var deal2 = Substitute.For<IPlayerDeal>();
      var filter = Substitute.For<IFilter>();
      var matchPointService = Substitute.For<IMatchPointScoreService>();
      sut.MatchPointScoreServiceFactory.CreateService().Returns(matchPointService);
      var impScoreService = Substitute.For<IImpScoreService>();
      sut.ImpScoreServiceFactory.CreateService().Returns(impScoreService);
      var fairBridgeScoreService = Substitute.For<IFairBridgeScoreService>();
      sut.FairBridgeScoreServiceFactory.CreateService(Arg.Any<IDeal>()).Returns(fairBridgeScoreService);
      sut.PlayerDealFactory.CreatePlayerDeal(game1, player1, matchPointService, impScoreService, fairBridgeScoreService).Returns(deal1);
      sut.PlayerDealFactory.CreatePlayerDeal(game2, player1, matchPointService, impScoreService, fairBridgeScoreService).Returns(deal2);
      var deal = sut.CreateDeal(new List<Game> { game1, game2 }, new List<Player> { player1 }, new Event(), filter);

      sut.FairBridgeScoreServiceFactory.Received().CreateService(deal);
      Assert.That(deal.PlayerDeals, Does.Contain(deal1));
      Assert.That(deal.PlayerDeals, Does.Contain(deal2));
    }

    [Test]
    public void ShouldOnlyIncludePlayerDealsAcceptedByFilter()
    {
      var sut = new DealFactory
                  {
                    CardsFactory = Substitute.For<ICardsFactory>(),
                    ImpScoreServiceFactory = Substitute.For<IImpScoreServiceFactory>(),
                    MatchPointScoreServiceFactory = Substitute.For<IMatchPointScoreServiceFactory>(),
                    PlayerDealFactory = Substitute.For<IPlayerDealFactory>(),
                    FairBridgeScoreServiceFactory = Substitute.For<IFairBridgeScoreServiceFactory>(),
                  };

      var matchPointService = Substitute.For<IMatchPointScoreService>();
      sut.MatchPointScoreServiceFactory.CreateService().Returns(matchPointService);
      var impService = Substitute.For<IImpScoreService>();
      sut.ImpScoreServiceFactory.CreateService().Returns(impService);
      var fairBridgeScoreService = Substitute.For<IFairBridgeScoreService>();
      sut.FairBridgeScoreServiceFactory.CreateService(Arg.Any<IDeal>()).Returns(fairBridgeScoreService);

      var player1 = new Player { Id = 3 };
      var player2 = new Player { Id = 4 };
      var game1 = new Game { EastId = 3 };
      var game2 = new Game { NorthId = 4 };
      var deal1 = Substitute.For<IPlayerDeal>();
      var deal2 = Substitute.For<IPlayerDeal>();
      var filter = Substitute.For<IFilter>();

      sut.PlayerDealFactory.CreatePlayerDeal(game1, player1, matchPointService, impService, fairBridgeScoreService).Returns(deal1);
      sut.PlayerDealFactory.CreatePlayerDeal(game2, player2, matchPointService, impService, fairBridgeScoreService).Returns(deal2);

      filter.Excluded(deal1).Returns(false);
      filter.Excluded(deal2).Returns(true);
      var deal = sut.CreateDeal(new List<Game> { game1, game2 }, new List<Player> { player1, player2 }, new Event(), filter);

      Assert.That(deal.PlayerDeals, Is.EquivalentTo(new[] { deal1 }));
    }
  }
}