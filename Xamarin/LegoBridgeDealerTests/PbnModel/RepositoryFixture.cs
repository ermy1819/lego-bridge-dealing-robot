﻿using System;
using System.Linq;
using LegoBridgeDealer.PbnModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel
{
  internal class RepositoryFixture
  {
    [TestCase(true)]
    [TestCase(false)]
    public void ShouldThrowExceptionWhenFetchingFromRepositoryWithoutQueryMethod(bool addValues)
    {
      var sut = new Repository<Game>();
      Assert.Throws<InvalidOperationException>(() => sut.Fetch(addValues));
    }

    [Test]
    [TestCase(true)]
    [TestCase(false)]
    public void ShouldUseQueryMethodWhenFetching(bool addValues)
    {
      bool isCalled = false;

      var sut = new Repository<Game>(() => 
      {
        isCalled = true;
        return Enumerable.Empty<Game>(); 
      });

      sut.Fetch(addValues);
      Assert.That(isCalled, Is.True);
    }

    [Test]
    public void ShouldNotAddAnyItemsWhenFetchingWithoutAdding()
    {
      var fetchedData = new[] { new Game(), new Game() };
      var sut = new Repository<Game>(() => fetchedData);
      var result = sut.Fetch(false);

      Assert.That(sut.Count, Is.EqualTo(0));
      Assert.That(result.Count, Is.EqualTo(fetchedData.Length));
      Assert.That(result[0], Is.SameAs(fetchedData[0]));
      Assert.That(result[1], Is.SameAs(fetchedData[1]));
    }

    [Test]
    public void ShouldAddFetchedItemsToCollectionWhenFetchingWithAdding()
    {
      var fetchedData = new[] { new Game { Id = 7 }, new Game { Id = 9 } };
      var sut = new Repository<Game>(() => fetchedData);
      var result = sut.Fetch(true);

      Assert.That(sut.Count, Is.EqualTo(fetchedData.Length));
      Assert.That(result.Count, Is.EqualTo(fetchedData.Length));
      Assert.That(result[0], Is.SameAs(fetchedData[0]));
      Assert.That(result[1], Is.SameAs(fetchedData[1]));
      Assert.That(sut.All(kv => kv.Key == kv.Value.Id));
      Assert.That(sut[7], Is.SameAs(fetchedData[0]));
      Assert.That(sut[9], Is.SameAs(fetchedData[1]));
    }
  }
}