﻿using LegoBridgeDealer.PbnModel.WriteModel;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.WriteFilder
{
  internal class ScoreFixture
  {
    [Test]
    public void ShouldBeForDeclarerWhenUsingDefaultArg()
    {
      var sut = new Score(50);
      Assert.That(sut.ForPlayer, Is.EqualTo(PlayerDirection.Declarer));
    }

    [TestCase(PlayerDirection.Declarer)]
    [TestCase(PlayerDirection.North)]
    [TestCase(PlayerDirection.East)]
    public void ValidForPlayers(PlayerDirection player)
    {
      var sut = new Score(-100, player);
      Assert.That(sut.ForPlayer, Is.EqualTo(player));
    }

    [TestCase(PlayerDirection.Dummy)]
    [TestCase(PlayerDirection.South)]
    [TestCase(PlayerDirection.West)]
    [TestCase(PlayerDirection.LeftHandDefender)]
    [TestCase(PlayerDirection.RightHandDefender)]
    [TestCase(PlayerDirection.Me)]
    [TestCase(PlayerDirection.Partner)]
    [TestCase(PlayerDirection.LeftHandOpponent)]
    [TestCase(PlayerDirection.RightHandOpponent)]
    public void InvalidForPlayers(PlayerDirection player)
    {
#pragma warning disable CA1806 // Do not ignore method results
      Assert.Throws<AssertionException>(() => new Score(-100, player));
#pragma warning restore CA1806 // Do not ignore method results
    }

    [TestCase(PlayerDirection.North)]
    [TestCase(PlayerDirection.East)]
    [TestCase(PlayerDirection.South)]
    [TestCase(PlayerDirection.West)]
    public void ValidForDeclarer(PlayerDirection player)
    {
      var sut = new Score(920);
      Assert.DoesNotThrow(() => sut.GetScore(player));
    }

    [TestCase(PlayerDirection.Declarer)]
    [TestCase(PlayerDirection.Dummy)]
    [TestCase(PlayerDirection.LeftHandDefender)]
    [TestCase(PlayerDirection.RightHandDefender)]
    [TestCase(PlayerDirection.Me)]
    [TestCase(PlayerDirection.Partner)]
    [TestCase(PlayerDirection.LeftHandOpponent)]
    [TestCase(PlayerDirection.RightHandOpponent)]
    public void InvalidForDeclarer(PlayerDirection player)
    {
      var sut = new Score(920);
      Assert.Throws<AssertionException>(() => sut.GetScore(player));
    }

    [TestCase(PlayerDirection.Declarer, PlayerDirection.North, false)]
    [TestCase(PlayerDirection.Declarer, PlayerDirection.South, false)]
    [TestCase(PlayerDirection.Declarer, PlayerDirection.East, false)]
    [TestCase(PlayerDirection.Declarer, PlayerDirection.West, false)]
    [TestCase(PlayerDirection.North, PlayerDirection.North, false)]
    [TestCase(PlayerDirection.North, PlayerDirection.South, false)]
    [TestCase(PlayerDirection.North, PlayerDirection.East, true)]
    [TestCase(PlayerDirection.North, PlayerDirection.West, true)]
    [TestCase(PlayerDirection.East, PlayerDirection.North, true)]
    [TestCase(PlayerDirection.East, PlayerDirection.South, true)]
    [TestCase(PlayerDirection.East, PlayerDirection.East, false)]
    [TestCase(PlayerDirection.East, PlayerDirection.West, false)]
    public void ShouldConvertScoreToDeclarerScore(PlayerDirection player, PlayerDirection declarer, bool switchSign)
    {
      var sut = new Score(200, player);
      var score = sut.GetScore(declarer);
      Assert.That(score, Is.EqualTo(switchSign ? -200 : 200));
    }
  }
}