using System.Linq;
using LegoBridgeDealer.PbnModel;
using LegoBridgeDealer.PbnModel.WriteModel;
using NSubstitute;
using NUnit.Framework;

namespace LegoBridgeDealerTests.PbnModel.WriteFilder
{
  internal class RootFixture
  {
    [Test]
    public void ShouldPersistInDatabaseWhenPersisting()
    {
      var database = Substitute.For<IDatabase>();
      var sut = new Root(database);
      var data = new Root(null);

      sut.Persist(data);

      database.Received().Persist(data);
    }

    [Test]
    public void ShouldGetPlayersFromDatabaseWhenFetchingFromPlayersRepository()
    {
      var database = Substitute.For<IDatabase>();
      var sut = new Root(database);

      sut.Players.Fetch(false);

      database.Received().GetAllPlayers();
    }

    [Test]
    public void ShouldGetEventsFromDatabaseWhenFetchingFromEventsRepository()
    {
      var database = Substitute.For<IDatabase>();
      var sut = new Root(database);

      sut.Events.Fetch(false);

      database.Received().GetAllEvents();
    }

    [Test]
    public void ShouldGetGamesFromDatabaseWhenFetchingFromGamesRepository()
    {
      var database = Substitute.For<IDatabase>();
      var sut = new Root(database);
      sut.Events.Add(0, new Event { Id = 0 });
      sut.Events.Add(1, new Event { Id = 1 });

      sut.Games.Fetch(false);

      foreach (var eventsValue in sut.Events.Values)
      {
        database.Received().GetGames(Arg.Is(eventsValue));
      }
    }

    [Test]
    public void ShouldGetAllGamesForAllEventsWhenFetchingFromGamesRepository()
    {
      var database = Substitute.For<IDatabase>();
      var sut = new Root(database);
      var event1 = new Event { Id = 0 };
      var event2 = new Event { Id = 1 };
      sut.Events.Add(0, event1);
      sut.Events.Add(1, event2);

      var returnedGames = new[] { new Game { Id = 3 }, new Game { Id = 5 } };
      database.GetGames(event1).Returns(returnedGames);
      var returnedGames2 = new[] { new Game { Id = -99 } };
      database.GetGames(event2).Returns(returnedGames2);

      sut.Games.Fetch(true);

      database.Received().GetGames(event1);
      database.Received().GetGames(event2);

      Assert.That(sut.Games.Values, Is.EquivalentTo(returnedGames.Concat(returnedGames2)));
    }

    [Test]
    public void ShouldGetEventIdsFromDatabaseWhenFetchingEventIdsForCurrentPlayers()
    {
      var database = Substitute.For<IDatabase>();
      var sut = new Root(database);
      sut.Players.Add(1, new Player());
      sut.Players.Add(7, new Player());
      sut.Players.Add(153, new Player());

      sut.FetchEventIdsForCurrentPlayers();

      database.Received().GetEventIdsForPlayers(sut.Players.Keys);
    }
  }
}