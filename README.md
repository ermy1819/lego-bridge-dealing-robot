# Lego Bridge Dealing Robot

Want a bridge dealing machine but don't want to spen $3,000 - $5,000 on buying one? Build one yourself using leftover lego, a $10 Raspberry Pi Zero W and a $30 Raspberry Pi camera.

This project contains the software needed to run it. It consists of three parts. 

 + The Java program for the Lego NXT unit running the Lejos firmware to control the engines and sensors needed to deal the cards.
 + The Raspberry Pi code identifying the cards. Based on Python3.5 and OpenCV 3.4.1.
 + The android app for selecting deals and sending them to the NXT unit.


