# import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy

class CapturePicture:

  def __init__ (self):
    self.camera = PiCamera()
    
    # initialize the camera and grab a reference to the raw camera capture
    self.rawCapture = PiRGBArray(self.camera)
    # allow the camera to warmup
    self.camera.contrast = 0
    self.camera.brightness = 50
    self.camera.awb_mode = 'flash'
    self.camera.exposure_mode = 'snow'
    time.sleep(1)

  def getRaw(self):
    self.rawCapture.truncate(0)
    self.camera.capture(self.rawCapture, format="bgr")
    image = self.rawCapture.array
    return image
 
  def crop(self, image):
    return image[20:355, 210:610, 0:3]  

  def preprocess(self, image):
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray,(5,5),0)
    return blur

  def close(self):
    self.camera.close()
    self.rawCapture.close()

  def findRankAndSuit(self, processed):
    retval, thresh = cv2.threshold(processed, 120, 255, cv2. THRESH_BINARY)
    dummy,cnts,hier = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    cnts = sorted(cnts, key=cv2.contourArea,reverse=True)
    x,y,w,h = cv2.boundingRect(cnts[0])
    rank = processed[y+int(2*h/3)-50:y+h-5, x+int(w/2)-40:x+w-20]
    suit = processed[y+int(2*h/3)-50:y+h-5, x+25:int(x+w/2)-10]
    return rank, suit

  def isolateRoi(self, cropped, size, threshold):
    retval, thresh = cv2.threshold(cropped, threshold, 255, cv2. THRESH_BINARY_INV)
    dummy,cnts,hier = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    cnts = sorted(cnts, key=cv2.contourArea,reverse=True)
    x,y,w,h = cv2.boundingRect(cnts[0])
    roi = thresh[y:y+h, x:x+w]
    sized = cv2.resize(roi, size, 0, 0)
    return sized

  def getRankAndSuitImages(self):
    image = self.getRaw()
    cropped = self.crop(image)
    processed = self.preprocess(cropped)
    #cv2.imshow("image", image)
    #cv2.imshow("processed", processed)
    #cv2.imshow("cropped", cropped)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    rank, suit = self.findRankAndSuit(processed)
    #cv2.imshow("rank", rank)
    #cv2.imshow("suit", suit)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    rank_sized = self.isolateRoi(rank, (120, 75), 100)
    rank_dilated = cv2.dilate(rank_sized, (1, 1, 1, 1, 1), iterations=3)
    suit_sized = self.isolateRoi(suit, (90, 60), 100)
    return rank_dilated, suit_sized


if (False):
  picture = CapturePicture()

  #rank_sized, suit_sized = picture.getRankAndSuitImages()
  image = picture.getRaw()
  cropped = picture.crop(image)
  processed = picture.preprocess(cropped)
  rank, suit = picture.findRankAndSuit(processed)
  rank_sized = picture.isolateRoi(rank, (120, 75), 100)
  suit_sized = picture.isolateRoi(suit, (90, 60), 100)
  #rank_dilated = cv2.dilate(rank_sized, (1, 1, 1, 1, 1), iterations=3)

  cv2.imshow("image", image)
  cv2.imshow("processed", processed)
  cv2.imshow("cropped", cropped)
  cv2.imshow("rank", rank)
  cv2.imshow("suit", suit)
  cv2.imshow("rank roi", rank_sized)
  #cv2.imshow("rank dialted", rank_dilated)
  cv2.imshow("suit roi", suit_sized)

  picture.close()
  cv2.waitKey(0)
  cv2.destroyAllWindows()
