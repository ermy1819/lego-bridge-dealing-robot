import cv2
import numpy
import os

MAX_DIFF_FOR_SUIT_MATCH = 85000
MAX_DIFF_FOR_RANK_MATCH = 300000

class ImageComparer:
  def __init__(self, path, folders, maxDiff):
    self.trainingSet = []
    self.maxDiff = maxDiff

    for folder in folders:
        fullPath = path + folder
        imagesInFolder = []
        for file in os.scandir(fullPath):
            image = cv2.imread(file.path, cv2.IMREAD_GRAYSCALE)
            imagesInFolder.append(image)

        self.trainingSet.append(imagesInFolder)

  def findBestMatch(self, image):
      i = 0
      bestDiff = self.maxDiff
      bestMatch = -1
      
      for set in self.trainingSet:
          for item in set:
              diffImg = cv2.absdiff(image, item)
              diff = numpy.sum(diffImg)
              #print (str(i) + ": diff " + str(diff))
              if (diff < bestDiff):
                bestDiff = diff
                bestMatch = i

          i = i + 1

      return bestMatch

#rankComparer = ImageComparer("trainingData/", ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"], MAX_DIFF_FOR_RANK_MATCH)
#suitComparer = ImageComparer("trainingData/", ["Clubs", "Diamonds", "Hearts", "Spades"], MAX_DIFF_FOR_SUIT_MATCH)

#rankComparer.findBestMatch(rankComparer.trainingSet[0][1])
#suitComparer.findBestMatch(suitComparer.trainingSet[0][1])
                        
