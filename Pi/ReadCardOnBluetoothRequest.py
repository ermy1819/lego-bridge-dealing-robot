import bluetooth
import random
import CapturePicture
import cv2
import ImageComparer
from gpiozero import LED

led5 = LED(5)
led12 = LED(12)
led5.on()
led12.on()
picture = CapturePicture.CapturePicture()
rankComparer = ImageComparer.ImageComparer("trainingData/", ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"], ImageComparer.MAX_DIFF_FOR_RANK_MATCH)
suitComparer = ImageComparer.ImageComparer("trainingData/", ["Clubs", "Diamonds", "Hearts", "Spades"], ImageComparer.MAX_DIFF_FOR_SUIT_MATCH)
i = 0

def readCardRandom():
    card = random.randint(0, 51)
    return card

def readCardRecord(picture, index):
    rank_sized, suit_sized = picture.getRankAndSuitImages()
    cv2.imwrite("trainingData/suit"+str(index)+".jpg",suit_sized)
    cv2.imwrite("trainingData/rank"+str(index)+".jpg",rank_sized)
    return index % 52

def compareCard(picture, index, suitComparer, rankComparer):
    rank_sized, suit_sized = picture.getRankAndSuitImages()
    bestSuit = suitComparer.findBestMatch(suit_sized)
    bestRank = rankComparer.findBestMatch(rank_sized)

    if (bestSuit == -1):
        cv2.imwrite("trainingData/suit"+str(index)+".jpg",suit_sized)
    if (bestRank == -1):
        cv2.imwrite("trainingData/rank"+str(index)+".jpg",rank_sized)
    if (bestSuit == -1 or bestRank == -1):
        return 255
    return bestSuit * 13 + bestRank
    
print('starting')
while (True):
  socket = bluetooth.BluetoothSocket()
  try:
      socket.bind(("", 1))
      socket.listen(1)
      clientSocket, clientAddress = socket.accept()
      print(clientAddress)
      try:
        while (True):
            received = clientSocket.recv(1)
            print(received)
            if (received == bytes([37])):
              #card = readCardRandom()
              try:
                #card = readCardRecord(picture, i)
                card = compareCard(picture, i, suitComparer, rankComparer)
                print(card)
                clientSocket.send(bytes([card]))
              except IndexError:
                print('error')
                clientSocket.send(bytes([255]))
            #clientSocket.close()
            i = i + 1
      except bluetooth.btcommon.BluetoothError:
          print('comerror')
          clientSocket.close()
      socket.close()
  except bluetooth.btcommon.BluetoothError:
      socket.close()
