import lejos.nxt.Button;
import lejos.nxt.LCD;

public class Logger {

	private int row;

	public void log(String message) {
		if (row > 7) {
			clear();
		}

		if (message == null)
			message = "<null>";
		LCD.drawString(message, 0, row);
		row++;
	}

	public void clear() {
		LCD.clear();
		row = 0;
	}

	public void throwIfCanceling() throws CanceledException {
		if (Button.ESCAPE.isDown())
			throw new CanceledException("Cancel requested by user");
	}
}
