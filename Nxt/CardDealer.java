import lejos.nxt.Button;

public class CardDealer {

	public static void main(String[] args) throws InterruptedException {

		Logger log = new Logger();
		DealReceiver receiver = new DealReceiver(log);
		CardRecogniser sender = new CardRecogniser(log);
		Dealer dealer = new Dealer(log, sender);
		boolean isRunning = true;

		while (isRunning) {
			try {
				sender.disconnect();
				dealer.resetCardSpeed();
				int[] deal = receiver.waitForDeal();
				dealer.Deal(deal);
				log.clear();
			} catch (CanceledException e) {
				if (Button.ESCAPE.isDown())
					isRunning = false;

				log.log(e.getMessage());
			}
		}
	}
}
