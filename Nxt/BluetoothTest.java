import java.io.*;
import javax.bluetooth.*;
import javax.microedition.location.Coordinates;

import lejos.nxt.*;
import lejos.nxt.comm.*;

public class BluetoothTest {

  public static void main(String[] args) throws IOException {

    String connected = "Connected";
    String waiting = "Waiting...";
    String closing = "Closing...";

    while (true) {
      LCD.drawString(waiting,0,0);
      NXTConnection connection = Bluetooth.waitForConnection(0, BTConnection.RAW); 
      LCD.clear();
      LCD.drawString(connected,0,0);

      DataInputStream dis = connection.openDataInputStream();
      DataOutputStream dos = connection.openDataOutputStream();

      for(int i=0;i<100;i++) {
        int n = dis.read();
        //LCD.drawInt(n,7,0,1);
        System.out.print(n);
        System.out.print(" ");
        dos.write(n);
        dos.flush();
      }
      dis.close();
      dos.close();

      LCD.clear();
      LCD.drawString(closing,0,0);

      connection.close();
      LCD.clear();
    }
  }
}
