import java.util.ArrayList;

import lejos.nxt.Button;
import lejos.nxt.ColorSensor;
import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.robotics.Color;

public class Dealer {
	private static final int RotationToOpenHatch = 45;
	private static final int RotationPerFack = 180;
	private static final int CardFoundThreshold = 35;
	private final static int WagonSpeed = 650;
	private final static int InitialCardSpeed = 200;
	private final static int CardMaxSpeed = 500;
	private final static int CardSpeedIncreasePerCard = 3;
	private final static int HatchSpeed = 110;

	private Logger log;
	private CardRecogniser recogniser;
	private ColorSensor cardDetector;
	private NXTRegulatedMotor wagon;
	private NXTRegulatedMotor moveCards;
	private NXTRegulatedMotor hatch;
	private int fack;
	private ArrayList<Integer> deltCards;

	public Dealer(Logger log, CardRecogniser recogniser) {
		this.log = log;
		this.recogniser = recogniser;
		cardDetector = new ColorSensor(SensorPort.S4);
		wagon = Motor.A;
		moveCards = Motor.B;
		hatch = Motor.C;

		wagon.setSpeed(WagonSpeed);
		hatch.setSpeed(HatchSpeed);
		fack = 4;
	}

	public void Deal(int[] cards) throws InterruptedException, CanceledException {
		log.log("Dealing cards");
		moveCards.forward();
		deltCards = new ArrayList<Integer>();
		long lastCardDetected = System.currentTimeMillis();
		boolean completed = false;
		int[] cardsPerPlayer = new int[5];

		try {
			while (!completed && Button.ENTER.isUp()) {
				log.throwIfCanceling();
				
				if (isCardPresent() && moveCards.isMoving()) {
					moveCards.stop();
					int card = -1;
					while (card == -1) {
						card = recogniser.detectCard();
						if (card == -1) {
							hatch.rotate(-10, false);
							hatch.rotate(15, false);
							hatch.rotate(-5, false);
							card = recogniser.detectCard();
						}
						if (card == -1) {
							hatch.rotate(-11, false);
							hatch.rotate(8, false);
							card = recogniser.detectCard();
						}
						if (card == -1) {
							hatch.rotate(15, false);
							hatch.rotate(-9, false);
							card = recogniser.detectCard();
						}
						if (card == -1) {
							hatch.rotate(-11, false);
							hatch.rotate(8, false);
							card = recogniser.detectCard();
						}
						if (card == -1)
							error("Unknown card");
					}
					if (deltCards.contains(card))
						error("Duplicate card");
					deltCards.add(card);
					if (deltCards.size() > 52)
						error("Too many cards");
					int destination = cards[card];
					moveWagonTo(destination);
					cardsPerPlayer[destination] = cardsPerPlayer[destination] + 1;
					hatch.rotate(-RotationToOpenHatch, false);
					lastCardDetected = System.currentTimeMillis();
				}

				if (!isCardPresent() && !moveCards.isMoving()) {
					hatch.rotate(RotationToOpenHatch, false);
					int currentSpeed = moveCards.getSpeed();
					if (currentSpeed < CardMaxSpeed)
						currentSpeed += CardSpeedIncreasePerCard;
					moveCards.setSpeed(currentSpeed);
					moveCards.forward();
				}

				if (System.currentTimeMillis() - lastCardDetected > 5000) {
					if (deltCards.size() == 52) {
						if (cardsPerPlayer[1] == 13 && cardsPerPlayer[2] == 13 && cardsPerPlayer[3] == 13 && cardsPerPlayer[4] == 13) {
							completed = true;
							log.log("Cards delt succesfully");
							Sound.beepSequenceUp();
						}
						else {
							moveWagonTo(4);
							error("Not 13 cards each");
						}
					}
					else{
						error("No card in 10s");
						// Do not timeout again until next card is found.
						lastCardDetected = Long.MAX_VALUE;
						}
					
				}
				
				Thread.sleep(1);
			}
		} catch (CanceledException e) {
			if (Button.ENTER.isUp()) {
				Sound.twoBeeps();
				if (Button.ESCAPE.isUp()) {
					moveCards.stop();
					moveWagonTo(4);
				}
				throw e;
			}
			log.log("Stops dealing");
		}

		moveCards.stop();
		moveWagonTo(4);
	}

	public void resetCardSpeed() {
		moveCards.setSpeed(InitialCardSpeed);
	}

	private boolean isCardPresent() {
		Color colorReading = cardDetector.getColor();
		return colorReading.getGreen() > CardFoundThreshold && colorReading.getBlue() > CardFoundThreshold /*|| colorReading.getRed() > CardFoundThreshold*/;
	}

	private void moveWagonTo(int destination) {
		wagon.rotate(RotationPerFack * (destination - fack), false);
		fack = destination;
	}
	
	private void error(String message) throws CanceledException {
		boolean startOnContinue = moveCards.isMoving();
		moveCards.stop();
		log.log(message);
		Sound.twoBeeps();
		int button = Button.waitForAnyPress();
		switch(button) {
		case Button.ID_RIGHT:
			//continue;
			break;
		case Button.ID_LEFT:
			// reset
			moveWagonTo(4);
			Button.waitForAnyPress();
			resetCardSpeed();
			deltCards.clear();
			break;
		default:
			throw new CanceledException(message);
		}
		
		if (startOnContinue)
			moveCards.forward();
	}
}
