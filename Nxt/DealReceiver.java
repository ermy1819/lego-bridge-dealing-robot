import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.Button;
import lejos.nxt.Sound;
import lejos.nxt.comm.NXTConnection;

public class DealReceiver {

	private Logger log;

	public DealReceiver(Logger log) {
		this.log = log;
	}

	public int[] waitForDeal() throws CanceledException, InterruptedException {
		log.log("Waiting for deal");

		//BluetoothWithTimeout.setLogger(log);
		
		NXTConnection connection = null;
		try {
			while (connection == null) {
				connection = BluetoothWithTimeout.waitForConnection(1500);
				//log.log("Waiting...");
				log.throwIfCanceling();
			}

			//log.log("connected");
			DataInputStream dis = connection.openDataInputStream();
			DataOutputStream dos = connection.openDataOutputStream();

			//log.log("streams created");
			boolean inputOk = true;
			int[] result = new int[52];
			int[] handCount = new int[5];

			for(int i = 0; i < 52; ++i) {
				int readValue = BluetoothWithTimeout.read(dis, 5000);
				//log.log("receiver " + readValue);
				result[i] = readValue;
				if (readValue < 1 || readValue > 4)
					inputOk = false;
				else
					handCount[readValue] = handCount[readValue] + 1;
			}

			//log.log("writing " + inputOk);
			dos.writeBoolean(inputOk);
			dis.close();
			dos.close();
			connection.close();
			//log.log("connection closed");

			for (int i = 1; i <= 4; i++)
				inputOk &= handCount[i] == 13;

			if (!inputOk)
				throw new CanceledException("Incorrect deal received");

			return result;
		}
		catch (IOException e) {
			log.log("BT broken");
			log.log("Please restart");
			Sound.twoBeeps();
			Sound.twoBeeps();
			//Button.waitForAnyPress();
			System.exit(-1);
			return null;
		}
	}
}
