import java.io.*;
import java.util.ArrayList;

import javax.bluetooth.*;
import javax.microedition.location.Coordinates;

import lejos.nxt.*;
import lejos.nxt.comm.*;

public class BtTestClient {

	public static void main(String[] args) throws IOException {
	    ArrayList<RemoteDevice> list = Bluetooth.getKnownDevicesList();
    	for (int i=0; i<list.size(); ++i) {
    		LCD.drawString(list.get(i).getFriendlyName(false) + "||" + list.get(i).getDeviceClass(), 0, i);
    	}

    	//String name = list.get(1).getFriendlyName(false);
    	//LCD.drawInt(name.length(), 0, 3);
    	//LCD.drawInt(name.charAt(2), 10, 3);
	    //LCD.drawString("Connecting...", 0, 0);
    	//ArrayList<RemoteDevice> foundDevices = Bluetooth.inquire(10, 5, 0);
        //LCD.clear();
        //RemoteDevice rpi = null;
    	//for (int i=0; i<foundDevices.size(); ++i) {
    	//	LCD.drawString(foundDevices.get(i).getFriendlyName(false) + "||" + foundDevices.get(i).getDeviceClass(), 0, i);
    	//	if (foundDevices.get(i).getFriendlyName(false).startsWith("rasp"))
    	//		rpi = foundDevices.get(i);
    	//}
  
    	//Button.waitForAnyPress();
    	//Bluetooth.addDevice(rpi);
    	//RemoteDevice btrd = Bluetooth.getKnownDevice(rpi.getFriendlyName(false));
    	
	    //if (btrd == null) {
	    //  LCD.clear();
	    //  LCD.drawString("No such device", 0, 0);
	    //  Button.waitForAnyPress();
	    //  System.exit(1);
	    //}
		
	    LCD.drawString("Left for G6", 0, 5);
	    LCD.drawString("Right for rpi", 0, 6);
	    int button = Button.waitForAnyPress();
	    //BTConnection btc = Bluetooth.connect(btrd);
	    BTConnection btc = null;
	    
	    if (button == Button.ID_LEFT)
	    	btc= Bluetooth.connect(list.get(2));
	    else if (button == Button.ID_RIGHT)
	    	btc= Bluetooth.connect(list.get(0));	    

	    if (btc == null) {
	      LCD.clear();
	      LCD.drawString("Connect fail", 0, 0);
	      Button.waitForAnyPress();
	      System.exit(1);
	    }
	  
	    btc.setIOMode(BTConnection.RAW);
	    
	    LCD.clear();
	    LCD.drawString("Connected", 0, 0);

	    DataInputStream dis = btc.openDataInputStream();
	    DataOutputStream dos = btc.openDataOutputStream();

	    for(int i=0;i<100;i++) {
	      try { 
	        LCD.drawInt(i, 8, 0, 2);
	        dos.writeChars("t:" + i + "\r\n");
	        dos.flush(); 	        
	      } catch (IOException ioe) {
	        LCD.drawString("Write Exception", 0, 0);
	      }
	    
	      try {
	    	  byte[] buffer = new byte[1]; 
	    	  dis.read(buffer, 0, 1);
	        LCD.drawInt(buffer[0],8, 0,3);
	      } catch (IOException ioe) {
	        LCD.drawString("Read Exception ", 0, 0);
	      }
	    }
	  
	    try {
	      LCD.drawString("Closing... ", 0, 0);
	      dis.close();
	      dos.close();
	      btc.close();
	    } catch (IOException ioe) {
	      LCD.drawString("Close Exception", 0, 0);
	    }
	  
	    LCD.drawString("Finished",3, 4);
	    Button.waitForAnyPress();	
	    }
	
}
