import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.bluetooth.RemoteDevice;

import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;

public class CardRecogniser {
	private Logger log;
	private DataInputStream dis;
	private DataOutputStream dos;
	private BTConnection btc;

	public CardRecogniser(Logger log) {
		this.log = log;
	}

	public int detectCard() throws CanceledException, InterruptedException {

		if (btc == null)
			connect();

		int result = -1;
		try {
			result = sendAndRecieve();
		} catch (CanceledException | IOException e) {
			log.throwIfCanceling();
			Thread.sleep(200);
			disconnect();
			connect();
			try {
				result = sendAndRecieve();
			} catch (IOException e1) {
				throw new CanceledException(e1.getMessage());
			}
		}

		return result;
	}

	public void disconnect() throws CanceledException {
		if (btc == null)
			return;

		log.log("closed rpi connection ");
		try {
			dis.close();
		} catch (IOException e) {
		}
		try {
			dos.close();
		} catch (IOException e) {
		}
		btc.close();

		dis = null;
		dos = null;
		btc = null;
	}

	private void connect() throws CanceledException {
		log.log("Connecting to rpi");
		ArrayList<RemoteDevice> list = Bluetooth.getKnownDevicesList();
		RemoteDevice rpi = null;
		for (RemoteDevice remoteDevice : list) {
			if (remoteDevice.getFriendlyName(false).startsWith("raspberrypi"))
				rpi = remoteDevice;
		}

		if (rpi == null)
			throw new CanceledException("No rpi found");

		btc = Bluetooth.connect(rpi);

		if (btc == null) {
			throw new CanceledException("Contacting rpi failed");
		}

		btc.setIOMode(BTConnection.RAW);

		dis = btc.openDataInputStream();
		dos = btc.openDataOutputStream();
	}

	private int sendAndRecieve() throws CanceledException, IOException, InterruptedException {
		int result = -1;
		// log.log("sending command");
		dos.write(37);
		dos.flush();
		// log.log("waiting for response");
		result = BluetoothWithTimeout.read(dis, 5000);
		// log.log("received " + result);
		if (result < 0 || result > 51)
			return -1; // throw new CanceledException("Invalid card received");

		return result;
	}
}
