import java.io.DataInputStream;
import java.io.IOException;

import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;

public class BluetoothWithTimeout extends Thread {
	private BluetoothOperation operation;
	private Object result;
	private IOException error;
	//private static Logger logger;
	
	private BluetoothWithTimeout() {
	}
		
	public void run() {
		try {
			result = operation.execute();
		} catch (IOException e) {
			error = e;
		}

		//logger.log("BT completed");
	}
	
	public static int read(DataInputStream dis, int timeout) throws IOException, InterruptedException {
		BluetoothWithTimeout bluetoothWithTimeout = new BluetoothWithTimeout();
		bluetoothWithTimeout.setOperation(bluetoothWithTimeout.new BluetoothRead(dis));
		return (int)bluetoothWithTimeout.doOperation(timeout);
	}
	
	public static NXTConnection waitForConnection(int timeout) throws IOException, InterruptedException {
		BluetoothWithTimeout bluetoothWithTimeout = new BluetoothWithTimeout();
		bluetoothWithTimeout.setOperation(bluetoothWithTimeout.new BluetoothWaitForConnection(timeout));
		return (NXTConnection)bluetoothWithTimeout.doOperation(timeout + 3000);
	}
	
	public static void reset(int timeout) throws IOException, InterruptedException {
		BluetoothWithTimeout bluetoothWithTimeout = new BluetoothWithTimeout();
		bluetoothWithTimeout.setOperation(bluetoothWithTimeout.new BluetoothReset());
		bluetoothWithTimeout.doOperation(timeout);
	}
	
	//public static void setLogger(Logger log) {
//		logger = log;
	//}
	
	private Object doOperation(int timeout) throws IOException, InterruptedException {
		start();
		
		join(timeout);
		if (isAlive()) {
			interrupt();
			throw new IOException("Bluetooth timeout");
		}
		
		if (error != null)
			throw error;
		
		return result;
	}

	private void setOperation(BluetoothOperation operation) {
		this.operation = operation;
	}
	
	private abstract class BluetoothOperation {
		public abstract Object execute() throws IOException;
	}
	
	private class BluetoothRead extends BluetoothOperation {
		private DataInputStream dis;

		public BluetoothRead(DataInputStream dis) {
			this.dis = dis;		
		}
		
		public Object execute() throws IOException {
			return dis.read();
		}
	}

	private class BluetoothWaitForConnection extends BluetoothOperation {
		private int timeout;

		public BluetoothWaitForConnection(int timeout) {
			this.timeout = timeout;		
		}
		
		public Object execute() throws IOException {
			return Bluetooth.waitForConnection(timeout, BTConnection.RAW);
		}
	}

	private class BluetoothReset extends BluetoothOperation {
		public Object execute() throws IOException {
			Bluetooth.cancelConnect();
			Bluetooth.reset();
			return null;
		}
	}
}
