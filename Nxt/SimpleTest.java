import lejos.nxt.Sound;

import java.util.Date;

import lejos.nxt.ADSensorPort;
import lejos.nxt.Button;
import lejos.nxt.ColorSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.LightSensor;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.Color;
import lejos.robotics.Touch;
import lejos.util.Timer;

public class SimpleTest {

	public static void main(String[] args) throws InterruptedException {
		TouchSensor touch1 = new TouchSensor(SensorPort.S1);
		TouchSensor touch2 = new TouchSensor(SensorPort.S2);
		UltrasonicSensor distance = new UltrasonicSensor(SensorPort.S3);
		ColorSensor color = new ColorSensor(SensorPort.S4);
		boolean luckaClosed = true;
		
		System.out.println(Motor.A.getSpeed());
		Motor.A.setSpeed(110);
		Motor.B.setSpeed(40);
		Motor.C.setSpeed(90);
    	Motor.B.forward();
    	int fack = 4;

    	//Date lastCardDetected = new Date();

    	while(Button.ESCAPE.isUp() && Button.RIGHT.isUp()) {
			//System.out.println("Hello World!");
			//Sound.beep();
		    //if (Motor.A.isMoving())
//		    	Motor.A.stop();
	//	    else
		//    	Motor.A.forward();
	
    		if(IsCardPresent(color) && Motor.B.isMoving())
    		{
    			//lastCardDetected = new Date();
		    	Motor.B.stop();
		    	fack++;
		    	if (fack > 4)
		    	{
		    		Motor.A.rotate(180*(2-fack), false);
		    		fack = 1;
		    	}
		    	else
		    	{
		    		Motor.A.rotate(180, false);
		    	}
		    	Motor.C.rotate(-75, false);
    		}
    		
    		if (!IsCardPresent(color) && !Motor.B.isMoving())
    		{
		    	Motor.C.rotate(75, false);
				Motor.B.setSpeed(Motor.B.getSpeed() + 1);
		    	Motor.B.forward();
    		}
//		    else
	//	    	Motor.B.forward();
		    
    		//if (new Date().getTime() - lastCardDetected > 15000)
/*		    if (luckaClosed)
		    	Motor.C.rotate(75);
		    else
		    	Motor.C.rotate(-75);
	    	luckaClosed = !luckaClosed;
		    
		    System.out.println("Button A is " + (touch1.isPressed() ? "down" : "up"));
		    System.out.println("Button B is " + (touch2.isPressed() ? "down" : "up"));
		    //System.out.println("Distance is " + distance.getDistance());
		    Color colorReading = color.getColor();
		    System.out.println("Color is " + colorReading.getRed() + "x" +  colorReading.getGreen() + "x" + colorReading.getBlue());
	*/	    
		    //Button.waitForAnyPress();
    		Thread.sleep(1);
		}
    	
    	if (!Button.RIGHT.isUp())
    	{
	    	Motor.B.stop();
    		Motor.A.rotate(180*(4-fack), false);
    	}
	}
	
	public static boolean IsCardPresent(ColorSensor color)
	{
	    Color colorReading = color.getColor();
	    return colorReading.getGreen() > 40;
	}

}


