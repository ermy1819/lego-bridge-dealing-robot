import lejos.nxt.Button;
import lejos.nxt.ColorSensor;
import lejos.nxt.SensorPort;
import lejos.robotics.Color;

public class ColorSensorTest {

	public static void main(String[] args) {
		ColorSensor color = new ColorSensor(SensorPort.S4);

		while(Button.ESCAPE.isUp()) {
		    Color colorReading = color.getColor();
		    System.out.println("r: " + colorReading.getRed());
		    System.out.println("g: " + colorReading.getGreen());
		    System.out.println("b: " + colorReading.getBlue());
		    Button.waitForAnyPress();
    	}
	}

}
